package com.retrozelda.nyanimals;

import android.opengl.GLSurfaceView;
import android.opengl.GLES20;
import android.util.Log;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.retrozelda.utilities.Utilities;

public class CNyanimalGLRender implements GLSurfaceView.Renderer
{
	private static final String TAG = CNyanimalGLRender.class.getSimpleName();
	
	// to draw debug text
	public CBitmapFont	m_Text;

	private interface ISurfaceHandler
	{
		public boolean DoAction(Object... args);
	}

	private class Startup implements ISurfaceHandler
	{
		@Override
		public boolean DoAction(Object... args) 
		{
			Utilities.Screen.SetInitialResolution((Integer)args[0], (Integer)args[1]);
			
			// TODO: Create functions for when we are in the render thread and have all graphic inits done in there
			Utilities.GameClass = new CNyanimalGame();
			Utilities.GameClass.Init();
			Utilities.GameClass.onCreate();
			Utilities.GameClass.onStart();
			
			CTextureManager texMan = CTextureManager.GetInstance();
			texMan.ReloadTextures();
			
			// Init debug font
			Utilities.Log.Debug(TAG, "Init debug font");
			m_Text = CNyanimalFactoy.RequestObject(CBitmapFont.class);
			m_Text.SetFontInfo('!', 32, 32, R.drawable.times_news_roman);
			m_Text.SetFontSize(12, 12);
			m_Text.SetDraw(true);
			
			SurfaceChangeHandler = new SendEvent();
			return false;
		}
	}
	
	private class SendEvent implements ISurfaceHandler
	{
		@Override
		public boolean DoAction(Object... args) 
		{
			Utilities.GameClass.onSurfaceChanged((Integer)args[0], (Integer)args[1]);
			return true;
		}
	}
	
	ISurfaceHandler SurfaceChangeHandler;
	
	
	/*
	public boolean ShouldExit()
	{
		return m_game.ShouldExit();
	}
*/
	@Override
	public void onDrawFrame(GL10 glUnused)
	{
		Utilities.Time.StopWatch_Start();
			Utilities.Time.StopWatch_Start();
				Utilities.GameClass.Update();
			Utilities.Time.UpdateTime = Utilities.Time.StopWatch_Stop();
	
			Utilities.Time.StopWatch_Start();
				Utilities.GameClass.Render();
			Utilities.Time.RenderTime = Utilities.Time.StopWatch_Stop();
		Utilities.Time.FrameTime = Utilities.Time.StopWatch_Stop();
		
		// draw this frames stats
		float[] nFontSize = m_Text.GetTransform().GetSize();
		if(Utilities.Settings.DrawDebug)
		{
			m_Text.DrawText("FPS:" + Utilities.Time.FPS(), nFontSize[1], Utilities.GLSurfaceView.getHeight() - (nFontSize[0]));
			m_Text.DrawText("RENDER:" + Utilities.Time.RenderTime, nFontSize[1], Utilities.GLSurfaceView.getHeight() - (nFontSize[0] * 3));
			m_Text.DrawText("UPDATE:" + Utilities.Time.UpdateTime, nFontSize[1], Utilities.GLSurfaceView.getHeight() - (nFontSize[0] * 6));
			m_Text.DrawText("FRAME:" + Utilities.Time.FrameTime, nFontSize[1], Utilities.GLSurfaceView.getHeight() - (nFontSize[0] * 9));
			m_Text.DrawText("DELTA:" + Utilities.Time.DeltaTime_Millis(), nFontSize[1], Utilities.GLSurfaceView.getHeight() - (nFontSize[0] * 12));
			m_Text.DrawText("ELAPSED:" + Utilities.Time.ElapsedTime(), nFontSize[1], Utilities.GLSurfaceView.getHeight() - (nFontSize[0] * 15));
		}
		m_Text.DrawText("FPS:" + Utilities.Time.FPS(), nFontSize[1], Utilities.GLSurfaceView.getHeight() - (nFontSize[0]));
		
		if(Utilities.GameClass.ShouldExit() == true)
		{
			Utilities.GameActivity.finish();
			CNyanimalFactoy.ReturnObject(m_Text);
		}
	}

	@Override
	public void onSurfaceChanged(GL10 glUnused, int width, int height)
	{
		while(!SurfaceChangeHandler.DoAction((Integer)width, (Integer)height));
	}

	@Override
	public void onSurfaceCreated(GL10 glUnused, EGLConfig config)
	{
		Log.d(TAG, "surfaceCreated Start");
		// enable backface culling
		// GLES20.glEnable(GLES20.GL_CULL_FACE);
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		GLES20.glBlendFunc(GLES20.GL_DST_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		GLES20.glEnable(GLES20.GL_BLEND);

		SurfaceChangeHandler = new Startup();
		
		Log.d(TAG, "surfaceCreated End");
	}

	public static int loadShader(int shaderType, String source)
	{
		int shader = GLES20.glCreateShader(shaderType);
		if (shader != 0)
		{
			GLES20.glShaderSource(shader, source);
			GLES20.glCompileShader(shader);
			int[] compiled = new int[1];
			GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
			if (compiled[0] == 0)
			{
				Log.e(TAG, "Could not compile shader " + shaderType + ":");
				Log.e(TAG, GLES20.glGetShaderInfoLog(shader));
				GLES20.glDeleteShader(shader);
				shader = 0;
			}
		}
		return shader;
	}

	public static int createProgram(String vertexSource, String fragmentSource)
	{
		int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
		if (vertexShader == 0)
		{
			return 0;
		}

		int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
		if (pixelShader == 0)
		{
			return 0;
		}

		int program = GLES20.glCreateProgram();
		if (program != 0)
		{
			GLES20.glAttachShader(program, vertexShader);
			GLES20.glAttachShader(program, pixelShader);
			GLES20.glLinkProgram(program);
			int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
			if (linkStatus[0] != GLES20.GL_TRUE)
			{
				Utilities.Log.Error(TAG, "Could not link program: ");
				Utilities.Log.Error(TAG, GLES20.glGetProgramInfoLog(program));
				GLES20.glDeleteProgram(program);
				program = 0;
			}
		}
		return program;
	}
}