package com.retrozelda.nyanimals.catchgame;

import com.retrozelda.FSMLibrary.*;
import com.retrozelda.FSMLibrary.FSMChapter.ChapterAlreadyRunningException;
import com.retrozelda.storybook.StoryBook;

public class CCatchFSMConstructor implements IFSMConstructor
{
	public static String CHAPTER = "NyanimalsCatchFSM";
	public static int CHAPTERHASH = CHAPTER.hashCode();

	@Override
    public String Chapter() { return CHAPTER; }
	@Override
    public int ChapterHash() { return CHAPTERHASH; }
	
	@Override
    public void Build()
    {
		StoryBook.Print("Building: " + Chapter() + " start");
		try
        {
			// NOTE: Add States Here:
	        FSMLibrary.Chapter(ChapterHash()).AddState(CCatchStartState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CCatchHowToState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CCatchEndState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CCatchGameIntroState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CCatchGameUpdateState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CCatchGameOverState.class);
	        /////////////////////////////////////////////////////////////////////////
	        
	        FSMLibrary.Chapter(ChapterHash()).SetDefaultState(CCatchStartState.class);
        } 
		catch (ChapterAlreadyRunningException e)
        {
	        e.printStackTrace();
        }
		StoryBook.Print("Building: " + Chapter() + " end");
    }
	
	@Override
    public void Destroy()
    {
		StoryBook.Print("Destroying: " + Chapter() + " start");
        try
        {
    		// NOTE: Remove States Here:
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CCatchStartState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CCatchHowToState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CCatchEndState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CCatchGameIntroState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CCatchGameUpdateState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CCatchGameOverState.class);
	        /////////////////////////////////////////////////////////////////////////
        } catch (ChapterAlreadyRunningException e)
        {
	        e.printStackTrace();
        }
		StoryBook.Print("Destroying: " + Chapter() + " end");
    }
}
