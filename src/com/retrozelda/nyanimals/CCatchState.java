package com.retrozelda.nyanimals;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.CNyanimal.NyanimalStates;
import com.retrozelda.nyanimals.catchgame.CCatchFSMConstructor;
import com.retrozelda.nyanimals.catchgame.CCatcher;
import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.CObjectBank;
import com.retrozelda.utilities.CObjectBank.ObjectAlreadyStoredException;
import com.retrozelda.utilities.Utilities;
import com.retrozelda.utilities.Utilities.Settings;

import android.graphics.RectF;


public class CCatchState extends CNyanState
{
	// scene stuff
	CNyanimal _Nyanimal;
	CCatcher _Catcher;
	CRectangle _Backdrop;
	CRectangle _BackCloud;
	CRectangle _FrontCloud;
	CBitmapFont _NumberDisplay;

	@Override
    public void OnCreate()
    {
		// teh catcher object
		_Catcher = CNyanimalFactoy.RequestObject(CCatcher.class);
		_NumberDisplay = CNyanimalFactoy.RequestObject(CBitmapFont.class);
		
		// teh background
		_Backdrop = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Backdrop.SetResourceID(R.drawable.catch_bg);
		_Backdrop.FitScreen();
		_Backdrop.SetDrawLayer(Utilities.CommonLayers.Background);
		
		// clouds
		_BackCloud = CNyanimalFactoy.RequestObject(CRectangle.class);
		_BackCloud.SetResourceID(R.drawable.catch_backcloud);
		_BackCloud.SetDrawLayer(Utilities.CommonLayers.Shadow - 1); 

		_FrontCloud = CNyanimalFactoy.RequestObject(CRectangle.class);
		_FrontCloud.SetResourceID(R.drawable.catch_frontcloud);
		_FrontCloud.SetDrawLayer(Utilities.CommonLayers.HudBase); 
		
		// number only font
		_NumberDisplay.SetFontInfo('0', 256, 256, R.drawable.catch_numbers);
		_NumberDisplay.SetFontSize(36, 36);
		_NumberDisplay.SetDrawLayer(Utilities.CommonLayers.HudBase);
		try
        {
	        CObjectBank.Deposit("catch_numberfont", _NumberDisplay);
        } 
		catch (ObjectAlreadyStoredException e)
        {
	        e.printStackTrace();
        }

		//StoryBook.Chapter(NyanimalPages.Chapters.RenderHash).Subscribe(NyanimalPages.RenderListCreateRunEvent.class, this);
    }

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
	}	
	
	@Override
	public void OnEnter()
	{
		int nCloudPosX = Utilities.Screen.GetWidth() / 2;

		//_Catcher.MakeScaledSizeLocal();
		_Catcher.GetTransform().SetSize(1.0f, 1.0f);
		float[] NyanSize = _Nyanimal.GetTransform().GetSize();
		float[] CatchSize = _Catcher.GetTransform().GetSize();
		_Catcher.GetTransform().SetParent(_Nyanimal.GetTransform());
		_Catcher.GetTransform().SetLocalPosition(0.0f, -CatchSize[1]);
		_Catcher.SetDrawLayer(Utilities.CommonLayers.ObjectHold);
		_Catcher.SetDraw(true);
		_Catcher.SetActive(true);
		
		_BackCloud.FitScreenWidth();
		_FrontCloud.FitScreenWidth();
		_BackCloud.GetTransform().SetPosition(nCloudPosX, _BackCloud.GetTransform().GetSize()[1] / 2);
		_FrontCloud.GetTransform().SetPosition(nCloudPosX, _FrontCloud.GetTransform().GetSize()[1] / 2);
        FSMLibrary.StartNewChapter(CCatchFSMConstructor.class, _Nyanimal, _Catcher);

		_Nyanimal.ChangeState(NyanimalStates.CATCH, _Nyanimal);
	}
	
	@Override
	public void OnExit()
	{
        FSMLibrary.StopChapter(CCatchFSMConstructor.class );

		_Catcher.GetTransform().SetParent(null);
		_Catcher.SetDraw(false);
		_Catcher.SetActive(false);
        
		_Nyanimal.DeterminNextAction();
		_Nyanimal.GetTransform().SetPosition((int)(0.5f * Utilities.Screen.GetWidth()), (int)(0.75f * Utilities.Screen.GetHeight()));
	}

	@Override
    public void OnPause()
    {
		_Backdrop.SetDraw(false);
		_Backdrop.SetActive(false);

		_BackCloud.SetDraw(false);
		_BackCloud.SetActive(false);
		
		_FrontCloud.SetDraw(false);
		_FrontCloud.SetActive(false);

		_Catcher.SetDraw(false);
		_Catcher.SetActive(false);

		_Nyanimal.SetDraw(false);
		_Nyanimal.SetActive(false);		

		_NumberDisplay.SetDraw(false);
		_NumberDisplay.SetActive(false);
    }

	@Override
    public void OnResume()
    {			
		_Backdrop.SetDraw(true);
		_Backdrop.SetActive(true);

		_BackCloud.SetDraw(true);
		_BackCloud.SetActive(true);
		
		_FrontCloud.SetDraw(true);
		_FrontCloud.SetActive(true);

		_Catcher.SetDraw(true);
		_Catcher.SetActive(true);

		_Nyanimal.SetDraw(true);
		_Nyanimal.SetActive(true);

		_NumberDisplay.SetDraw(true);
		_NumberDisplay.SetActive(true);
    }


	@Override
    public void PriorityUpdate()
    {
    }

	@Override
    public void Update()
    {
    }

	@Override
    public void LateUpdate()
    {
    }

	@Override
    public void OnDestroy()
    {
		// remove hte font from the bank
        CObjectBank.Widthdraw("catch_numberfont");
		
		CNyanimalFactoy.ReturnObject(_Catcher);
		CNyanimalFactoy.ReturnObject(_Backdrop);
		CNyanimalFactoy.ReturnObject(_BackCloud);
		CNyanimalFactoy.ReturnObject(_FrontCloud);
		CNyanimalFactoy.ReturnObject(_NumberDisplay);
    }

	@Override
    public RectF CheckCollision(CNyanObject inner)
    {
	    return null;
    }

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

}
