package com.retrozelda.nyanimals.catchgame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.inputmethodservice.KeyboardView;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.CBackdrop;
import com.retrozelda.nyanimals.CBitmapFont;
import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimalFactoy;
import com.retrozelda.nyanimals.CRectangle;
import com.retrozelda.nyanimals.CTextureManager;
import com.retrozelda.nyanimals.NyanimalPages;
import com.retrozelda.nyanimals.R;
import com.retrozelda.nyanimals.catchgame.CCatchObject.FallObjectInfo;
import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.CObjectBank;
import com.retrozelda.utilities.Utilities;
import com.retrozelda.utilities.Utilities.Time;

public class CCatchGameUpdateState extends CNyanState implements SensorEventListener
{	
    private SensorManager _SensorManager;
    private Sensor _Accelerometer;

	private CNyanimal _Nyanimal;
	private CCatcher _Catcher;
	private float _fGameTime;
	private int _nScore;
		
	private float _fSpawnTime;
	private float _fCurSpawnTime;
	private float _fSpawnTimeInc;
	private ArrayList<FallObjectInfo> _FallInfoTypes;

	private LinkedList<CCatchObject> _CatchObjects;
	private LinkedList<CCatchObject> _CatchObjects_Destroy;
	
	private CRectangle _Score;
	private CRectangle _Time;
	private CBitmapFont _NumberDisplay;
	private float _fFontSize = 15.0f;
	
	private int _AppleCount;
	private int _GrapeCount;
	private int _CherryCount;
	private int _CandyCount;
	private int _CakeCount;
	private int _StunCount;
	private int _CoreCount;
	
	private float _fStunTime;
	
	private int _nGameUpdateLogic; // 0 = start.  1 = normal. 2 = finish
	private int _nFallState; // 0 = falling; 1 = hold; 2 = falling
	private float _fStartFinishTime;
	private float _fStartFinishTotalTime;
	private float _fStartFinishBeginning;
	private float _fStartFinishMiddle;
	private float _fStartFinishEnd;
	float fStartFinishLamda;
	float fStartFinishPosX;
	float fStartFinishPosY;
	private CRectangle _Start;
	private CRectangle _Finish;
	
	private void SpawnObject()
	{
		// determine what object to spawn
		FallObjectInfo info;
		int nChance = Utilities.Random.nextInt(100);
		if(nChance < 7) // stun
		{
			info = _FallInfoTypes.get(5);
		}
		else if(nChance < 14) // bad
		{
			info = _FallInfoTypes.get(6);
		}
		else if(nChance < 65) // fruit
		{
			int nFruit = Utilities.Random.nextInt() & 0x3; // 0 - 3
			info = _FallInfoTypes.get(nFruit);
		}
		else if(nChance < 80) // Candy
		{
			info = _FallInfoTypes.get(3);
		}
		else // Cake
		{
			info = _FallInfoTypes.get(4);
		}
		
		// create the new object
		CCatchObject newObj = CNyanimalFactoy.RequestObject(CCatchObject.class);
		newObj.SetCatchInfo(new FallObjectInfo(info));
		newObj.SetActive(true);
		newObj.SetDraw(true);
		newObj.SetDrawLayer(Utilities.CommonLayers.ObjectHold + 1);
				
		// determine where to spawn the new object
		float nObjectSpawnMin = _Nyanimal.GetTransform().GetSize()[0] / 2.0f;
		float nObjectSpawnMax = Utilities.Screen.GetWidth() - nObjectSpawnMin;
		float nSpawnDelta = nObjectSpawnMax - nObjectSpawnMin;
		float nSpawnPosX = Utilities.Random.nextInt((int)nSpawnDelta) + nObjectSpawnMin;
		newObj.GetTransform().SetPosition(nSpawnPosX, 0);
		
		_CatchObjects.add(newObj);
	}
	
	private void RemoveObject(CCatchObject obj)
	{
		// give the object back
		_CatchObjects.remove(obj);
		CNyanimalFactoy.ReturnObject(obj);
	}

	@Override
    public void OnCreate()
    {
		_SensorManager = (SensorManager)Utilities.GameActivity.getSystemService(Context.SENSOR_SERVICE);
		_Accelerometer = _SensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		_CatchObjects = new LinkedList<CCatchObject>();
		_CatchObjects_Destroy = new LinkedList<CCatchObject>();
		
		// ensure we have all the dropping objects
		CTextureManager.GetInstance().PreloadTextures(R.drawable.apple, R.drawable.grapes, R.drawable.cherry, R.drawable.cupcake, R.drawable.cake, R.drawable.meatrotten);
		
		// create the types of objects that will fall
		_FallInfoTypes = new ArrayList<CCatchObject.FallObjectInfo>();
		_FallInfoTypes.add(new FallObjectInfo(R.drawable.apple, 1, 1.0f, 0.0f));
		_FallInfoTypes.add(new FallObjectInfo(R.drawable.grapes, 1, 1.0f, 0.0f));
		_FallInfoTypes.add(new FallObjectInfo(R.drawable.cherry, 1, 1.0f, 0.0f));
		_FallInfoTypes.add(new FallObjectInfo(R.drawable.candy, 2, 1.3f, 0.0f));
		_FallInfoTypes.add(new FallObjectInfo(R.drawable.cake, 3, 1.5f, 0.0f));
		_FallInfoTypes.add(new FallObjectInfo(R.drawable.meatrotten, 0, 1.0f, 2.0f));
		_FallInfoTypes.add(new FallObjectInfo(R.drawable.applecore, -1, 1.0f, 0.0f));
		
		// load our labels
		_Start = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Finish = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Score = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Time = CNyanimalFactoy.RequestObject(CRectangle.class);

		_Start.SetResourceID(R.drawable.catch_start);
		_Finish.SetResourceID(R.drawable.catch_finish);
		_Score.SetResourceID(R.drawable.catch_score);
		_Time.SetResourceID(R.drawable.catch_time);

		_Start.SetDrawLayer(Utilities.CommonLayers.ObjectHold + 1);
		_Finish.SetDrawLayer(Utilities.CommonLayers.ObjectHold + 1);
		_Score.SetDrawLayer(Utilities.CommonLayers.HudBase);
		_Time.SetDrawLayer(Utilities.CommonLayers.HudBase);
    }
	
	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
		_Catcher = (CCatcher)_PassthroughObjects[1];
	}
	
	@Override
	public void OnEnter()
	{
		_fGameTime = 29.9f;
		_nScore = 0;
		_fSpawnTime = 3.0f;
		_fCurSpawnTime = _fSpawnTime;
		_fSpawnTimeInc = 0.005f;	
		_AppleCount = 0;
		_GrapeCount = 0;
		_CherryCount = 0;
		_CandyCount = 0;
		_CakeCount = 0;	
		_StunCount = 0;
		_CoreCount = 0;
		_fStunTime = 0.0f;
		_nGameUpdateLogic = 0;
		
		_NumberDisplay = (CBitmapFont)CObjectBank.Retrieve("catch_numberfont");
		_NumberDisplay.SetFontSize(_fFontSize, _fFontSize);
		
		_Start.MakeScaledSize();
		_Finish.MakeScaledSize();
		_Score.MakeScaledSize();
		_Time.MakeScaledSize();
		
		_fStartFinishTotalTime = 0.75f;
		_fStartFinishTime = _fStartFinishTotalTime;
		_fStartFinishBeginning = _Start.GetTransform().GetSize()[1] * -2.0f;
		_fStartFinishMiddle = Utilities.Screen.GetHeight() / 2.0f;
		_fStartFinishEnd = Utilities.Screen.GetHeight() + (-_fStartFinishBeginning);
		_nFallState = 0;
		
		float fScale = 0.15f;
		_Score.GetTransform().Scale(fScale, fScale);
		_Time.GetTransform().Scale(fScale, fScale);

		float[] fScoreSize = _Score.GetTransform().GetSize();
		float[] fTimeSize = _Time.GetTransform().GetSize();
		_Score.GetTransform().SetPosition(Utilities.Screen.GetWidth() - (3.5f * fScoreSize[0]), fScoreSize[1]);
		_Time.GetTransform().SetPosition(fTimeSize[0] * 1.5f, fTimeSize[1]);
		
		_Start.GetTransform().SetPosition(0.0f, -1000.0f);
		_Finish.GetTransform().SetPosition(0.0f, -1000.0f);		
		
		_Score.SetDraw(true);
		_Score.SetActive(true);
	
		_Time.SetDraw(true);
		_Time.SetActive(true);	
		
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Set(new NyanimalPages.CatchGame_Play());
	}
	
	@Override 
	public void OnExit()
	{
		for(CCatchObject obj : _CatchObjects)
		{
			CNyanimalFactoy.ReturnObject(obj);
		}

		
		_CatchObjects.clear();
		
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Set(new NyanimalPages.CatchGame_End());
	}

	@Override
    public void OnPause()
    {
		for(CCatchObject obj : _CatchObjects)
		{
			obj.SetDraw(false);
			obj.SetActive(false);
		}
		
		_Start.SetDraw(false);
		_Start.SetActive(false);
		
		_Finish.SetDraw(false);
		_Finish.SetActive(false);
		
		_Score.SetDraw(false);
		_Score.SetActive(false);
	
		_Time.SetDraw(false);
		_Time.SetActive(false);		

		_SensorManager.unregisterListener((SensorEventListener)this);
		StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Unsubscribe(NyanimalPages.CatchGame_ObjectCaught.class, this);
    }

	@Override
    public void OnResume()
    {			
		for(CCatchObject obj : _CatchObjects)
		{
			obj.SetDraw(true);
			obj.SetActive(true);
		}
		
		_Start.SetDraw(true);
		_Start.SetActive(true);
		
		_Finish.SetDraw(true);
		_Finish.SetActive(true);
		
		_Score.SetDraw(true);
		_Score.SetActive(true);
	
		_Time.SetDraw(true);
		_Time.SetActive(true);	

		StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Subscribe(NyanimalPages.CatchGame_ObjectCaught.class, this);
		_SensorManager.registerListener((SensorEventListener)this, _Accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
	
	@Override
	public void ReadPage(IPage page)
	{
		if(page instanceof NyanimalPages.CatchGame_ObjectCaught)
		{
			NyanimalPages.CatchGame_ObjectCaught e = (NyanimalPages.CatchGame_ObjectCaught)page;
			
			// update the score
			CCatchObject obj = (CCatchObject)e.GetCaughtObject();
			_nScore += obj.GetPointValue();
			
			// update the count
			if(obj.GetResourceID() == _FallInfoTypes.get(0).ResourceID)
			{
				_AppleCount++;
			}
			else if(obj.GetResourceID() == _FallInfoTypes.get(1).ResourceID)
			{
				_GrapeCount++;
			}
			else if(obj.GetResourceID() == _FallInfoTypes.get(2).ResourceID)
			{
				_CherryCount++;
			}
			else if(obj.GetResourceID() == _FallInfoTypes.get(3).ResourceID)
			{
				_CandyCount++;
			}
			else if(obj.GetResourceID() == _FallInfoTypes.get(4).ResourceID)
			{
				_CakeCount++;
			}
			else if(obj.GetResourceID() == _FallInfoTypes.get(5).ResourceID)
			{
				_StunCount++;
			}
			else if(obj.GetResourceID() == _FallInfoTypes.get(6).ResourceID)
			{
				_CoreCount++;
			}
			
			// TODO: handle stun
			if(obj.ShouldStun())
			{
				Utilities.Log.Debug("CCatchGameUpdateState", "Stun for " + obj.GetStunTime() + " seconds!");
				_fStunTime = obj.GetStunTime();
				StoryBook.Chapter(NyanimalPages.Chapters.CatchGameHash).Set(new NyanimalPages.CatchGame_Stun(_fStunTime));
			}
			
			// remove
			_CatchObjects_Destroy.add(obj);
		}
		else
		{
			super.ReadPage(page);
		}
	}

	@Override
    public void PriorityUpdate()
    {
		switch(_nGameUpdateLogic)
		{
		case 0: // start
			break;
		case 1: // game update
			_fGameTime -= Utilities.Time.DeltaTime_Seconds();
			_fCurSpawnTime += Utilities.Time.DeltaTime_Seconds();
			
			if(_fCurSpawnTime >= _fSpawnTime)
			{
				SpawnObject();
				_fCurSpawnTime = 0.0f;
			}

			// remove destroyed objects
			for(CCatchObject obj : _CatchObjects_Destroy)
			{			
				// update spawn timers and spawn a new
				_fSpawnTime -= _fSpawnTimeInc;
				SpawnObject();
				
				// remove it
				RemoveObject(obj);
			}
			_CatchObjects_Destroy.clear();
			break;
		case 2: // finish
			break;
		}
    }
	
	@Override
    public void Update()
    {
		// draw all the text
		float[] pos = _Time.GetTransform().getPosition();
		float[] size = _Time.GetTransform().GetSize();
		float[] fontSize = _NumberDisplay.GetTransform().GetSize();
		_NumberDisplay.DrawText(" " + (int)(_fGameTime + 1.0f), pos[0] + size[0], pos[1] - (fontSize[1] * 0.8f));

		pos = _Score.GetTransform().getPosition();
		size = _Score.GetTransform().GetSize();
		_NumberDisplay.DrawText(" " + _nScore, pos[0] + size[0], pos[1] - (fontSize[1] * 0.8f));
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// update shtuff
		switch(_nGameUpdateLogic)
		{
		case 0: // start
			_fStartFinishTime -= Time.DeltaTime_Seconds();
			
			// position logic
			fStartFinishLamda = 1.0f - (_fStartFinishTime / _fStartFinishTotalTime);
			fStartFinishPosY = Utilities.Screen.GetWidth() - _Start.GetTransform().GetSize()[0];
			
			// handle moving
			switch(_nFallState)
			{
			case 0:
				fStartFinishPosX = _fStartFinishBeginning + ((_fStartFinishMiddle - _fStartFinishBeginning) * fStartFinishLamda);
				_Start.GetTransform().SetPosition(fStartFinishPosY, fStartFinishPosX);				
				break;
			case 1:
				break;
			case 2:
				fStartFinishPosX = _fStartFinishMiddle + ((_fStartFinishEnd - _fStartFinishMiddle) * fStartFinishLamda);
				_Start.GetTransform().SetPosition(fStartFinishPosY, fStartFinishPosX);
				break;
			}

			// move to the next (sub)sub state
			if(_fStartFinishTime <= 0.0f)
			{
				_nFallState++;
				_fStartFinishTime = _fStartFinishTotalTime;
				
				// next sub state
				if(_nFallState == 3)
				{
					_nGameUpdateLogic++;
					_nFallState = 0;
				}
			}
			break;
		case 1: // game update	    	
	    	// have all the spawned objects fall
			for(CCatchObject obj : _CatchObjects)
			{
				if(obj.IsOffscreen())
				{
					_CatchObjects_Destroy.add(obj);
				}
				else if(_fStunTime <= 0.0f)
				{
					// check collision
					RectF collisionResult = _Catcher.CheckCollision(obj);
					if(collisionResult != null)
					{
						// handle collision
						_Catcher.HandleCollision(obj, collisionResult);
					}
				}
			}
			break;
		case 2: // finish
			_fStartFinishTime -= Time.DeltaTime_Seconds();
			
			// position logic
			fStartFinishLamda = 1.0f - (_fStartFinishTime / _fStartFinishTotalTime);
			fStartFinishPosY = Utilities.Screen.GetWidth() - _Finish.GetTransform().GetSize()[0];
			
			// handle moving
			switch(_nFallState)
			{
			case 0:
				fStartFinishPosX = _fStartFinishBeginning + ((_fStartFinishMiddle - _fStartFinishBeginning) * fStartFinishLamda);
				_Finish.GetTransform().SetPosition(fStartFinishPosY, fStartFinishPosX);				
				break;
			case 1:
				break;
			case 2:
				fStartFinishPosX = _fStartFinishMiddle + ((_fStartFinishEnd - _fStartFinishMiddle) * fStartFinishLamda);
				_Finish.GetTransform().SetPosition(fStartFinishPosY, fStartFinishPosX);
				break;
			}

			// move to the next (sub)sub state
			if(_fStartFinishTime <= 0.0f)
			{
				_nFallState++;
				_fStartFinishTime = _fStartFinishTotalTime;
				
				// next state
				if(_nFallState == 3)
				{
					FSMLibrary.Chapter(CCatchFSMConstructor.CHAPTERHASH).ChangeState(CCatchGameOverState.class, _Nyanimal, _Catcher, _nScore, _AppleCount, _GrapeCount, _CherryCount, _CandyCount, _CakeCount, _StunCount, _CoreCount);
				}
			}
			break;
		}
    }

	@Override
    public void LateUpdate()
    {		
		switch(_nGameUpdateLogic)
		{
		case 0: // start
			break;
		case 1: // game update
			_fStunTime -= Utilities.Time.DeltaTime_Seconds();
			if(_fGameTime <= 0.0f)
			{
				// Game Over
				_nGameUpdateLogic++;
			}
			break;
		case 2: // finish
			break;
		}
    }

	@Override
    public void OnDestroy()
    {
		CNyanimalFactoy.ReturnObject(_Start);
		CNyanimalFactoy.ReturnObject(_Finish);
		CNyanimalFactoy.ReturnObject(_Score);
		CNyanimalFactoy.ReturnObject(_Time);
    }
	
    public void onAccuracyChanged(Sensor sensor, int accuracy) 
    {
    }

    public void onSensorChanged(SensorEvent event) 
    { 
    	StoryBook.Chapter(NyanimalPages.Chapters.AccelerometerHash).Set(new NyanimalPages.SensorChangedEvent(event));
    }

	@Override
    public RectF CheckCollision(CNyanObject inner)
    {
	    return null;
    }

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

}