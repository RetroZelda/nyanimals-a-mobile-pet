package com.retrozelda.nyanimals;

import android.graphics.RectF;

public class CInventorySlot extends CNyanObject
{
	CRectangle m_ItemSlotBG;
	CFood m_HeldFood;
	
	int m_nHeldCount;
	
	public CInventorySlot()
	{
		m_HeldFood = null;
	}
	
	@Override
	public void Init() 
	{
		m_ItemSlotBG = CNyanimalFactoy.RequestObject(CRectangle.class);
	}

	@Override
	public void Update() 
	{
		m_ItemSlotBG.Update();
	}


	@Override
	public void Destroy() 
	{
		CNyanimalFactoy.ReturnObject(m_ItemSlotBG);
	}

	@Override
	public RectF CheckCollision(CNyanObject inner) 
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap) 
	{
		
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{
		return false;
	}

	@Override
	public Type GetType() 
	{
		return Type.INVENTORYSLOT;
	}

	@Override
    public void OnGraphicsInit()
    {
    }

	@Override
    public void OnEnable()
    {
    }

	@Override
    public void OnDisable()
    {
    }

}
