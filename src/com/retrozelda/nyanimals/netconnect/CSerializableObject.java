package com.retrozelda.nyanimals.netconnect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.retrozelda.utilities.Utilities;

public class CSerializableObject
{	
	private HashMap<String, Object> _HashData;
	private JSONObject _JSON;
	
	private String _ObjectName;
	private boolean _bIsDirty;
	
	public CSerializableObject(String ObjName)
	{
		_ObjectName = ObjName;
		_HashData = new HashMap<String, Object>();
		_bIsDirty = true;
	}
	
	public static CSerializableObject BuildFromJSON(JSONObject JSON)
	{
		CSerializableObject toReturn = null;
		try
        {
			// get the object name
			Iterator<?> JSONKeys = JSON.keys();
			String name = (String)JSONKeys.next();
			
			// create the new object
			toReturn = new CSerializableObject(name);
			
			// load in the rest
			JSONObject JSONTable = JSON.getJSONObject(name);
	        
			// insert everything in the table
			toReturn._HashData.clear();
			JSONKeys = JSONTable.keys();
			while(JSONKeys.hasNext())
			{
				String curKey = (String)JSONKeys.next();
				toReturn._HashData.put(curKey, JSONTable.get(curKey));
			}
			
        } 
		catch (JSONException e)
        {
			// void anything read
			toReturn = null;
			
			// print the problem
	        e.printStackTrace();
        }

		return toReturn;
	}

	public static CSerializableObject[] BuildBatchFromJSON(JSONObject JSON)
	{
		ArrayList<CSerializableObject> toReturn = new ArrayList<CSerializableObject>();
		try
        {
			// get the object name
			Iterator<?> JSONNameKeys = JSON.keys();
			while(JSONNameKeys.hasNext())
			{
				// create the new object
				String name = (String)JSONNameKeys.next();
				CSerializableObject newJSON = new CSerializableObject(name);
				
				// load in the rest
				JSONObject JSONTable = JSON.getJSONObject(name);
		        
				// insert everything in the table
				newJSON._HashData.clear();
				Iterator<?> JSONKeys = JSONTable.keys();
				while(JSONKeys.hasNext())
				{
					String curKey = (String)JSONKeys.next();
					newJSON._HashData.put(curKey, JSONTable.get(curKey));
				}
				
				// add to return list
				toReturn.add(newJSON);
			}
			
        } 
		catch (JSONException e)
        {
			// void anything read
			toReturn = null;
			
			// print the problem
	        e.printStackTrace();
        }

		CSerializableObject[] returnArray = new CSerializableObject[toReturn.size()];
		returnArray = toReturn.toArray(returnArray);

		return returnArray;
	}
	
	protected static JSONObject Combine(JSONObject l, JSONObject r)
	{
		JSONObject ret = new JSONObject();

		Iterator<?> jsonIter0 = l.keys();
		Iterator<?> jsonIter1 = r.keys();
		
		// add the left one
		while(jsonIter0.hasNext())
		{
			String key = (String)jsonIter0.next();
			try
            {
                ret.put(key, l.get(key));
            } 
			catch (JSONException e)
            {
                e.printStackTrace();
            }
		}
		
		// add the right one
		while(jsonIter1.hasNext())
		{
			String key = (String)jsonIter1.next();
			try
            {
                ret.put(key, r.get(key));
            } 
			catch (JSONException e)
            {
                e.printStackTrace();
            }
		}
		
		// return
		return ret;
	}
	
	public static JSONObject Combine(CSerializableObject... Serialized)
	{
		JSONObject batched = new JSONObject();
		for(CSerializableObject JSON : Serialized)
		{
			batched = Combine(batched, JSON.BuildJSON());
		}			
		
		return batched;
	}
	
	protected Object GetObject(String szName)
	{
		return _HashData.get(szName);
	}

	public JSONObject GetJSON(String szName)
	{
		return (JSONObject)GetObject(szName);
	}
	
	/*
	public JSONArray GetJSONArray(String szName)
	{
		return (JSONArray)GetObject(szName);
	}	
	 */
	public String GetString(String szName)
	{
		return (String)GetObject(szName);
	}
	public boolean GetBool(String szName)
	{
		return ((Boolean)GetObject(szName)).booleanValue();
	}
	public int GetInt(String szName)
	{
		return ((Integer)GetObject(szName)).intValue();
	}
	public float GetFloat(String szName)
	{
		return ((Float)GetObject(szName)).floatValue();
	}
	public long GetLong(String szName)
	{
		return ((Long)GetObject(szName)).longValue();
	}
	public double GetDouble(String szName)
	{
		return ((Double)GetObject(szName)).doubleValue();
	}
	
	protected void AddObject(String szName, Object Value)
	{
		_HashData.put(szName, Value);
		_bIsDirty = true;
	}
	
	public <T extends JSONObject> void AddObject(String szName, T Value)
	{
		_HashData.put(szName, Value);
		_bIsDirty = true;
	}
	
	/*
	public <T extends JSONArray>void AddObject(String szName, T Value)
	{
		_HashData.put(szName, Value);
		_bIsDirty = true;
	}
	*/
	public void AddString(String szName, String szValue)
	{
		AddObject(szName, szValue);
	}
	public void AddBool(String szName, Boolean bValue)
	{
		AddObject(szName, bValue);
	}
	public void AddInt(String szName, Integer nValue)
	{
		AddObject(szName, nValue);
	}
	public void AddFloat(String szName, Float fValue)
	{
		AddObject(szName, fValue);
	}
	public void AddLong(String szName, Long lValue)
	{
		AddObject(szName, lValue);
	}
	public void AddDouble(String szName, Double dValue)
	{
		AddObject(szName, dValue);
	}
	
	public void FlushJSON()
	{
		_HashData.clear();
	}
	
	public void ReadJSON(JSONObject JSON)
	{
		// get the JSON for this object
		try
        {
			JSONObject JSONTable = JSON.getJSONObject(_ObjectName);
			if(JSONTable == null)
			{
				Utilities.Log.Error("CSerializableObject", "This JSON isnt for this object!  Name not found!");
				Utilities.Log.Error("CSerializableObject", JSON.toString());
				throw new NullPointerException("This JSON isnt for this object!  Name not found!");
			}
	        
			// insert everything in the table
			_HashData.clear();
			Iterator<?> JSONKeys = JSONTable.keys();
			while(JSONKeys.hasNext())
			{
				String curKey = (String)JSONKeys.next();
				_HashData.put(curKey, JSONTable.get(curKey));
			}
			
        } 
		catch (JSONException e)
        {
			// void anything read
			_HashData.clear();
			
			// print the problem
	        e.printStackTrace();
        }
		
		_bIsDirty = true;
	}

	public JSONObject BuildJSON()
	{
		if(_bIsDirty)
		{
			_JSON = new JSONObject();
			JSONObject JSONTable = new JSONObject();
			for(String hashKey : _HashData.keySet())
			{
				Object obj = _HashData.get(hashKey);
				try
	            {
		            JSONTable.put(hashKey, obj.toString());
	            } 
				catch (JSONException e)
	            {
		            e.printStackTrace();
	            }
			}
			
			try
	        {
		        _JSON.put(_ObjectName, JSONTable);
	        } 
			catch (JSONException e)
	        {
		        e.printStackTrace();
	        }
			_bIsDirty = false;
		}
		return _JSON;
	}

	
	@Override
	public String toString()
	{
		return BuildJSON().toString();
	}
}
