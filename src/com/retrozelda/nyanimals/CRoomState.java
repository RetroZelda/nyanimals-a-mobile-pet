package com.retrozelda.nyanimals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.graphics.RectF;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.CNyanObject.ObjStatus;
import com.retrozelda.nyanimals.nyanimal.CNyanimalStateConstructor;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.CObjectBank;
import com.retrozelda.utilities.Utilities;

public class CRoomState extends CNyanState
{
	private static final String	TAG	= CRoomState.class.getSimpleName();
	

	public class BrushTouch implements IButtonAction
	{

		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CBrushState.class);
		}
	}
	public class FeedTouch implements IButtonAction
	{

		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			
			m_bCreateFood = true;
			//FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CItemState.class);
			
		}
	}
	public class GamesTouch implements IButtonAction
	{

		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{	
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CGameSelectState.class, m_Nyanimal);
		}
	}
	public class StatTouch implements IButtonAction
	{
		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			
			Utilities.Log.Debug(TAG, "Stats Touched!");
			
			// TODO: Pass data to the status screen?
			// m_GameClass.PushState(m_StatusScreen);	
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CStatusScreenState.class);		
		}
	}
	public class WashTouch implements IButtonAction
	{

		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CWashState.class);	
		}
	}
	public class SleepTouch implements IButtonAction
	{

		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CSleepState.class);	
		}
	}
	public class ExitTouch implements IButtonAction
	{

		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease()  
		{
			Utilities.GameActivity.finish();
	        //StoryBook.Chapter(NyanimalPages.Chapters.Input).Mark(new NyanimalPages.BackButtonEvent(false));
		}
	}
	public class MedicineTouch implements IButtonAction
	{

		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			m_bCreateMedicine = true;
			//FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CMedicalState.class);	
		}
	}
	public class BattleTouch implements IButtonAction
	{

		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CBattleState.class);	
		}
	}
	
	private void CollisionCheck(CNyanObject left, CNyanObject right)
	{
		// check collision
		RectF collisionResult = left.CheckCollision(right);
		if(collisionResult != null)
		{
			// handle collision
			left.HandleCollision(right, collisionResult);
		}
		
		collisionResult = right.CheckCollision(left);
		if(collisionResult != null)
		{
			// handle collision
			right.HandleCollision(left, collisionResult);
		}
	}
	
	List<CNyanObject> 			m_ActiveObjects;
	
	CNyanimal					m_Nyanimal;
	CBackdrop					m_BackGround;
	CGarbage					m_Garbage;
	
	// UI
	CRoomHUD					m_HUD;
	
	// flags
	boolean m_bCreateFood;
	boolean m_bCreateMedicine;

	public CRoomState() 
	{
		super();	
		m_ActiveObjects = new ArrayList<CNyanObject>();
		
		
		m_bCreateFood = false;
		m_bCreateMedicine = false;
	}
	
	public void CreateFood()
	{
		CFood newFood = CNyanimalFactoy.RequestObject(CFood.class);
		newFood.SetDraw(true);
		newFood.SetActive(true);
		newFood.SetDrawLayer(Utilities.CommonLayers.Object);
		m_ActiveObjects.add(newFood);
		
		int nRandX = Utilities.Random.nextInt(Utilities.GLSurfaceView.getWidth() - 200) + 100;
		
		newFood.GetTransform().SetPosition(nRandX, 0);
	}
	
	public void CreateMedicine()
	{
		CMedicine newMed = CNyanimalFactoy.RequestObject(CMedicine.class);
		newMed.SetDraw(true);
		newMed.SetActive(true);
		newMed.SetDrawLayer(Utilities.CommonLayers.Object);
		m_ActiveObjects.add(newMed);
		
		int nRandX = Utilities.Random.nextInt(Utilities.GLSurfaceView.getWidth() - 200) + 100;
		
		newMed.GetTransform().SetPosition(nRandX, 0);
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{
		// handle all objects
		boolean bBusy = false;
		for(int nObjIndex = 0; nObjIndex < m_ActiveObjects.size(); ++nObjIndex)
		{
			CNyanObject curObj = m_ActiveObjects.get(nObjIndex);
			bBusy = curObj.HandleTouch(nPosX, nPosY, nTouchAction);	
			
			if(bBusy == true)
			{	
				// move this food to the "front"
				m_ActiveObjects.remove(curObj);
				m_ActiveObjects.add(curObj);
				break;
			}
		}
		
		// handle the buttons if not dragging food
		if(bBusy == false)
		{
			m_HUD.HandleTouch(nPosX, nPosY, nTouchAction);
		}
		
		return true;
	}

	@Override
	public RectF CheckCollision(CNyanObject inner) {
		
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap) {
		
		
	}

	@Override
    public void OnCreate()
    {
		Utilities.Log.Debug(TAG, "Init Background");
		
		/*
		CButton dummyButton = CNyanimalFactoy.RequestObject(CButton.class);
		dummyButton.SetButtonInformation(Utilities.Screen.GetWidth() / 2, Utilities.Screen.GetHeight() / 2, 
				64, 64, R.drawable.battle_button, R.drawable.battle_button_pressed);
		*/
		
		m_BackGround = CNyanimalFactoy.RequestObject(CBackdrop.class);
		m_Garbage = CNyanimalFactoy.RequestObject(CGarbage.class);
		m_HUD = CNyanimalFactoy.RequestObject(CRoomHUD.class);

		
		m_BackGround.SetBackdropInformation(R.drawable.room_0, R.drawable.none);


		m_BackGround.SetDraw(false);
		m_Garbage.SetDraw(false);
		m_HUD.SetDraw(false);		
		m_BackGround.SetDraw(false);		

		m_HUD.SetActive(false);	
		m_Garbage.SetActive(false);	
		
		Utilities.Log.Debug(TAG, "Room init complete");
		
    }
	
	@Override
	public void OnEnter()
	{
		m_Nyanimal = (CNyanimal) CObjectBank.Retrieve("Nyanimal");
		
//		Log.d(TAG, "Init bow");
//		CNyanimalAccessory blueBow =  CNyanimalFactoy.RequestObject(CNyanimalAccessory.class);
//		float fRenderTime = 1.0f / 24.0f; //24 fps
//		_AnimationTextureLink bowRock = new _AnimationTextureLink("rock", R.drawable.bowsprite_rockin, 128, 128, 5, 5, fRenderTime, true);
//		_AnimationTextureLink bowEat = new _AnimationTextureLink("eat", R.drawable.bowsprite_bite, 128, 128, 4, 4, fRenderTime, true);
//		_AnimationTextureLink bowIdle = new _AnimationTextureLink("idle", R.drawable.bowsprite_zzz_idle, 128, 128, 5, 5, fRenderTime, true);
//
//		blueBow.AddAnimation(bowIdle, NyanimalAnimations.IDLE);
//		blueBow.AddAnimation(bowIdle, NyanimalAnimations.HUNGRY);
//		blueBow.AddAnimation(bowIdle, NyanimalAnimations.SICK);
//		blueBow.AddAnimation(bowIdle, NyanimalAnimations.SLEEPING);
//		blueBow.AddAnimation(bowEat, NyanimalAnimations.EATING);
//		blueBow.AddAnimation(bowRock, NyanimalAnimations.WALKING_UP);
//		blueBow.AddAnimation(bowRock, NyanimalAnimations.WALKING_DOWN);
//		blueBow.AddAnimation(bowRock, NyanimalAnimations.WALKING_LEFT);
//		blueBow.AddAnimation(bowRock, NyanimalAnimations.WALKING_RIGHT);		
		
		Utilities.Log.Debug(TAG, "Init Nyanimal");
		//m_Nyanimal.AddAccessory(blueBow);
		
		m_HUD.SetWatchObject(this, m_Nyanimal);		
		m_ActiveObjects.add(m_Nyanimal);
		m_ActiveObjects.add(m_Garbage);	
		
		m_Nyanimal.GetTransform().SetPosition((int)(0.5f * Utilities.Screen.GetWidth()), (int)(0.75f * Utilities.Screen.GetHeight()));
	}

	@Override
	public void OnPause()
	{
		m_Nyanimal.SetDraw(false);
		m_BackGround.SetDraw(false);
		m_Garbage.SetDraw(false);
		m_HUD.SetDraw(false);		
		m_BackGround.SetDraw(false);
		
		m_Nyanimal.SetActive(false);
		m_HUD.SetActive(false);	
		m_Garbage.SetActive(false);	
		
		for(CNyanObject obj : m_ActiveObjects)
		{
			obj.SetActive(false);
			((IRenderObject)obj).SetDraw(false);
		}
	}
	
	@Override
	public void OnResume()
	{
		m_Nyanimal.SetDraw(true);
		m_BackGround.SetDraw(true);
		m_Garbage.SetDraw(true);
		m_HUD.SetDraw(true);		
		m_BackGround.SetDraw(true);

		m_Nyanimal.SetActive(true);
		m_HUD.SetActive(true);	
		m_Garbage.SetActive(true);		

		for(CNyanObject obj : m_ActiveObjects)
		{
			obj.SetActive(true);
			((IRenderObject)obj).SetDraw(true);
		}
	}

	@Override
    public void PriorityUpdate()
    {
	    
	    
    }

	@Override
    public void Update()
    {		
		// TODO: Use Storybook and object manager
		if(m_bCreateFood == true)
		{
			CreateFood();
			m_bCreateFood = false;
		}

		// TODO: Use Storybook and object manager
		if(m_bCreateMedicine == true)
		{
			CreateMedicine();
			m_bCreateMedicine = false;
		}
		
		// update all the active objects
		for(int nObjIndex = 0; nObjIndex < m_ActiveObjects.size(); ++nObjIndex)
		{
			CNyanObject curObj = m_ActiveObjects.get(nObjIndex);
			
			// remove if necessary
			if(curObj.GetObjectStatus() == ObjStatus.TO_DELETE)
			{
				CNyanimalFactoy.ReturnObject(m_ActiveObjects.remove(nObjIndex));				
				nObjIndex--;
			}
		}

		// TODO: Do this after Update() and before LateUpdate() in the states, but for all active objects
		// check collisions
		for(CNyanObject inner : m_ActiveObjects)
		{
			for(CNyanObject outer : m_ActiveObjects)
			{
				if(inner == outer)
					continue;
				CollisionCheck(inner, outer);
			}
		}
		
    }

	@Override
    public void LateUpdate()
    {
	    	    
    }

	@Override
    public void OnDestroy()
    {
		// TODO: Save Nyanimal on ALL pauses, and load him in in ALL resumes
		m_Nyanimal.SaveNyanimal(Utilities.GLSurfaceView.getContext());

		// TODO: dont return nyanimal here.
		CNyanimalFactoy.ReturnObject(m_Nyanimal);
		CNyanimalFactoy.ReturnObject(m_BackGround);
		CNyanimalFactoy.ReturnObject(m_Garbage);
		CNyanimalFactoy.ReturnObject(m_HUD);
		
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Unsubscribe(NyanimalPages.SurfaceChangeEvent.class, this);		
    }
}
