package com.retrozelda.nyanimals;

import org.apache.http.HttpResponse;

import android.hardware.SensorEvent;
import android.view.MotionEvent;

import com.retrozelda.storybook.IPage;

public class NyanimalPages
{
	public static class Chapters
	{
		public static String Input = "NyanimalInputChapter";
		public static String Render = "NyanimalRenderChapter";
		public static String CatchGame = "NyanimalCatchMiniGame";
		public static String Accelerometer = "AccelerometerChapter";

		public static int InputHash = Input.hashCode();
		public static int RenderHash = Render.hashCode();
		public static int CatchGameHash = CatchGame.hashCode();
		public static int AccelerometerHash = Accelerometer.hashCode();
	}
	
	public static class TransformUpdatedEvent implements IPage
	{
		private CTransform _Transform;
		public TransformUpdatedEvent(CTransform transform)
		{
			_Transform = transform;
		}
		
		public CTransform Transform() { return _Transform;}
	}

	public static class SurfaceChangeEvent implements IPage
	{
		private int _Width;
		private int _Height;
		public SurfaceChangeEvent(int width, int height)
		{
			_Width = width;
			_Height = height;
		}
		
		public int Width() { return _Width;}
		public int Height() { return _Height;}
	}
	
	public static class ScreenTouchEvent implements IPage
	{
		private int _X;
		private int _Y;
		private int _Action;
		public ScreenTouchEvent(int x, int y, int action)
		{
			_X = x;
			_Y = y;
			_Action = action;
		}
		
		public int X() { return _X;}
		public int Y() { return _Y;}
		public int Action() { return _Action;}
	}
	
	public static class AndroidMotionEvent implements IPage
	{
		private MotionEvent _Motion;
		public AndroidMotionEvent(MotionEvent motion)
		{
			_Motion = motion;
		}
		
		public MotionEvent Motion() { return _Motion;}
	}
	
	public static class BackButtonEvent implements IPage
	{
		private boolean _HardwareKey;
		public BackButtonEvent(boolean bHardwareKey)
		{
			_HardwareKey = bHardwareKey;
		}
		
		public boolean HardwareKey() { return _HardwareKey;}
	}
	
	public static class FinishedOpenGLInitEvent implements IPage
	{
		// NOTE: Should normally be the same object...
		private CNyanObject _NyanimalObject;
		private IRenderObject _RenderObject;
		
		public FinishedOpenGLInitEvent(CNyanObject nyan, IRenderObject render)
		{
			_NyanimalObject = nyan;
			_RenderObject = render;
		}
		

		public CNyanObject GetNyanObject() { return _NyanimalObject; }
		public IRenderObject GetRenderObject() { return _RenderObject; }
	}

	public static class RenderListCreateRunEvent implements IPage
	{
		public RenderListCreateRunEvent()
		{
		}
	}

	public static class RenderListDestroyRunEvent implements IPage
	{
		public RenderListDestroyRunEvent()
		{
		}
	}

	public static class SensorChangedEvent implements IPage
	{
		private SensorEvent _EventData;
		public SensorEvent GetEventData() { return _EventData;}
		
		public SensorChangedEvent(SensorEvent event)
		{
			_EventData = event;
		}
	}

	public static class CatchGame_Start implements IPage
	{
		public CatchGame_Start()
		{
		}
	}

	public static class CatchGame_Play implements IPage
	{
		public CatchGame_Play()
		{
		}
	}
	
	public static class CatchGame_End implements IPage
	{
		public CatchGame_End()
		{
		}
	}

	public static class CatchGame_ObjectCaught implements IPage
	{
		private CNyanObject _Obj;
		public CNyanObject GetCaughtObject() { return _Obj;}
		
		public CatchGame_ObjectCaught(CNyanObject obj)
		{
			_Obj = obj;
		}
	}
	
	public static class CatchGame_Stun implements IPage
	{
		private float _fStunTime;
		public float GetStunTime() { return _fStunTime;}
		
		public CatchGame_Stun(float fStunTime)
		{
			_fStunTime = fStunTime;
		}
	}
	
	public static class WebThread_Response implements IPage
	{
		private int _nUniqueID;
		private HttpResponse _Response;
		
		public int GetUniqueID() { return _nUniqueID;}
		public HttpResponse GetResponse() { return _Response;}
		
		public WebThread_Response(int nUniqueID, HttpResponse response)
		{
			_nUniqueID = nUniqueID;
			_Response = response;
		}		
	}
}





















