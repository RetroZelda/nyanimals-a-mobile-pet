package com.retrozelda.nyanimals;

import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;


public class CBackdrop extends CNyanObject
{
	CRectangle m_BackGround;
	CRectangle m_Corner;

	float fScrollX;
	float fScrollY;
	
	float fScaleX;
	float fScaleY;
	
	public void SetScroll(float fX, float fY)
	{
		fScrollX = fX;
		fScrollY = fY;
	}
	public void SetUVScale(float fX, float fY)
	{
		fScaleX = fX;
		fScaleY = fY;
		m_BackGround.UVScale(fScaleX, fScaleY);
	}
	
	public CBackdrop()
	{		
		fScrollX = 0.0f;
		fScrollY = 0.0f;
	}

	public void SetDraw(boolean b)
    {
		m_BackGround.SetDraw(b);
		m_Corner.SetDraw(b);
    }
	
	@Override
	public void Init() 
	{
		m_BackGround = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_Corner = CNyanimalFactoy.RequestObject(CRectangle.class);
	}
	
	public void SetBackdropInformation(int nBackground, int nCorner)
	{
		// int nScreenMiddle = Utilities.Screen.GetWidth() / 2;
		int nScreenWidth = Utilities.Screen.GetWidth();
		// int nScreenHeight = Utilities.Screen.GetHeight();
		
		m_BackGround.SetResourceID(nBackground);
		m_BackGround.GetTransform().SetPosition((int)(0.5f * Utilities.Screen.GetWidth()), (int)(0.5f * Utilities.Screen.GetHeight()));
		m_BackGround.GetTransform().SetSize(Utilities.Screen.GetWidth() / 2, Utilities.Screen.GetHeight() / 2); 
		m_BackGround.SetDrawLayer(Utilities.CommonLayers.Background);
		
		m_Corner.SetResourceID(nCorner);
		float[] nCornerSize = m_Corner.GetTransform().GetSize();
		float nCornerWidth = nScreenWidth / 4.0f;
		float nCornerHeight = nCornerWidth * (nCornerSize[0] / nCornerSize[1]);
		m_Corner.GetTransform().SetSize(nCornerWidth, nCornerHeight);
		m_Corner.GetTransform().SetPosition(nCornerWidth, nCornerHeight);
		m_Corner.SetDrawLayer(Utilities.CommonLayers.HudBase);
	}

	@Override
    public void Update()
	{
		if(fScrollX != 0.0f || fScrollY != 0.0f)
		{
			m_BackGround.ScrollUVs(fScrollX * Utilities.Time.DeltaTime_Seconds(), fScrollY * Utilities.Time.DeltaTime_Seconds());
		}
	}

	@Override
	public void Destroy() 
	{		
		
	}

	@Override
	public RectF CheckCollision(CNyanObject inner) 
	{		
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap) 
	{		
		
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{		
		return false;
	}

	@Override
	public Type GetType() 
	{		
		return Type.BACKDROP;
	}
	@Override
    public void OnGraphicsInit()
    {
    }
	@Override
    public void OnEnable()
    {
    }
	@Override
    public void OnDisable()
    {
    }
}
