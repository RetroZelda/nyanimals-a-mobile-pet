package com.retrozelda.nyanimals;

import com.facebook.AppEventsLogger;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

public class CMainActivity extends FragmentActivity
{
    CMainFragment _MainFragment;
    
	public CMainActivity()
	{
	}

	public void onPlayNowButtonClick(View v) 
	{
        Toast.makeText(this, "Launching Game", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, CGameActivity.class);
        
        // TODO: pass any info to the new activity here
        startActivity(intent);
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		if (savedInstanceState == null) 
	    {
	        // Add the fragment on initial activity setup
			_MainFragment = new CMainFragment();
	        getSupportFragmentManager().beginTransaction().add(android.R.id.content, _MainFragment).commit();
	    } 
		else 
		{
	        // Or set the fragment from restored state info
	    	_MainFragment = (CMainFragment)getSupportFragmentManager().findFragmentById(android.R.id.content);
	    }
	}
		
	@Override
	protected void onStart()
	{
		super.onStart();
	}

	@Override
	protected void onStop()
	{
		super.onStop();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		
		// Logs 'app deactivate' App Event.
		AppEventsLogger.deactivateApp(this);
	}

	@Override
	protected void onResume()
	{
		super.onResume(); 

		// Logs 'install' and 'app activate' App Events.
		AppEventsLogger.activateApp(this);
	}
}
