package com.retrozelda.nyanimals.catchgame;

import android.graphics.RectF;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.CBitmapFont;
import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimalFactoy;
import com.retrozelda.nyanimals.NyanimalPages;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.CObjectBank;
import com.retrozelda.utilities.Utilities;

// TODO: Do an intro sequence or countdown here
public class CCatchGameIntroState extends CNyanState
{
	private float _CountdownTime;
	private CNyanimal _Nyanimal;
	private CCatcher _Catcher;
	private CBitmapFont _NumberDisplay;
	private float _fFontSize = 32.0f;
	
	@Override
    public void OnCreate()
    {
    }
	
	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
		_Catcher = (CCatcher)_PassthroughObjects[1];
	}
	
	@Override
	public void OnEnter()
	{
		_CountdownTime = 5.0f;
		
		_NumberDisplay = (CBitmapFont)CObjectBank.Retrieve("catch_numberfont");
		_NumberDisplay.SetFontSize(_fFontSize, _fFontSize);
		
		
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Set(new NyanimalPages.CatchGame_Start());
	}

	@Override
    public void OnPause()
    {
    }

	@Override
    public void OnResume()
    {			
    }


	@Override
    public void PriorityUpdate()
    {
		_CountdownTime -= Utilities.Time.DeltaTime_Seconds();
    }

	@Override
    public void Update()
    {
		float fOffset = _fFontSize / 2.0f;
		_NumberDisplay.DrawText("" + (int)(_CountdownTime + 1.0f), (Utilities.Screen.GetWidth() / 2) - fOffset, (Utilities.Screen.GetHeight() / 2) - fOffset);
    }

	@Override
    public void LateUpdate()
    {
		if(_CountdownTime <= 0.0f)
		{
			// TODO: Pass objects to the game state as needed
			FSMLibrary.Chapter(CCatchFSMConstructor.CHAPTERHASH).ChangeState(CCatchGameUpdateState.class, _Nyanimal, _Catcher);
		}
    }

	@Override
    public void OnDestroy()
    {
    }

	@Override
    public RectF CheckCollision(CNyanObject inner)
    {
	    return null;
    }

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

}