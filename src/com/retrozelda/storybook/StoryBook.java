package com.retrozelda.storybook;

import java.util.*;
public class StoryBook 
{
	// our data
	private HashMap<Integer, Chapter> _Chapters;
		
	// singleton
	private static StoryBook Instance = new StoryBook();	
	private StoryBook()
	{
		_Chapters = new HashMap<Integer, Chapter>();
	}
	/////////////////////////////////////////////////////////
	
	// private functions
	private Chapter GetChapter(int nID)
	{
		if(_Chapters.containsKey(nID) == false)
		{
			_Chapters.put(nID, new Chapter(nID));
		}
		return _Chapters.get(nID);
	}

	public void Process_Internal()
	{
		for(Chapter chapter : _Chapters.values())
		{
			chapter.Process();
		}
	}
	
	// public static bridge functions
	public static Chapter Chapter(int nID)
	{
		return Instance.GetChapter(nID);
	}
	
	public static Chapter Chapter(String szID)
	{
		return Instance.GetChapter(szID.hashCode());
	}
	
	public static void Process()
	{
		Instance.Process_Internal();
	}
	
	// utility functions
	public static void Print(String szText)
	{
        System.out.println(szText);
	}
}
