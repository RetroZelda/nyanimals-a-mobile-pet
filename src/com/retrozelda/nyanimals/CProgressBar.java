package com.retrozelda.nyanimals;

import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;

public class CProgressBar extends CNyanObject
{
	public enum BarDirection {FillUp, FillDown, FillLeft, FillRight, NoFill};
	
	CTransform _Transform;
	CRectangle m_BackgroundBar;
	CRectangle m_ForegroundBar;
	
	int m_maxValue;
	int m_curValue;
	float m_percent;
	
	BarDirection m_Direction;
	
	public void SetFillDirection(BarDirection dir)
	{
		m_Direction = dir;
	}

	public CRectangle GetBackground()
	{
		return m_BackgroundBar;		
	}
	
	public CRectangle GetForeground()
	{
		return m_ForegroundBar;		
	}
		
	public CProgressBar()
	{
		_Transform = new CTransform();
		m_Direction = BarDirection.FillRight;
	}
		
	public void SetCurrentValue(int val)
	{
		m_curValue = val;
	}

	public void SetMaxValue(int val)
	{
		m_maxValue = val;
	}

	public int GetPercent()
	{
		return (int)(m_percent * 100.0f);
	}

	public void SetDraw(boolean b)
    {
		m_ForegroundBar.SetDraw(b);
		m_BackgroundBar.SetDraw(b);
    }
	
	// NOTE: posX and posY are 0 -> 1 to be on screen
	public void SetBarInformation(int frontTexture, int backTexture, int posX, int posY, int width, int height, int maxValue, int curValue)
	{
		m_maxValue = maxValue;
		m_curValue = curValue;
		m_ForegroundBar.SetResourceID(frontTexture);
		m_BackgroundBar.SetResourceID(backTexture);
		_Transform.SetSize(width, height);
		_Transform.SetPosition(posX, posY);
	}
	
	@Override
	public void Init()
	{
		m_BackgroundBar = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_ForegroundBar = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_ForegroundBar.GetTransform().SetParent(_Transform);
		m_BackgroundBar.GetTransform().SetParent(_Transform);

		m_ForegroundBar.SetDrawLayer(Utilities.CommonLayers.HudBarFront);
		m_BackgroundBar.SetDrawLayer(Utilities.CommonLayers.HudBarBack);
	}

	@Override
	public void Update()
	{		
		if(m_curValue > m_maxValue)
		{
			m_curValue = m_maxValue;
			m_percent = 1.0f;
		}
		else if(m_curValue < 0)
		{
			m_curValue = 0;
			m_percent = 0;
		}
		else
		{
			m_percent = (float)m_curValue / (float)m_maxValue;
		}
	}
	
	@Override 
	public void LateUpdate()
	{
		// set the position
		CRectangle fillBar = m_BackgroundBar;
		
		float[] size = _Transform.GetSize();
		float[] pos = _Transform.getPosition();
		float nWidth = 0;
		float nHeight = 0;
		float nPosX = 0;
		float nPosY = 0;
		switch(m_Direction)
		{
		case FillUp:
			nWidth = size[0];
			nHeight = size[1] * m_percent;
			nPosX = pos[0];
			nPosY = ((pos[1] - (m_percent)) / m_percent);
			break;
		case FillDown:
			nWidth = size[0];
			nHeight = size[1] * m_percent;
			nPosX = pos[0];
			nPosY = ((pos[1] - (pos[1] * m_percent)));// * m_percent);
			break;
		case FillLeft:
			nWidth = size[0] * m_percent;
			nHeight = size[1];
			nPosX = pos[0]  - nWidth;
			nPosY = pos[1] + size[1] / 2.0f;
			break;
		case FillRight:
			nWidth = size[0] * m_percent;
			nHeight = size[1];
			nPosX = pos[0]  + nWidth;
			nPosY = pos[1] + size[1] / 2.0f;	
			break;
		case NoFill:
			nWidth = size[0];
			nHeight = size[1];
			nPosX = pos[0];
			nPosY = pos[1];
			break;
		}
		fillBar.GetTransform().SetPosition((int)nPosX, (int)nPosY);		
		fillBar.GetTransform().SetSize((int)nWidth, (int)nHeight);
		////////////////////////////////////////
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{		
		return false;
	}

	@Override
	public Type GetType() 
	{		
		return Type.PROGRESSBAR;
	}

	@Override
	public RectF CheckCollision(CNyanObject inner) {
		
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap) {
		
		
	}

	@Override
	public void Destroy() {
		
		
	}

	@Override
    public void OnGraphicsInit()
    {
    }

	@Override
    public void OnEnable()
    {
    }

	@Override
    public void OnDisable()
    {
    }

}
