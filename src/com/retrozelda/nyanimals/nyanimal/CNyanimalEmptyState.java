package com.retrozelda.nyanimals.nyanimal;

import android.graphics.RectF;

import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;

public class CNyanimalEmptyState extends CNyanState
{
	CNyanimal _Nyanimal;

	@Override
	public void OnCreate()
	{
	}

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
	}
	
	@Override
	public void PriorityUpdate()
	{
	}

	@Override
	public void Update()
	{
	}

	@Override
	public void LateUpdate()
	{
	}

	@Override
	public void OnDestroy()
	{
	}

	@Override
	public RectF CheckCollision(CNyanObject inner)
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
	{
		return false;
	}

}
