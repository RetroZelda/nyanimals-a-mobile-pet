package com.retrozelda.nyanimals;

import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;
import android.view.MotionEvent;


public class CButton extends CNyanObject
{	
	CRectangle m_UpRect;
	CRectangle m_PressedRect;
	boolean m_bPrevTouchState	= false; // false = up.  true = pressed
	boolean m_bTouchState		= false; // false = up.  true = pressed

	CTransform _Transform;
	IButtonAction m_Action;	

	public CRectangle GetUpRect()
	{
		return m_UpRect;
		
	}
	
	public CRectangle GetPressedRect()
	{
		return m_PressedRect;
		
	}	
	
	public void SetAction(IButtonAction act)
	{
		m_Action = act;
	}	
	
	public boolean GetTouchState()
	{
		return m_bTouchState;
	}
	
	public void SetTouchState(boolean bTouch)
	{
		m_bTouchState = bTouch;
		UpdateVisibility(m_bTouchState);
	}
	
	public void ToggleTouchState()
	{
		SetTouchState(!m_bTouchState);
	}

	public CTransform GetTransform()
    {
	    return _Transform;
    }
	
	public void MakeScaledSize()
	{
		// screen info
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		
		// calculate size
		GetUpRect().MakeTextureSizeLocal();
		float[] nArrowSize = GetUpRect().GetTransform().GetLocalSize();
		float fArrowAspect = nArrowSize[0] / nArrowSize[1];
		nArrowSize[0] = (nScreenWidth * (nArrowSize[0] / 1080.0f)) / 2;
		nArrowSize[1] = nArrowSize[0] / fArrowAspect;
		
		// set the rect transforms to default size
		GetUpRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		GetPressedRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		
		// set button size
		GetTransform().SetSize(nArrowSize[0], nArrowSize[1]);
	}
	
	public CButton()
	{
		_Transform = new CTransform();
	}

	@Override
	public void Init() 
	{
		m_UpRect = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_PressedRect = CNyanimalFactoy.RequestObject(CRectangle.class);

		m_UpRect.GetTransform().SetParent(_Transform);
		m_PressedRect.GetTransform().SetParent(_Transform);

		m_UpRect.SetDraw(false);
		m_PressedRect.SetDraw(false);

		m_UpRect.SetDrawLayer(Utilities.CommonLayers.HudItem);
		m_PressedRect.SetDrawLayer(Utilities.CommonLayers.HudItem);
		
	}
	
	@Override
	public void Destroy() 
	{
		m_UpRect.GetTransform().SetParent(null);
		m_PressedRect.GetTransform().SetParent(null);
		CNyanimalFactoy.ReturnObject(m_UpRect);
		CNyanimalFactoy.ReturnObject(m_PressedRect);		
	}
	
	public void SetButtonInformation(int nPosX, int nPosY, int nSizeX, int nSizeY, int nUpResourceID, int nPressedResourceID)
	{
		_Transform.SetPosition(nPosX, nPosY);
		_Transform.SetSize(nSizeX, nSizeY);

		m_UpRect.SetResourceID(nUpResourceID);
		m_PressedRect.SetResourceID(nPressedResourceID);
		UpdateVisibility(false);

	}
	
	private void UpdateVisibility(boolean bTouch)
	{
		m_UpRect.SetDraw(!bTouch);
		m_PressedRect.SetDraw(bTouch);
	}


	@Override
	public void Update() 
	{
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{
		RectF rectToCheck = null;
		if(m_UpRect.IsDrawing() == true)
		{
			rectToCheck = m_UpRect.GetBoundingRect();
		}
		else
		{
			rectToCheck = m_PressedRect.GetBoundingRect();		
		}
		m_bPrevTouchState = m_bTouchState;
		m_bTouchState = rectToCheck.contains(nPosX, nPosY);

		switch (nTouchAction)
		{
		case MotionEvent.ACTION_UP:
			if(m_bTouchState)
			{
				m_Action.HandleRelease();
			}		
			UpdateVisibility(false);
			break;
		case MotionEvent.ACTION_MOVE:		
			UpdateVisibility(m_bTouchState);
			break;
		case MotionEvent.ACTION_DOWN:			
			UpdateVisibility(m_bTouchState);
			break;
		}
		return m_bTouchState;
	}

	@Override
	public Type GetType() 
	{
		return Type.BUTTON;
	}

	@Override
	public RectF CheckCollision(CNyanObject inner) 
	{		
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap) 
	{		
		
	}

	@Override
    public void OnGraphicsInit()
    {
    }

	@Override
	public void SetActive(boolean bActive)
	{
		super.SetActive(bActive);
	}
	
	public void SetDraw(boolean b)
    {
		if(b)
		{
			UpdateVisibility(!b);
		}
		else
		{
			m_PressedRect.SetDraw(b);
			m_UpRect.SetDraw(b);
		}
    }

	@Override
    public void OnEnable()
    {
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Subscribe(NyanimalPages.ScreenTouchEvent.class, this);
    }

	@Override
    public void OnDisable()
    {
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Unsubscribe(NyanimalPages.ScreenTouchEvent.class, this);
    }
}
