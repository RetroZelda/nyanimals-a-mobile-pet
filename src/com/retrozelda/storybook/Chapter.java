package com.retrozelda.storybook;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;

public class Chapter 
{
	// variables
	private HashMap<Class<? extends IPage>, ArrayList<IReader>> _PageSubscriptions;
	private ArrayDeque<IPage> _PageQueue;
	private int _ID;

	public int getID() { return _ID; }
	public void setID(int _ID) { this._ID = _ID; }

	// Private methods
	private void ChapterCreate(int nID)
	{
		_PageSubscriptions = new HashMap<Class<? extends IPage>, ArrayList<IReader>>();
		_PageQueue = new ArrayDeque<IPage>();
		_ID = nID;
		StoryBook.Print("Chapter Created: " + nID);
	}
	
	// public methods
	public Chapter(String szID)
	{
		ChapterCreate(szID.hashCode());
	}
	public Chapter(int nID)
	{
		ChapterCreate(nID);
	}

	// listed for a page
	public void Subscribe(Class<? extends IPage> pageType, IReader reader)
	{
		// create the new list if not already in existence
		if(_PageSubscriptions.containsKey(pageType) == false)
		{
			_PageSubscriptions.put(pageType, new ArrayList<IReader>());
		}
		
		// add
		_PageSubscriptions.get(pageType).add(reader);
		StoryBook.Print(reader.toString() + " Subscribed to: " + _PageSubscriptions.get(pageType).toString());
	}
	
	// stop listening for a page
	public void Unsubscribe(Class<? extends IPage> pageType, IReader reader)
	{
		// remove
		_PageSubscriptions.get(pageType).remove(reader);
		StoryBook.Print(reader.toString() + " Unsubscribed from: " + _PageSubscriptions.get(pageType).toString());
	}
	
	// distribute new page to all readers now
	public void Set(IPage page)
	{
		// send to all readers
		if(_PageSubscriptions.containsKey(page.getClass()))
		{
			ArrayList<IReader> readers = _PageSubscriptions.get(page.getClass());
			for(int nReaderIndex = 0; nReaderIndex < readers.size(); ++nReaderIndex)
			{
				readers.get(nReaderIndex).ReadPage(page);
			}
		}
	}
	
	// mark new page to be distributed soon
	public void Mark(IPage page)
	{
		_PageQueue.addLast(page);
	}
	
	// process each queued page
	public void Process()
	{
		while(_PageQueue.size() > 0)
		{
			Set(_PageQueue.pollFirst());
		}
	}
}
