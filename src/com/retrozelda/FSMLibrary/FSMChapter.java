package com.retrozelda.FSMLibrary;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.HashMap;

import com.retrozelda.FSMLibrary.FSMLibrary.RunningState;

public class FSMChapter 
{
	// exceptions
	public class ChapterAlreadyRunningException extends Exception
	{
        private static final long serialVersionUID = 4947003576142110164L;
		public ChapterAlreadyRunningException() { super(); }
		public ChapterAlreadyRunningException(String message) { super(message); }
		public ChapterAlreadyRunningException(String message, Throwable cause) { super(message, cause); }
		public ChapterAlreadyRunningException(Throwable cause) { super(cause); }
	}
	public class ChapterNotRunningException extends Exception
	{
        private static final long serialVersionUID = -7213719635730224069L;
		public ChapterNotRunningException() { super(); }
		public ChapterNotRunningException(String message) { super(message); }
		public ChapterNotRunningException(String message, Throwable cause) { super(message, cause); }
		public ChapterNotRunningException(Throwable cause) { super(cause); }
	}
	public class StartingChapterNotSetException extends Exception
	{
        private static final long serialVersionUID = -7213719635730224069L;
		public StartingChapterNotSetException() { super(); }
		public StartingChapterNotSetException(String message) { super(message); }
		public StartingChapterNotSetException(String message, Throwable cause) { super(message, cause); }
		public StartingChapterNotSetException(Throwable cause) { super(cause); }
	}
	
	
	// variables
	private HashMap<Class<? extends IState>, IState> _States;
	private ArrayDeque<Class<? extends IState>> _StateQueue;
	private Class<? extends IState> _NewPush;
	private Object[] _PassthroughObjects;
	private int _PopCount;
	private int _ID;	
	private FSMLibrary.RunningState _RunningState;

	public int getID() { return _ID; }
	public void setID(int _ID) { this._ID = _ID; }

	// Private methods
	private void ChapterCreate(int nID)
	{
		_States = new HashMap<Class<? extends IState>, IState>();
		_StateQueue = new ArrayDeque<Class<? extends IState>>();
		_NewPush = null;
		_PopCount = 0;
		_ID = nID;
		_RunningState = FSMLibrary.RunningState.Suspended;
	}

	private void PushState_Internal(Class<? extends IState> stateType)
	{
		_StateQueue.addFirst(stateType);
		if(_RunningState == FSMLibrary.RunningState.Running)
		{
			// pause current top
			Class<? extends IState > curTop = PeekNext();
			if(curTop != null)
			{
				_States.get(curTop).OnPause();
			}
			_States.get(stateType).RetrievePassthrough(_PassthroughObjects);
			_States.get(stateType).OnEnter();
			_States.get(stateType).OnResume();
			
			// clear the passthrough
			_PassthroughObjects = null;
		}
	}

	private void PopState_Internal(boolean bExit)
	{
		Class<? extends IState> top = _StateQueue.pollFirst();
		
		// only exit when running
		if(_RunningState == FSMLibrary.RunningState.Running)
		{
			_States.get(top).OnPause();
			_States.get(top).OnExit();
			
			// resume new top
			if(Peek() != null)
			{
				_States.get(_StateQueue.peek()).OnResume();
			}
		}
	}
	
	private void FlushStates()
	{
		// flush all states, exiting only on the top
		boolean bFirst = true;
		while(_StateQueue.size() > 0)
		{
			PopState_Internal(bFirst);
			bFirst = false;
		}
	}

	private IState CreateState(Class<? extends IState> stateType)
	{
		IState ret = null;
		try
		{
			Class<?> newConstructor = Class.forName(stateType.getName());
			Constructor<?> ctor = newConstructor.getConstructor();
			ret = (IState)ctor.newInstance(new Object[] { });
		}
		catch(Exception e)
		{
			e.printStackTrace();
			ret = null;
		}
		return ret;
	}
	
	// protected processing functions
	protected void StartChapter() throws StartingChapterNotSetException, ChapterAlreadyRunningException
	{
		// ensure we have a start
		if(_NewPush == null)
		{
			throw new StartingChapterNotSetException("Default state not set for chapter " + _ID);
		}
		
		if(_RunningState == RunningState.Running)
		{
			throw new ChapterAlreadyRunningException("Chapter is already running!");
		}
		
		// create all states
	    for (IState state : _States.values()) 
	    {
	    	state.OnCreate();
	    }
		
		_RunningState = FSMLibrary.RunningState.Running;
	}
		
	protected void StopChapter() throws ChapterNotRunningException
	{
		if(_RunningState != RunningState.Running)
		{
			throw new ChapterNotRunningException("Chapter isnt running!");
		}
		// exit all current states
		FlushStates();
		
		// destroy all states
	    for (IState state : _States.values()) 
	    {
	    	state.OnDestroy();
	    }
		
	    _RunningState = FSMLibrary.RunningState.Suspended;
	}

	public void PauseChapter() throws ChapterNotRunningException
    {
		if(_RunningState == RunningState.Running)
		{
			throw new ChapterNotRunningException("Chapter isnt running!");
		}
		// pause all states
	    for (IState state : _States.values()) 
	    {
	    	state.OnPause();
	    }
	    _RunningState = FSMLibrary.RunningState.Paused;
    }
	
	public void ResumeChapter() throws ChapterNotRunningException
    {
		if(_RunningState == RunningState.Running)
		{
			throw new ChapterNotRunningException("Chapter isnt running!");
		}
		// resume all states
	    for (IState state : _States.values()) 
	    {
	    	state.OnResume();
	    }
		_RunningState = FSMLibrary.RunningState.Running;
    }

	protected void PriorityUpdate()
	{
		// push the new pop
		if(_NewPush != null)
		{
			PushState_Internal(_NewPush);
			_NewPush = null;
		}
		
		Class<? extends IState> topState = Peek();
		if(topState != null)
		{
			_States.get(topState).PriorityUpdate();		
		}
	}
	
	protected void Update()
	{
		Class<? extends IState> topState = Peek();
		if(topState != null)
		{
			_States.get(topState).Update();		
		}
	}

	protected void LateUpdate()
	{		
		Class<? extends IState> topState = Peek();
		if(topState != null)
		{
			_States.get(topState).LateUpdate();		
		}
		
		// remove all pops because update is done
		boolean bFirst = true;
		while(_PopCount > 0)
		{
			PopState_Internal(bFirst);
			_PopCount--;
			bFirst = false;
		}
	}
	
	private Object Invoke(Class<?> State, Method method, Object ... parameters)
	{
		Object ret = null;
		try
        {
			ret = method.invoke(_States.get(State), parameters);
        } 
		catch (IllegalAccessException e)
        {
            e.printStackTrace();
        } 
		catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        } 
		catch (InvocationTargetException e)
        {
            e.printStackTrace();
        }	
		return ret;
	}

	protected Object Call(String szMethod, Object ... parameters)
	{		
		Class<?> topState = Peek();
		if(topState != null)
		{
			// get type array
			Class<?>[] classTypes = new Class<?>[parameters.length];
			int nCount = 0;
			for(Object obj : parameters)
			{
				classTypes[nCount++] = obj.getClass();
			}
			
			// get the method
			Method method = null;
			 try
			{
			    method = topState.getMethod(szMethod, classTypes.length > 0 ? classTypes : null);
			} 
			  catch (NoSuchMethodException e)
			{
			    e.printStackTrace();
			}
			 
			 // invoke it
			return Invoke(topState, method, parameters);
		}
		return null;
	}
	
	protected Object[] CallQueue(String szMethod, Object ... parameters)
	{		
		if(_StateQueue.size() > 0)
		{
			// get type array
			Class<?>[] classTypes = new Class<?>[parameters.length];
			int nCount = 0;
			for(Object obj : parameters)
			{
				classTypes[nCount++] = obj.getClass();
			}
			
			// get the method
			Method method = null;
			try
			{
			    method = _StateQueue.peek().getMethod(szMethod, classTypes);
			} 
			catch (NoSuchMethodException e)
			{
			    e.printStackTrace();
			}
	
			 // invoke it on all objects
			nCount = 0;
			Object[] ret = new Object[_StateQueue.size()];
			for(Class<?> topState : _StateQueue)
			{
				ret[nCount] = Invoke(topState, method, parameters);
			}
			return ret;
		}
		return null;
	}
	
	// public methods
	protected FSMChapter(String szID)
	{
		ChapterCreate(szID.hashCode());
	}
	
	protected FSMChapter(int nID)
	{
		ChapterCreate(nID);
	}
	
	public void AddState(Class<? extends IState> stateType) throws ChapterAlreadyRunningException
	{
		// ensure we don't add while running
		if(_RunningState == FSMLibrary.RunningState.Running)
		{
			throw new ChapterAlreadyRunningException("Chapter \"" + _ID + "\" is already running.  Use PushState() or ChangeState() instead.");
		}
		_States.put(stateType, CreateState(stateType));
	}
	
	public void RemoveState(Class<? extends IState> stateType) throws ChapterAlreadyRunningException
	{
		// ensure we don't remove while running
		if(_RunningState == FSMLibrary.RunningState.Running)
		{
			throw new ChapterAlreadyRunningException("Chapter \"" + _ID + "\" is already running.  Use PushState() or ChangeState() instead.");
		}
		_States.remove(stateType);
	}
	
	public void SetDefaultState(Class<? extends IState> stateType) throws ChapterAlreadyRunningException
	{
		// ensure we don't set while running
		if(_RunningState == FSMLibrary.RunningState.Running)
		{
			throw new ChapterAlreadyRunningException("Chapter \"" + _ID + "\" is already running.  Use PushState() or ChangeState() instead.");
		}
		_NewPush = stateType;
	}

	public Class<? extends IState> Peek()
	{
		Class<? extends IState> ret = _StateQueue.peekFirst();
		return ret;
	}
	
	public Class<? extends IState> PeekNext()
	{
		int nCount = 0;
		for(Class<? extends IState> ret : _StateQueue)
		{
			if(nCount == 1)
			{
				return ret;
			}
			nCount++;
		}
		return null;
	}
	
	// TP
	public void PushState(Class<? extends IState> stateType, Object... passThrough)
	{
		_NewPush = stateType;
		_PassthroughObjects = passThrough;
	}
	
	public void PopState()
	{
		_PopCount++;
	}
	
	public void ChangeState(Class<? extends IState> stateType, Object... passThrough)
	{
		_PopCount = _StateQueue.size();
		_NewPush = stateType;
		_PassthroughObjects = passThrough;
	}
	
	public void SetPassthrough(Object... passThrough)
    {
		_PassthroughObjects = passThrough;
    }
}
