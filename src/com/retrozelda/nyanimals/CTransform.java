package com.retrozelda.nyanimals;

import java.util.ArrayList;

import com.retrozelda.utilities.Utilities;

import android.opengl.Matrix;

public class CTransform
{
	// TODO: mark dirty when View or Projection changes
	private boolean _Dirty = true;
	
	private CTransform _Parent;
	private ArrayList<CTransform> _Children;
	
	protected float[] m_MMatrix = new float[16]; // model matrix
	protected float[] m_MVPMatrix = new float[16]; // model * view * proj
	protected float[] m_position = new float[2];
	protected float[] m_scale = new float[2];
	
	public boolean IsDirty() { return _Dirty;}
	public void MakeDirty()
	{
		if(_Dirty == false)
		{
			_Dirty = true;
			for(CTransform child : _Children)
			{
				child.MakeDirty();
			}
		}
	}
	
	public float[] GetModelMatrix() 
	{
		if(_Dirty)
		{
			BuildTransform();
			_Dirty = false;
		}
		return m_MMatrix;
	}
	
	public float[] GetMVPMatrix()
	{
		if(_Dirty)
		{
			BuildTransform();
			_Dirty = false;
		}
		return m_MVPMatrix;
	}
	public void SetModelMatrix(float[] model) { m_MMatrix = model;}
	public void SetMVPMatrix(float[] mvp) { m_MVPMatrix = mvp;}
	
	public float[] getLocalPosition()
	{
		return m_position.clone();
	}
	
	public float[] getPosition()
	{
		float[] pos = getLocalPosition();
		if(_Parent != null)
		{
			float[] parentPos = _Parent.getPosition();
			pos[0] += parentPos[0];
			pos[1] += parentPos[1];
		}
		return pos;
	}

	public void SetPosition(float posX, float posY)
	{
		m_position[0] = posX;
		m_position[1] = posY;
		if(_Parent != null)
		{
			float[] parentPos = _Parent.getPosition();
			m_position[0] = parentPos[0] - m_position[0];
			m_position[1] = parentPos[1] - m_position[1];
		}
		MakeDirty();
	}
	
	public void SetLocalPosition(float posX, float posY)
	{
		m_position[0] = posX;
		m_position[1] = posY;
		
		if(_Parent != null)
		{
			float[] parentSize = _Parent.GetSize();
			m_position[0] *= parentSize[0];
			m_position[1] *= parentSize[1];
		}
		
		MakeDirty();
	}
	
	public void Translate(float posX, float posY)
	{
		m_position[0] += posX;
		m_position[1] += posY;
		MakeDirty();
	}

	// scale by percent
	public void Scale(float fScaleX, float fScaleY)
	{
		SetLocalSize(m_scale[0] * fScaleX, m_scale[1] * fScaleY);
		MakeDirty();
	}

	public void SetLocalSize(float Width, float Height)
	{
		m_scale[0] = Width;
		m_scale[1] = Height;
		MakeDirty();
	}
	
	public void SetSize(float Width, float Height)
	{
		m_scale[0] = Width;
		m_scale[1] = Height;
		
		if(_Parent != null)
		{
			float[] parentSize = _Parent.GetSize();
			m_scale[0] = parentSize[0] / m_scale[0];
			m_scale[1] = parentSize[1] / m_scale[1];
		}

		MakeDirty();
	}
	

	public void SetSize(float[] size)
    {
		SetSize(size[0], size[1]);
    }
	
	public void SetLocalSize(float[] size)
    {
		SetLocalSize(size[0], size[1]);
    }	
	
	public float[] GetLocalSize()
	{
		return m_scale.clone();
	}
	
	public float[] GetSize()
	{
		float[] size = GetLocalSize();
		if(_Parent != null)
		{
			float[] parentSize = _Parent.GetSize();
			size[0] *= parentSize[0];
			size[1] *= parentSize[1];
		}
		return size;
	}
	
	public CTransform GetParent() { return _Parent;}
	public void SetParent(CTransform parent)
	{
		// unparent any existing parent stuff
		if(_Parent != null)
		{
			// TODO: undo parent transform
			_Parent.RemoveChild(this);
		}
		_Parent = parent;
		
		// set child
		if(_Parent != null)
		{
			_Parent.AddChild(this);
		}
		MakeDirty();
	}
	
	private void AddChild(CTransform child)
	{
		_Children.add(child);
	}
	
	private void RemoveChild(CTransform child)
	{
		_Children.remove(child);
	}
	
	public CTransform()
	{
		_Parent = null;
		_Children = new ArrayList<CTransform>();
		Matrix.setIdentityM(m_MVPMatrix, 0);
		SetPosition(0, 0);
		SetSize(1, 1);
	}
	
	// NOTE: parent is applied through the normal Get--() functions.  it avoids matrix multiplies
	//		 use the getLocal--() functions if you want the matrix multiply in
	protected void BuildTransform()
	{
		// freshen the model matrix
		Matrix.setIdentityM(m_MMatrix, 0);
		
		// set the position
		float[] pos = getPosition();
		Matrix.translateM(m_MMatrix, 0, pos[0], pos[1], 0.0f);
		
		// set any rotations
		
		// set any scales
		float[] scale = GetSize();
		Matrix.scaleM(m_MMatrix, 0, scale[0], scale[1], 1.0f);
		
		/*
		// apply parent
		if(_Parent != null)
		{
			float[] parentModel = _Parent.GetModelMatrix();
			Matrix.multiplyMM(m_MMatrix, 0, parentModel, 0, m_MMatrix, 0);
		}
		*/
		////////////////////////////////////////
		
		// calculate the MVP
		Matrix.multiplyMM(m_MVPMatrix, 0, Utilities.GameClass.GetViewProjection(), 0, m_MMatrix, 0);
	}
}
