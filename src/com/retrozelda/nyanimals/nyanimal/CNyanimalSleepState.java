package com.retrozelda.nyanimals.nyanimal;

import android.graphics.RectF;

import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CRectangle;
import com.retrozelda.nyanimals._AnimationTextureLink;
import com.retrozelda.nyanimals.CNyanimal.NyanimalAnimations;
import com.retrozelda.utilities.Utilities;

public class CNyanimalSleepState extends CNyanState
{
	CNyanimal _Nyanimal;
	_AnimationTextureLink _ZeeAnimation;
	CRectangle _ZeeRect;

	@Override
	public void OnCreate()
	{
	}

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
		_ZeeRect = (CRectangle)_PassthroughObjects[1];
		_ZeeAnimation = (_AnimationTextureLink)_PassthroughObjects[2];
	}
	
	@Override
	public void OnEnter()
	{
		_Nyanimal.SetAnimation(NyanimalAnimations.SLEEPING);
		_ZeeRect.SetDraw(true);
		_ZeeRect.GetTransform().SetParent(_Nyanimal.GetTransform());
	}
	
	@Override
	public void OnExit()
	{
		_ZeeRect.SetDraw(false);
		_ZeeRect.SetActive(false);
	}
	
	@Override 
	public void OnPause()
	{
		_ZeeRect.SetActive(false);
		_ZeeRect.SetDraw(false);
	}
	
	@Override
	public void OnResume()
	{
		_ZeeRect.SetActive(true);
		_ZeeRect.SetDraw(true);
	}

	@Override
	public void PriorityUpdate()
	{
	}

	@Override
	public void Update()
	{
		// no negatives
		if(_Nyanimal.GetTiredMeter() < 0)
		{
			_Nyanimal.SetTiredMeter(0.0f);
		}
		
		_Nyanimal.AppendTiredMeter(1.0f * Utilities.Time.DeltaTime_Seconds());			
		
		if(_Nyanimal.GetTiredMeter() > _Nyanimal.GetMeterMax())
		{
			// no sleep
			_ZeeRect.SetDraw(false);
			_Nyanimal.SetTiredMeter(_Nyanimal.GetMeterMax());
			_Nyanimal.DeterminNextAction();
		}

		// check if we have new UVs
		boolean newFrame = _ZeeAnimation.GetAnimation().Update(Utilities.Time.DeltaTime_Seconds());
		if(newFrame == true)
		{
			// NOTE: dont mirror Zees
			_ZeeRect.UpdateUV(_ZeeAnimation, false);
		}
	}

	@Override
	public void LateUpdate()
	{
	}

	@Override
	public void OnDestroy()
	{
	}

	@Override
	public RectF CheckCollision(CNyanObject inner)
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
	{
		return false;
	}

}
