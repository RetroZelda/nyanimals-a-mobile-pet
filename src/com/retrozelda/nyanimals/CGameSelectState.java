package com.retrozelda.nyanimals;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;
public class CGameSelectState extends CNyanState
{

	class BowlingTouch implements IButtonAction
	{
		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			Utilities.Log.Debug("CGameSelectState", "To Bowling!");
		}
	}
	class CatchTouch implements IButtonAction
	{
		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			Utilities.Log.Debug("CGameSelectState", "To Catch!");
			
			// TODO: Use change state
			// NOTE: The back button needs handled to not pop when in other single states(e.g. catch)
	        StoryBook.Chapter(NyanimalPages.Chapters.Input).Set(new NyanimalPages.BackButtonEvent(false));
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).PushState(CCatchState.class, m_Nyanimal);
		}
	}
	class HideSeekTouch implements IButtonAction
	{
		@Override
		public void HandlePress() {}

		@Override
		public void HandleRelease() 
		{
			Utilities.Log.Debug("CGameSelectState", "To Hide and Seek!");
		}
	}
	
	CNyanimal m_Nyanimal;
	CBackdrop m_Backdrop;
	CButton m_BowlingButton;
	CButton m_CatchButton;
	CButton m_HideSeekButton;
	CButton m_BackButton;

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		m_Nyanimal = (CNyanimal)_PassthroughObjects[0];
	}	

	@Override
	public RectF CheckCollision(CNyanObject inner) 
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{
		
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{
		return false;
	}

	@Override
    public void OnCreate()
    {
		m_Backdrop = CNyanimalFactoy.RequestObject(CBackdrop.class);
		m_BowlingButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_CatchButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_HideSeekButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_BackButton = CNyanimalFactoy.RequestObject(CButton.class);
		
		int nScreenMiddle = Utilities.GLSurfaceView.getWidth() / 2;
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nWidthScaled = nScreenWidth / 4;
		float fAspect = 745.0f / 425.0f; // TODO: use each button's size instead of hardcoding
		int nHeightScaled = (int)(nWidthScaled / fAspect);

		m_Backdrop.SetBackdropInformation(R.drawable.gamesbg, R.drawable.gamescornerpiece);
		m_Backdrop.SetUVScale(2.0f, 2.0f / Utilities.Screen.GetAspectRatio());
		m_Backdrop.SetScroll(-0.05f, -0.05f);
		
		m_BowlingButton.SetButtonInformation(nScreenMiddle, nHeightScaled * 3, nWidthScaled, nHeightScaled, R.drawable.bowling, R.drawable.bowlingpressed);
		m_CatchButton.SetButtonInformation(nScreenMiddle / 2, nHeightScaled * 5, nWidthScaled, nHeightScaled, R.drawable.catch_, R.drawable.catchpressed);
		m_HideSeekButton.SetButtonInformation(nScreenMiddle + (nScreenMiddle / 2), nHeightScaled * 5, nWidthScaled, nHeightScaled, R.drawable.hidenseek, R.drawable.hidenseekpressed);
		m_BackButton.SetButtonInformation(0, 0, 0, 0, R.drawable.gamesarrow, R.drawable.gamesarrow);
		
		m_BowlingButton.SetAction(new BowlingTouch());
		m_CatchButton.SetAction(new CatchTouch());
		m_HideSeekButton.SetAction(new HideSeekTouch());
		m_BackButton.SetAction(new BackTouch());
		
		m_Backdrop.SetDraw(false);
		m_BackButton.SetDraw(false);
		m_BowlingButton.SetDraw(false);
		m_CatchButton.SetDraw(false);
		m_HideSeekButton.SetDraw(false);
		

		m_BackButton.SetActive(false);
		m_BowlingButton.SetActive(false);
		m_CatchButton.SetActive(false);
		m_HideSeekButton.SetActive(false);
    }
	
	@Override
	public void OnEnter()
	{
		// screen info
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nScreenHeight = Utilities.GLSurfaceView.getHeight();
		
		// calculate size and position
		m_BackButton.GetUpRect().MakeTextureSizeLocal();
		float[] nArrowSize = m_BackButton.GetUpRect().GetTransform().GetLocalSize();
		float fArrowAspect = (float)nArrowSize[0] / (float)nArrowSize[1];
		nArrowSize[0] = (int)(nScreenWidth * (nArrowSize[0] / 1080.0f)) / 2;
		nArrowSize[1] = (int) (nArrowSize[0] / fArrowAspect);
		
		// set the rect transforms to default size
		m_BackButton.GetUpRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		m_BackButton.GetPressedRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		
		// set button size and position
		m_BackButton.GetTransform().SetSize(nArrowSize[0], nArrowSize[1]);
		m_BackButton.GetTransform().SetPosition(nScreenWidth - (nArrowSize[0]), nScreenHeight - (nArrowSize[1]));
	}

	@Override
	public void OnPause()
	{
		m_Backdrop.SetDraw(false);
		m_BowlingButton.SetDraw(false);
		m_CatchButton.SetDraw(false);
		m_HideSeekButton.SetDraw(false);
		m_BackButton.SetDraw(false);

		m_Backdrop.SetActive(false);
		m_BowlingButton.SetActive(false);
		m_CatchButton.SetActive(false);
		m_HideSeekButton.SetActive(false);
		m_BackButton.SetActive(false);
	}

	@Override
	public void OnResume()
	{
		m_Backdrop.SetDraw(true);
		m_BowlingButton.SetDraw(true);
		m_CatchButton.SetDraw(true);
		m_HideSeekButton.SetDraw(true);
		m_BackButton.SetDraw(true);

		m_Backdrop.SetActive(true);
		m_BowlingButton.SetActive(true);
		m_CatchButton.SetActive(true);
		m_HideSeekButton.SetActive(true);
		m_BackButton.SetActive(true);
	}

	@Override
    public void PriorityUpdate()
    {   
    }

	@Override
    public void Update()
    {
    }
	
	@Override
    public void LateUpdate()
    {
    }

	@Override
    public void OnDestroy()
    {
		m_BackButton.SetAction(null);
		m_CatchButton.SetAction(null);
		m_BowlingButton.SetAction(null);
		m_HideSeekButton.SetAction(null);
		CNyanimalFactoy.ReturnObject(m_HideSeekButton);
		CNyanimalFactoy.ReturnObject(m_BowlingButton);
		CNyanimalFactoy.ReturnObject(m_CatchButton);
		CNyanimalFactoy.ReturnObject(m_BackButton);
		CNyanimalFactoy.ReturnObject(m_Backdrop);
    }
}
