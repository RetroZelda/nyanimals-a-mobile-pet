package com.retrozelda.nyanimals.nyanimal;

import android.graphics.RectF;

import com.retrozelda.nyanimals.CFood;
import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanObject.Type;
import com.retrozelda.nyanimals.CNyanimal.NyanimalAnimations;
import com.retrozelda.utilities.Utilities;

public class CNyanimalEatState extends CNyanState
{
	CNyanimal _Nyanimal;
	CFood _HeldFood;
	int m_nEatLoopCounter;

	@Override
	public void OnCreate()
	{
	}

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
		_HeldFood = (CFood)_PassthroughObjects[1];

		// determine the position and stuff
		float[] nPos = _Nyanimal.GetTransform().getPosition();
		float[] nSize = _Nyanimal.GetTransform().GetSize();
		_HeldFood.SetMirrored(!_Nyanimal.IsMirrored());
		if(_Nyanimal.IsMirrored() == true) 
		{
			_HeldFood.GetTransform().SetPosition(nPos[0] + (nSize[0] / 2), nPos[1] + (nSize[1] / 2));
		}
		else
		{
			_HeldFood.GetTransform().SetPosition(nPos[0] - (nSize[0] / 2), nPos[1] + (nSize[1] / 2));
		}
		
		// hold it
		_HeldFood.PickedUp(_Nyanimal);
		_HeldFood.SetBiteAmmount(5.0f);
	}

	@Override
	public void OnEnter()
	{
		_Nyanimal.SetAnimation(NyanimalAnimations.EATING);
		m_nEatLoopCounter = 0;
	}
	
	@Override
	public void OnExit()
	{
		// drop anything the food
		_HeldFood.PickedUp(null);
		_HeldFood = null;
	}
	
	@Override
	public void PriorityUpdate()
	{
	}

	@Override
	public void Update()
	{
		int nCurLoopCount = _Nyanimal.GetCurAnimation().GetLoopCount();
		
		if(m_nEatLoopCounter != nCurLoopCount)
		{
			// take a bite
			m_nEatLoopCounter = nCurLoopCount;
			float fBiteAmount = _HeldFood.TakeBite(25.0f);
			
			if(fBiteAmount > 0)
			{
				CFood.FoodStates curFoodState = _HeldFood.GetFoodState();
				_Nyanimal.AppendHungerMeter(fBiteAmount);
				
				// handle the health
				if(curFoodState == CFood.FoodStates.SPOILED)
				{
					_Nyanimal.SetHealthMeter(0.0f);
				}
				
				// stop if full
				if(_Nyanimal.GetHungerMeter() > _Nyanimal.GetMeterMax())
				{
					Utilities.Log.Debug("CNyanimalEatState", "Full!");
					_Nyanimal.SetHungerMeter(_Nyanimal.GetMeterMax());
					_Nyanimal.GrabFood(null);
				}
			}
			else					
			{
				if(_HeldFood.GetType() == Type.MEDICINE)
				{
					_Nyanimal.SetHealthMeter(_Nyanimal.GetMeterMax());
				}
				// no more food
				Utilities.Log.Debug("CNyanimalEatState", "Food Gone!");
				_Nyanimal.GrabFood(null);
			}
		}			
	}

	@Override
	public void LateUpdate()
	{
	}

	@Override
	public void OnDestroy()
	{
	}

	@Override
	public RectF CheckCollision(CNyanObject inner)
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
	{
		return false;
	}

}
