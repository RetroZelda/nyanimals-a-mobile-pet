package com.retrozelda.nyanimals;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.Animations.CAnimation;
import com.retrozelda.nyanimals.netconnect.CSerializableObject;
import com.retrozelda.nyanimals.nyanimal.CNyanimalCatchState;
import com.retrozelda.nyanimals.nyanimal.CNyanimalEatState;
import com.retrozelda.nyanimals.nyanimal.CNyanimalIdleState;
import com.retrozelda.nyanimals.nyanimal.CNyanimalSleepState;
import com.retrozelda.nyanimals.nyanimal.CNyanimalStateConstructor;
import com.retrozelda.nyanimals.nyanimal.CNyanimalWalkingState;
import com.retrozelda.utilities.Utilities;

import android.content.Context;
import android.graphics.RectF;

public class CNyanimal extends CRectangle
{
	private static final String TAG = CNyanimal.class.getSimpleName();
	

	
	// ////////////////////////////////////////////////////////
	// ////////////////////////////////////////////////////////

	// ////////// Nyanimal Actions/Status /////////////////////
	// ////////////////////////////////////////////////////////
	public enum NyanimalStates { IDLE, EATING, SLEEPING, WALKING, CATCH };
	public enum NyanimalStatus { HAPPY, HUNGRY, SLEEPY, BORED, SICK, DEAD }; // determines different "idle" animations
	public enum NyanimalStages 
	{ 
		EGG, BABY, CHILD, TEEN, ADULT, SENIOR; 
		
		// gets the age that the particular stage starts
		public int GetStartAge()
		{
			switch(this)
			{
			case EGG:
				return -1;
			case BABY:
				return 0;
			case CHILD:
				return 5;
			case TEEN:
				return 13;
			case ADULT:
				return 25;
			case SENIOR:
				return 65;
			}		
			return -2;
		}		
	};
	
	// NOTE: if you add here, you need to add in NyanimalSerializer
	Date m_birthDay;
	NyanimalStates	m_curState;
	NyanimalStages	m_curStage;
	NyanimalStatus	m_curStatus;
	
	// NOTE: when anything is < 0 the happy meter goes down, and everything > 0 makes the meter go up
	// happy meter goes down faster with multiple problems, and goes up at a constant rate
	float			m_happyMeter;	// 100 is happy,	 0 is unhappy,		// -100 mean dead
	float			m_healthMeter;	// 100 is healthy,	 0 is unhealthy,	// medicine brings to 100
	float			m_hungerMeter;	// 100 is full,		 0 is hungry,		// goes down slower while sleeping
	float			m_tiredMeter;	// 100 is sleepy, 	 0 is wide awake,	// sleep brings to 100
	float			m_boredMeter;	// 100 is bored,	 0 is content,		

	public Date GetBirthday() { return m_birthDay;}
	public NyanimalStages GetCurStage() { return m_curStage;}
	public NyanimalStates GetCurState() { return m_curState;}
	public NyanimalStatus GetCurStatus() { return m_curStatus;}
	
	public float GetmeterMin() { return  0.0f;}
	public float GetMeterMax() { return  100.0f;}
	public float GetHappyMeter() { return m_happyMeter;}
	public float GetHealthMeter() { return m_healthMeter;}
	public float GetHungerMeter() { return m_hungerMeter;}
	public float GetTiredMeter() { return m_tiredMeter;}
	public float GetBoredMeter() { return m_boredMeter;}

	public void SetHappyMeter(float fval) {m_happyMeter = fval;}
	public void SetHealthMeter(float fval) {m_healthMeter = fval;}
	public void SetHungerMeter(float fval) {m_hungerMeter = fval;}
	public void SetTiredMeter(float fval) {m_tiredMeter = fval;}
	public void SetBoredMeter(float fval) {m_boredMeter = fval;}

	public void AppendHappyMeter(float fDelta) {SetHappyMeter(m_happyMeter + fDelta);}
	public void AppendHealthMeter(float fDelta) {SetHealthMeter(m_healthMeter + fDelta);}
	public void AppendHungerMeter(float fDelta) {SetHungerMeter(m_hungerMeter + fDelta);}
	public void AppendTiredMeter(float fDelta) {SetTiredMeter(m_tiredMeter + fDelta);}
	public void AppendBoredMeter(float fDelta) {SetBoredMeter(m_boredMeter + fDelta);}

	// ////////////////////////////////////////////////////////
	// ////////////////////////////////////////////////////////

	// ///////////////// animations/movement ///////////////////////////
	// ////////////////////////////////////////////////////////
	public enum NyanimalAnimations { IDLE, EATING, SLEEPING, 
		WALKING_UP, WALKING_DOWN, WALKING_LEFT, WALKING_RIGHT, 
		HUNGRY, SLEEPY, BORED, SICK, DEAD};
		
	public enum Direction { 
		UP, DOWN, LEFT, RIGHT;
		static Direction GetDirectionByValue(int val)
		{
			int dir = val % 4; // safety filter
			switch(dir)
			{
			case 0:
				return UP;
			case 1:
				return DOWN;
			case 2:
				return LEFT;
			case 3:
				return RIGHT;
			}
			return DOWN;
		}
	};
	
	
	
	CRectangle m_Shadow;
	Hashtable<NyanimalAnimations, _AnimationTextureLink> m_Animations;
	NyanimalAnimations m_curAnimation;
	Direction m_curDirection;
		
	// sleep Z's
	_AnimationTextureLink m_ZeeAnimation;
	CRectangle m_ZeeRect;
	
	List<CNyanimalAccessory> m_Accessories;
	
	public Direction GetCurDirection(){ return m_curDirection;}	
	public void SetDirection(Direction dir) { m_curDirection = dir;}
	
	public void AddAccessory(CNyanimalAccessory accessory)
	{
		accessory.m_Nyanimal = this;
		accessory.GetTransform().SetSize(GetTransform().GetSize());
		accessory.SetDrawLayer(Utilities.CommonLayers.ObjectAcc);
		m_Accessories.add(accessory);
	}
	
	public void RemoveAccessory(CNyanimalAccessory accessory)
	{
		accessory.m_Nyanimal = null;
		m_Accessories.remove(accessory);
	}
	
	public CAnimation GetCurAnimation()
	{
		return m_Animations.get(m_curAnimation).m_Animation;
	}
	
	public void DeterminNextAction()
	{
		// determin the new thing to do
		int decision = Utilities.Random.nextInt() & 0x1; // walk, idle
		NyanimalStates newState = (decision == 0?NyanimalStates.WALKING:NyanimalStates.IDLE);
		Direction newDirection = Direction.GetDirectionByValue(Utilities.Random.nextInt());
				
		ChangeState(newState, newDirection, this);
	}
	
	// ////////////////////////////////////////////////////////
	// ////////////////////////////////////////////////////////

	public CNyanimal()
	{
		super();
		m_Animations = new Hashtable<NyanimalAnimations, _AnimationTextureLink>();
		m_Accessories = new ArrayList<CNyanimalAccessory>();
	}
	
	@Override
	public void SetDraw(boolean bDraw) 
	{
		super.SetDraw(bDraw);
		m_Shadow.SetDraw(bDraw);
		for(CNyanimalAccessory acc : m_Accessories)
		{
			acc.SetDraw(bDraw);
		}
	}

	@Override
	public void SetActive(boolean bActive)
	{
		super.SetActive(bActive);
		m_Shadow.SetActive(bActive);
		for(CNyanimalAccessory acc : m_Accessories)
		{
			acc.SetActive(bActive);
		}
	}
	
	@Override
    public void Init()
	{
		Utilities.Log.Debug(TAG, "Nyanimal Init Start");
		super.Init();
		SetDrawLayer(Utilities.CommonLayers.Object);
		
		// Determine the screen scale
		float fScreenScale = 0.118f; // based on Nexus 5 screen width of 1080 and frame size of 128
		float fNyanimalRatio = 1.0f;
		int nNyanimalWidth = (int)(Utilities.Screen.GetWidth() * fScreenScale);
		int nNyanimalHeight = (int)(nNyanimalWidth * fNyanimalRatio);
				
		// set the Zees
		m_ZeeRect = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_ZeeRect.SetResourceID(R.drawable.zzzzz);
		m_ZeeRect.GetTransform().SetSize(1, 1);
		m_ZeeRect.SetDraw(false);
		m_ZeeRect.SetDrawLayer(Utilities.CommonLayers.ObjectMisc);
				
		// set the shadow
		m_Shadow = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_Shadow.SetResourceID(R.drawable.shadow);
		m_Shadow.SetDrawLayer(Utilities.CommonLayers.Shadow);
		
		// set the start size and position
		GetTransform().SetSize(nNyanimalWidth, nNyanimalHeight);
		GetTransform().SetPosition((int)(0.5f * Utilities.Screen.GetWidth()), (int)(0.75f * Utilities.Screen.GetHeight()));
		
		// parent
		m_Shadow.GetTransform().SetParent(GetTransform());
		m_ZeeRect.GetTransform().SetParent(GetTransform());
			
		
		// other defaults
		m_curState = NyanimalStates.IDLE;
		m_curStatus = NyanimalStatus.HAPPY;
		m_curStage = NyanimalStages.EGG;
		m_happyMeter = 100;	
		m_healthMeter = 100;	
		m_hungerMeter = 100;	
		m_tiredMeter = 100;	
		m_boredMeter = 100;	
		m_birthDay = new Date();		
		
		Utilities.Log.Debug(TAG, "Nyanimal Init End");
	}
	
	@Override
    public void OnEnable()
    {
    }

	@Override
    public void OnDisable()
    {
    }
	
	@Override
	public void Destroy()
	{
		// TODO: get rid of animations
		
		for(CNyanimalAccessory leftover : m_Accessories)
		{
			Utilities.Log.Debug(TAG, "Leftover accessory: " + leftover.getClass().getName());
			leftover.m_Nyanimal = null;
			CNyanimalFactoy.ReturnObject(leftover);
		}
		m_Accessories.clear();
		
		CNyanimalFactoy.ReturnObject(m_ZeeRect);
		CNyanimalFactoy.ReturnObject(m_Shadow);
		m_ZeeRect = null;
		m_Shadow = null;
	}

	@Override
    public void OpenGL_Create()
    {
		super.OpenGL_Create();
		
		float fRenderTime = 1.0f / 24.0f; //24 fps
		float f20FPS = 1.0f / 20.0f;
		float f18FPS = 1.0f / 18.0f;

		int nFrameWidth = 128;
		int nFrameHeight = 128;
		
		// int nFrameWidth = 128;
		// int nFrameHeight = 128;
		 
		// cattage idle
		_AnimationTextureLink idleLink = new _AnimationTextureLink();
		idleLink.Create("idle", R.drawable.cattageidle, nFrameWidth, nFrameHeight, 5, 5, fRenderTime, true);
		m_Animations.put(NyanimalAnimations.IDLE, idleLink);
		
		// cattage walk
		_AnimationTextureLink walkLink = new _AnimationTextureLink();
		walkLink.Create("idle", R.drawable.cattagesprite2, nFrameWidth, nFrameHeight, 5, 5, fRenderTime, true);
		m_Animations.put(NyanimalAnimations.WALKING_UP, walkLink);
		m_Animations.put(NyanimalAnimations.WALKING_DOWN, walkLink);
		m_Animations.put(NyanimalAnimations.WALKING_LEFT, walkLink);
		m_Animations.put(NyanimalAnimations.WALKING_RIGHT, walkLink);
		
		// cattage eat
		_AnimationTextureLink eatLink = new _AnimationTextureLink();	
		eatLink.Create("eat", R.drawable.cattagebite, nFrameWidth, nFrameHeight, 4, 4, fRenderTime, true);
		m_Animations.put(NyanimalAnimations.EATING, eatLink);
		
		// cattage sleep
		_AnimationTextureLink sleepLink = new _AnimationTextureLink();
		sleepLink.Create("sleep", R.drawable.cattagesnooze, nFrameWidth, nFrameHeight, 5, 5, f20FPS, true);
		m_Animations.put(NyanimalAnimations.SLEEPING, sleepLink);
		
		// cattage hungry
		_AnimationTextureLink hungerLink = new _AnimationTextureLink();
		hungerLink.Create("hungry", R.drawable.cattagesprite_hungry, nFrameWidth, nFrameHeight, 4, 4, fRenderTime, true);
		m_Animations.put(NyanimalAnimations.HUNGRY, hungerLink);
		
		// cattage sick
		_AnimationTextureLink sickLink = new _AnimationTextureLink();
		sickLink.Create("sick", R.drawable.cattagesprite_sick, nFrameWidth, nFrameHeight, 4, 4, fRenderTime, true);
		m_Animations.put(NyanimalAnimations.SICK, sickLink);
		
		// the missing
		m_Animations.put(NyanimalAnimations.SLEEPY, idleLink);
		m_Animations.put(NyanimalAnimations.BORED, idleLink);
		m_Animations.put(NyanimalAnimations.DEAD, idleLink);
		
		// Zee animation
		m_ZeeAnimation = new _AnimationTextureLink();
		m_ZeeAnimation.Create("zzzzz", R.drawable.zzzzz, nFrameWidth, nFrameHeight, 4, 4, f18FPS, true);
		
		// the shadow
		m_Shadow.MakeTextureSize();
		/*
		float[] nSize = m_Shadow.GetTransform().GetLocalSizeF();
		float[] mySize = GetTransform().GetSizeF();
		m_Shadow.GetTransform().SetSize(nSize[0] / mySize[0], nSize[1] / mySize[1]);
		 */
		
		DeterminNextAction();
    }

	@Override
    public void OpenGL_Destroy()
    {
    }

	public void LoadNyanimal()
	{
	}
	
	public void SaveNyanimal()
	{
	}
	
	public void LoadNyanimal(Context applicationContext)
	{
		Utilities.Log.Debug(TAG, "Nyanimal Load Start");
				
		// write the file
        BufferedInputStream in = null;
	    try
	    {
	    	FileInputStream inStream = applicationContext.openFileInput("cattage.nyan");
	    	
	    	in = new BufferedInputStream(inStream);
	    	
	    	byte[] loadBuffer = new byte[512];
	    	int nReadAmmount = in.read(loadBuffer);
	    	
	    	if(nReadAmmount > 0)
	    	{
				ByteBuffer loadedData = ByteBuffer.wrap(loadBuffer);
				
				m_curStatus = NyanimalStatus.values()[loadedData.getInt()];
				m_curStage = NyanimalStages.values()[loadedData.getInt()];
				m_happyMeter = loadedData.getFloat();	
				m_healthMeter = loadedData.getFloat();	
				m_hungerMeter = loadedData.getFloat();	
				m_tiredMeter = loadedData.getFloat();	
				m_boredMeter = loadedData.getFloat();	
				m_birthDay.setTime(loadedData.getLong());
				
				long nSavedTime = loadedData.getLong();
				long nLoadTime = new Date().getTime();
				long nTimeAway = nLoadTime - nSavedTime;
				UpdateStats((float)nTimeAway / 1000);
	    	}
	    	
	    	Utilities.Log.Debug(TAG, "File successfully loaded.");
	    }
	    catch (Exception ex)
	    {
	    	Utilities.Log.Debug(TAG, "Error loading file: " + ex.getLocalizedMessage() + ".  Using default!");
	    }
		
	    Utilities.Log.Debug(TAG, "Nyanimal Load End");
	}
	
	public void SaveNyanimal(Context applicationContext)
	{
		ByteBuffer saveData = ByteBuffer.allocate(512);
		        
        BufferedOutputStream out = null;
	    try
	    {
	    	FileOutputStream outStream = applicationContext.openFileOutput("cattage.nyan", Context.MODE_PRIVATE);
	    	out = new BufferedOutputStream(outStream);

	    	// write the states
			saveData.putInt(m_curStatus.ordinal());			
			saveData.putInt(m_curStage.ordinal());

			// write the meters
			saveData.putFloat(m_happyMeter);
			saveData.putFloat(m_healthMeter);
			saveData.putFloat(m_hungerMeter);
			saveData.putFloat(m_tiredMeter);
			saveData.putFloat(m_boredMeter);
			
			// write the birthdate
			long nBirthdayMS = m_birthDay.getTime();
			saveData.putLong(nBirthdayMS);
			
			// write the current time
			long nNowMS = new Date().getTime();
			saveData.putLong(nNowMS);
			
			// write to file
			out.write(saveData.array());
			out.close();
	        //Log.d(TAG, "File successfully saved.");
	    }
	    catch (Exception ex)
	    {
	    	Utilities.Log.Debug(TAG, "Error saving file: " + ex.getLocalizedMessage());
	    }
	}
	
	public void SetAnimation(NyanimalAnimations newAnim)
	{
		if(newAnim != m_curAnimation)
		{
			m_Animations.get(newAnim).m_Animation.Reset();
		}
		m_curAnimation = newAnim;

		// update the accessories
		for(int nAccessoryIndex = 0; nAccessoryIndex < m_Accessories.size(); ++nAccessoryIndex)
		{
			m_Accessories.get(nAccessoryIndex).m_curAnimation = m_curAnimation;
		}
		UpdateUV(m_Animations.get(m_curAnimation), m_bMirror);
	}
	
	/*
	 * 
	enum NyanimalStates { IDLE, EATING, SLEEPING, WALKING, TOTAL };
	enum NyanimalStatus { HAPPY, HUNGRY, SLEEPY, BORED, SICK, DEAD }; // determines different "idle" animations
	 * 
	 * */
	public void ChangeState(NyanimalStates state, Object... passthrough)
	{
		ChangeState(state, null, passthrough);
	}
	public void ChangeState(NyanimalStates state, Direction dir, Object... passthrough)
	{
		if(m_curState == state && m_curDirection == dir)
		{
			return;
		}
		Utilities.Log.Debug(TAG, "Changing to state: " + state + " facing: " + dir);
		m_curState = state;
		switch(m_curState)
		{
		case EATING:
			FSMLibrary.Chapter(CNyanimalStateConstructor.CHAPTERHASH).ChangeState(CNyanimalEatState.class, passthrough);
			break;
		case IDLE:
			FSMLibrary.Chapter(CNyanimalStateConstructor.CHAPTERHASH).ChangeState(CNyanimalIdleState.class, passthrough);
			break;
		case SLEEPING:
			FSMLibrary.Chapter(CNyanimalStateConstructor.CHAPTERHASH).ChangeState(CNyanimalSleepState.class, passthrough);
			break;
		case WALKING:
			FSMLibrary.Chapter(CNyanimalStateConstructor.CHAPTERHASH).ChangeState(CNyanimalWalkingState.class, passthrough);
			break;
		case CATCH:
			FSMLibrary.Chapter(CNyanimalStateConstructor.CHAPTERHASH).ChangeState(CNyanimalCatchState.class, passthrough);
			break;
		default:
			break;			
		}
		
		if(dir == null)
		{
			dir = m_curDirection;
		}
		m_curDirection = dir;
	}
	
	private void UpdateStats(float deltaTime)
	{
		// change bars where needed		
		if(m_curState != NyanimalStates.EATING)
		{
			m_hungerMeter -= 1.0f * deltaTime;

			if(m_hungerMeter < 0)
			{
				// get sicker
				m_healthMeter -= 0.01f * deltaTime;
				
				// clamp hunger
				m_hungerMeter = 0.0f;
			}
			if(m_healthMeter < 0)
			{
				// lose affection
				m_happyMeter -= 0.01f * deltaTime;
				
				// clamp sick
				m_healthMeter = 0.0f;
			}
			if(m_happyMeter < 0)
			{
				// TODO: handle happiness's min
				m_happyMeter = 0;
			}
			
			if(m_curState != NyanimalStates.SLEEPING)
			{
				m_tiredMeter -= 0.25f * deltaTime;
			}
		}
	}
	
	public void CheckForStateChanges(float deltaTime)
	{
		// change bars where needed		
		if(m_curState != NyanimalStates.EATING && m_curState != NyanimalStates.SLEEPING && m_curState != NyanimalStates.CATCH)
		{				
			// get sick
			if(m_healthMeter < 20.0f)
			{
				if(m_curState != NyanimalStates.IDLE && m_curStatus != NyanimalStatus.SICK && m_curAnimation.name() != "sick")
				{
					m_curStatus = NyanimalStatus.SICK;
					ChangeState(NyanimalStates.IDLE, this);
				}
			}		
			// get hungry
			else if(m_hungerMeter < 20.0f)
			{
				if(m_curState != NyanimalStates.IDLE && m_curStatus != NyanimalStatus.HUNGRY && m_curAnimation.name() != "hungry")
				{
					m_curStatus = NyanimalStatus.HUNGRY;
					ChangeState(NyanimalStates.IDLE, this);
				}
			}				
			// no more sick
			else
			{
				if(m_curStatus == NyanimalStatus.HUNGRY)
				{
					m_curStatus = NyanimalStatus.HAPPY;
					ChangeState(NyanimalStates.IDLE, this);
				}
				if(m_curStatus == NyanimalStatus.SICK)
				{
					m_curStatus = NyanimalStatus.HAPPY;
					ChangeState(NyanimalStates.IDLE, this);
				}
			}
			
			// go to sleep
			if(m_tiredMeter < 0)
			{
				ChangeState(NyanimalStates.SLEEPING, this, m_ZeeRect, m_ZeeAnimation);
			}
		}
	}

	@Override
	public void Update()
	{
		// update all our time-basedStats 
		UpdateStats(Utilities.Time.DeltaTime_Seconds());
		CheckForStateChanges(Utilities.Time.DeltaTime_Seconds());		
		UpdateAnimationFrame(Utilities.Time.DeltaTime_Seconds());

		// int[] frameSize = m_Animations.get(m_curAnimation).m_Animation.GetCurrentFrameSize();
		//SetSize(frameSize[0], frameSize[1]);
		
		if(m_Shadow != null)
		{
			float[] fSize = GetTransform().GetSize();
			float fShadowOffset = fSize[1] * 0.9f;
			m_Shadow.GetTransform().SetPosition(0.0f, fShadowOffset / fSize[1]);
		}
		
		// update the accessories
		for(int nAccessoryIndex = 0; nAccessoryIndex < m_Accessories.size(); ++nAccessoryIndex)
		{
			m_Accessories.get(nAccessoryIndex).Update();
		}
	}

	private void UpdateAnimationFrame(float deltaTime)
	{
		boolean newFrame = m_Animations.get(m_curAnimation).m_Animation.Update(deltaTime);
		
		// check if we have new UVs
		if(newFrame == true)
		{
			UpdateUV(m_Animations.get(m_curAnimation), m_bMirror);
		}
	}
	
	public void GrabFood(CFood food)
	{
		// pick up
		if(food != null)
		{			
			// its chow time
			ChangeState(NyanimalStates.EATING, this, food);
		}
		// put dowm
		else
		{
			DeterminNextAction();
		}
	}

	@Override
	public void HandleCollision(CNyanObject other, RectF overlap)
	{
		super.HandleCollision(other, overlap);
		
		switch(other.GetType())
		{
		case MEDICINE:
		case FOOD:
			CFood food = (CFood)other;
			if(food.StoppedDragging() == true && m_curState != NyanimalStates.EATING)
			{
				// flip if needed
				float[] nFoodPos = food.GetTransform().getPosition();
				float[] nMyPos = GetTransform().getPosition();
				if(nFoodPos[0] > nMyPos[0])
				{
					m_bMirror = true;
				}
				else
				{
					m_bMirror = false;
				}
				
				// grab the bitch
				GrabFood(food);
			}
			break;
		default:
			break;
		};
		
	}

	@Override
	public Type GetType() 
	{
		return Type.NYANIMAL;
	}
    
	
}
