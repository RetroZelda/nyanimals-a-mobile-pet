package com.retrozelda.nyanimals.nyanimal;

import android.graphics.RectF;

import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimal.NyanimalAnimations;
import com.retrozelda.nyanimals.CNyanimal.NyanimalStatus;
import com.retrozelda.utilities.Utilities;

public class CNyanimalIdleState extends CNyanState
{
	CNyanimal _Nyanimal;
	float m_timeInState;

	@Override
	public void OnCreate()
	{
	}

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
	}
	
	@Override
	public void OnEnter()
	{
		m_timeInState = Utilities.Random.nextFloat() * 5.0f + 5.0f;

		switch(_Nyanimal.GetCurStatus())
		{
			case HAPPY:				
				_Nyanimal.SetAnimation(NyanimalAnimations.IDLE);	
			break;
			case HUNGRY:
				_Nyanimal.SetAnimation(NyanimalAnimations.HUNGRY);
			break;
			case SLEEPY:
				_Nyanimal.SetAnimation(NyanimalAnimations.SLEEPY);
			break;
			case BORED:
				_Nyanimal.SetAnimation(NyanimalAnimations.BORED);
			break;
			case SICK:
				_Nyanimal.SetAnimation(NyanimalAnimations.SICK);
			break;
			case DEAD:
				_Nyanimal.SetAnimation(NyanimalAnimations.DEAD);
			break;
		}
	}

	@Override
	public void PriorityUpdate()
	{
	}

	@Override
	public void Update()
	{
		m_timeInState -= Utilities.Time.DeltaTime_Seconds();
		if(m_timeInState < 0.0f && _Nyanimal.GetCurStatus() != NyanimalStatus.HUNGRY)
		{
			_Nyanimal.DeterminNextAction();
		}
	}

	@Override
	public void LateUpdate()
	{
	}

	@Override
	public void OnDestroy()
	{
	}

	@Override
	public RectF CheckCollision(CNyanObject inner)
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
	{
		return false;
	}

}
