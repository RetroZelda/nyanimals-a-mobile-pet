package com.retrozelda.nyanimals;

public interface IRenderObject
{
	public abstract CTransform GetTransform();
	public abstract boolean IsDrawing();
	public abstract void SetDraw(boolean bDraw);
	public abstract void OpenGL_Create();
	public abstract void OpenGL_Destroy();
	public abstract void Draw();	
	public abstract int DrawPriority();
	public abstract int GetDrawLayer();
	public abstract void SetDrawLayer(int nLayer);
}
