package com.retrozelda.nyanimals;

@Deprecated
public class NyanimalUpdateThread extends Thread
{
	boolean	running	= false;	
	private CNyanimalGame m_game;
	
	void setRunning(boolean b)
	{
		running = b;
	}
	
	void setGame(CNyanimalGame game)
	{
		m_game = game;
	}
	
	CNyanimalGame GetGame()
	{
		return m_game;
	}

	@Override
	public void run()
	{
		// super.run();
		while (running == true)
		{
			m_game.Update();
		}
	}

}