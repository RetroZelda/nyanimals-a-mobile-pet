package com.retrozelda.nyanimals;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import com.retrozelda.utilities.Utilities;

import android.graphics.Color;
import android.graphics.RectF;
import android.opengl.GLES20;

public class CRectangle extends CNyanObject implements IRenderObject
{
	// ///////////////// OPENGL ///////////////////////////////
		// ////////////////////////////////////////////////////////
		// vertices
	protected static final int	FLOAT_SIZE_BYTES					= 4;
	protected static final int	TRIANGLE_VERTICES_DATA_STRIDE_BYTES	= 5 * FLOAT_SIZE_BYTES;
	protected static final int	TRIANGLE_VERTICES_DATA_POS_OFFSET	= 0;
	protected static final int	TRIANGLE_VERTICES_DATA_UV_OFFSET	= 3;
	protected final float[]		m_TriangleVerticesData				= {
				// X,	 Y, 	Z, 		U, 		V
				-1.0f,	-1.0f,	0.0f,	0.0f,	0.0f,	// 	right	top
				-1.0f, 	 1.0f,	0.0f,	0.0f,	1.0f,	// 	right	bottom
				 1.0f,	-1.0f,	0.0f, 	1.0f, 	0.0f,	// 	left	top
				 1.0f, 	 1.0f, 	0.0f, 	1.0f, 	1.0f};	//	left	bottom
																		

	protected FloatBuffer			m_TriangleVertices;

	// texture
	int _ResourceID;
	CTexture m_Texture;

	// shader
	protected static final String m_VertexShader =	 
			  "uniform mat4 uMVPMatrix;\n"
			+ "attribute vec4 aPosition;\n"
			+ "attribute vec2 aTextureCoord;\n"
			+ "varying vec2 vTextureCoord;\n"
			+ "void main() {\n"
			+ "  gl_Position = uMVPMatrix * aPosition;\n"
			+ "  vTextureCoord = aTextureCoord;\n"
			+ "}\n";

	protected static final String m_FragmentShader_Tex =					
			  "precision mediump float;\n"
			+ "varying vec2 vTextureCoord;\n"
			+ "uniform vec4 vColor;\n"
			+ "uniform sampler2D sTexture;\n"
			+ "void main() {\n"
			+ "  gl_FragColor = texture2D(sTexture, vTextureCoord) * vColor;\n"
			+ "}\n";
	protected static final String m_FragmentShader_Color =					
			  "precision mediump float;\n"
			+ "uniform vec4 vColor;\n"
			+ "void main() {\n"
			+ "  gl_FragColor = vColor;\n"
			//+ "  gl_FragColor = vec4(0.0, 0.0, 1.0, 1.0);\n"
			+ "}\n";

	// shader handles
	protected int					m_ShaderProgram;
	protected int					m_MVPMatrixHandle;
	protected int					m_PositionHandle;
	protected int					m_UVHandle;
	protected int					m_ColorHandle;
	protected int					m_TextureHandle;
	
	// tint color
	protected float					m_Red, m_Green, m_Blue, m_Alpha;

	
	// settings
	private boolean					m_bDraw = false;
	protected boolean 				m_bMirror = false;
	private RectF 					m_BoundingRect;
	
	// Utilities
	CTransform _Transform;
	
	protected RectF GetBoundingRect()
	{
		// TODO: Dont build touch box each call
		// NOTE: issue here is that the dirty flag gets reset on render, not flagging the box to rebuild
		//if(_Transform.IsDirty())
		{
			_Transform.BuildTransform();
			BuildTouchBox();
		}
		return m_BoundingRect;
	}
	
	// ////////////////////////////////////////////////////////
	// ////////////////////////////////////////////////////////

	public int GetResourceID() { return _ResourceID;}
	public void SetResourceID(int nID) { _ResourceID = nID;}
	
	public CTexture GetTexture() { return m_Texture;}
	public void SetTexture(CTexture tex) 
	{ 
		m_Texture = tex;
		//_Transform.SetSize(GetTextureSize());
	}
	
	@Override
	public boolean IsDrawing() { return m_bDraw;}
	
	public boolean IsMirrored() { return m_bMirror;}
	public void SetMirrored(boolean bMirror) { m_bMirror = bMirror;}
	
	@Override
	public void SetDraw(boolean bDraw) 
	{
		if(m_bDraw != bDraw)
		{
			CRenderList.SetToDraw(this, bDraw);
			m_bDraw = bDraw;
		}
	}

	public void ScrollUVs(float fScrollX, float fScrollY)
	{
			m_TriangleVerticesData[3] += fScrollX;
			m_TriangleVerticesData[4] += fScrollY;
			m_TriangleVerticesData[8] += fScrollX;
			m_TriangleVerticesData[9] += fScrollY;
			m_TriangleVerticesData[13] += fScrollX;
			m_TriangleVerticesData[14] += fScrollY;
			m_TriangleVerticesData[18] += fScrollX;
			m_TriangleVerticesData[19] += fScrollY;
			
			m_TriangleVertices.position(0);
			m_TriangleVertices.put(m_TriangleVerticesData).position(0);
	}

	public void ResetUVs()
	{
		UVScale(1.0f, 1.0f);
	}
	public void UVScale(float fScaleX, float fScaleY)
	{
		m_TriangleVerticesData[3] = 0.0f;
		m_TriangleVerticesData[4] = 0.0f;
		m_TriangleVerticesData[8] = 0.0f;
		m_TriangleVerticesData[9] = fScaleY;
		m_TriangleVerticesData[13] = fScaleX;
		m_TriangleVerticesData[14] = 0.0f;
		m_TriangleVerticesData[18] = fScaleX;
		m_TriangleVerticesData[19] = fScaleY;
		
		m_TriangleVertices.position(0);
		m_TriangleVertices.put(m_TriangleVerticesData).position(0);
	}
	
	public void SetUVs(float[] UVs)
	{
		m_TriangleVerticesData[3] = UVs[0];
		m_TriangleVerticesData[4] = UVs[1];
		m_TriangleVerticesData[8] = UVs[2];
		m_TriangleVerticesData[9] = UVs[3];
		m_TriangleVerticesData[13] = UVs[4];
		m_TriangleVerticesData[14] = UVs[5];
		m_TriangleVerticesData[18] = UVs[6];
		m_TriangleVerticesData[19] = UVs[7];
		
		m_TriangleVertices.position(0);
		m_TriangleVertices.put(m_TriangleVerticesData).position(0);
	}
	
	public void SetUVs(RectF Rect)
	{
		// top right
		m_TriangleVerticesData[3] = Rect.right;
		m_TriangleVerticesData[4] = Rect.top;

		// bottom right
		m_TriangleVerticesData[8] = Rect.right;
		m_TriangleVerticesData[9] = Rect.bottom;

		// top left
		m_TriangleVerticesData[13] = Rect.left;
		m_TriangleVerticesData[14] = Rect.top;

		// bottom left
		m_TriangleVerticesData[18] = Rect.left;
		m_TriangleVerticesData[19] = Rect.bottom;
		
		m_TriangleVertices.position(0);
		m_TriangleVertices.put(m_TriangleVerticesData).position(0);
	}
	
	@Override
	public CTransform GetTransform() { return _Transform;}

	public void FitScreen()
	{
		GetTransform().SetPosition((int)(0.5f * Utilities.Screen.GetWidth()), (int)(0.5f * Utilities.Screen.GetHeight()));
		GetTransform().SetSize(Utilities.Screen.GetWidth() / 2, Utilities.Screen.GetHeight() / 2); 
	}
	
	public void FitScreenWidth()
	{
		int nCenter =  Utilities.Screen.GetWidth() / 2;
		int nWidth = nCenter;	
		int nHeight = nWidth / (GetTexture().GetWidth() / GetTexture().GetHeight());
	
		GetTransform().SetSize(nWidth, nHeight);
	}
	
	public CRectangle()
	{
		m_TriangleVertices = ByteBuffer.allocateDirect(m_TriangleVerticesData.length * FLOAT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asFloatBuffer();
		m_TriangleVertices.put(m_TriangleVerticesData).position(0);

		m_Red = 1.0f;
		m_Green = 1.0f;
		m_Blue = 1.0f;
		m_Alpha = 1.0f;
		
		_ResourceID = -1;
		
		_Transform = new CTransform();		
	}
	
	public float[] GetTextureSize()
	{
		float[] size = new float[2];
		size[0] = (float)m_Texture.GetWidth();
		size[1] = (float)m_Texture.GetHeight();
		
		return size;
	}
	
	protected void LoadTexture()
	{
		if(_ResourceID > -1)
		{
			SetTexture(CTextureManager.GetInstance().LoadTexture(_ResourceID));
		}
	}

	protected void LoadTextureShader()
	{
		// create shader program
		m_ShaderProgram = CNyanimalGLRender.createProgram(m_VertexShader, m_FragmentShader_Tex);
		if (m_ShaderProgram == 0)
		{
			return;
		}
	}

	public void SetColor(float R, float G, float B, float A)
	{
		m_Red = R;
		m_Green = G;
		m_Blue = B;
		m_Alpha = A;
	}
	public void SetColor(int R, int G, int B, int A)
	{
		m_Red = (R / 255.0f);
		m_Green = (G / 255.0f);
		m_Blue = (B / 255.0f);
		m_Alpha = (A / 255.0f);
	}
	public void SetColor(Color col)
	{
		int nColorHash = col.hashCode();
		SetColor(nColorHash);
	}
	public void SetColor(int nColorHash)
	{
		m_Red = ((Color.red(nColorHash)) / 255.0f);
		m_Green = ((Color.green(nColorHash)) / 255.0f);
		m_Blue = ((Color.blue(nColorHash)) / 255.0f);
		m_Alpha = ((Color.alpha(nColorHash)) / 255.0f);
	}

	@Override
	public int GetDrawLayer() { return m_nDrawLayer;}

	@Override
	public void SetDrawLayer(int nLayer) { m_nDrawLayer = nLayer;}
	
	@Override
	public int DrawPriority() { return 0;}

	@Override
    public void OpenGL_Create()
    {
		LoadTexture();
		LoadTextureShader();
		BuildTouchBox();
		
		// get positional handle
		m_PositionHandle = GLES20.glGetAttribLocation(m_ShaderProgram, "aPosition");
		if (m_PositionHandle == -1)
		{
			throw new RuntimeException("Could not get attrib location for aPosition");
		}

		// get Color handle
		m_ColorHandle = GLES20.glGetUniformLocation(m_ShaderProgram, "vColor");
		if (m_ColorHandle == -1)
		{
			throw new RuntimeException("Could not get attrib location for vColor");
		}
	
		// get texture handle
		m_UVHandle = GLES20.glGetAttribLocation(m_ShaderProgram, "aTextureCoord");
		if (m_UVHandle == -1)
		{
			throw new RuntimeException("Could not get attrib location for aTextureCoord");
		}
		// get the mvp matrix handle
		m_MVPMatrixHandle = GLES20.glGetUniformLocation(m_ShaderProgram, "uMVPMatrix");
		if (m_MVPMatrixHandle == -1)
		{
			throw new RuntimeException("Could not get attrib location for uMVPMatrix");
		}
		
		m_TextureHandle = GLES20.glGetUniformLocation(m_ShaderProgram, "sTexture");
		if (m_TextureHandle == -1)
		{
			throw new RuntimeException("Could not get attrib location for sTexture");
		}
    }

	@Override
    public void OpenGL_Destroy()
    {
    }

	@Override
	public void Init()
	{
	}
	
	@Override
	public void Update()
	{
	}

	@Override
	public void Destroy() 
	{		
	}
	
	@Override
	public void Draw()
	{
		float[] mvp = _Transform.GetMVPMatrix();
		
		// activate the stuff
		GLES20.glUseProgram(m_ShaderProgram);
		
		GLES20.glUniform1i(m_TextureHandle, 0);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);		
		if(m_Texture != null)
		{
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, m_Texture.GetTextureID());
		}

		// set the verts
		m_TriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
		GLES20.glVertexAttribPointer(m_PositionHandle, 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, m_TriangleVertices);

		// set the UVs
		m_TriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
		GLES20.glEnableVertexAttribArray(m_PositionHandle);

		// set the texture
		GLES20.glVertexAttribPointer(m_UVHandle, 2, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, m_TriangleVertices);
		GLES20.glEnableVertexAttribArray(m_UVHandle);
		
		// set the MVP
		GLES20.glUniformMatrix4fv(m_MVPMatrixHandle, 1, false, mvp, 0);

		// set teh color
		GLES20.glUniform4f(m_ColorHandle, m_Red, m_Green, m_Blue, m_Alpha );
		
		// Draw
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

	}

	public void MakeScaledSizeLocal()
	{
		// screen info
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		
		// calculate size
		MakeTextureSizeLocal();
		float[] nArrowSize = GetTransform().GetLocalSize();
		float fArrowAspect = nArrowSize[0] / nArrowSize[1];
		nArrowSize[0] = (nScreenWidth * (nArrowSize[0] / 1080.0f)) / 2;
		nArrowSize[1] = nArrowSize[0] / fArrowAspect;
		
		GetTransform().SetLocalSize(nArrowSize[0], nArrowSize[1]);
	}
	
	public void MakeScaledSize()
	{
		// screen info
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		
		// calculate size
		MakeTextureSize();
		float[] nArrowSize = GetTransform().GetSize();
		float fArrowAspect = nArrowSize[0] / nArrowSize[1];
		nArrowSize[0] = (nScreenWidth * (nArrowSize[0] / 1080.0f)) / 2;
		nArrowSize[1] = nArrowSize[0] / fArrowAspect;
		
		GetTransform().SetSize(nArrowSize[0], nArrowSize[1]);
	}
	
	
	public enum ScreenPosition {CenterLeft, CenterMiddle, CenterRight,
								TopLeft, TopMiddle, TopRight,
								BottomLeft, BottomMiddle, BottomRight};
	
	public void SetToScreenPosition(ScreenPosition screenPosition)
	{
		float[] size = GetTransform().GetSize();
		float fWidth = Utilities.Screen.GetWidth();
		float fHeight = Utilities.Screen.GetHeight();
		float[] destinationPos = new float[2];
		float[] center = new float[2];
		center[0] = fWidth / 2.0f;
		center[1] = fHeight / 2.0f;
		
		switch(screenPosition)
		{
		case BottomLeft:
			destinationPos[0] = size[0];
			destinationPos[1] = fHeight - size[1];
			break;
		case BottomMiddle:
			destinationPos[0] = center[0];// - size[0];
			destinationPos[1] = fHeight - size[1];
			break;
		case BottomRight:
			destinationPos[0] = fWidth - size[0];
			destinationPos[1] = fHeight - size[1];
			break;
		case CenterLeft:
			destinationPos[0] = size[0];
			destinationPos[1] = center[1];// - size[1];
			break;
		case CenterMiddle:
			destinationPos[0] = center[0];// - size[0];
			destinationPos[1] = center[1];// - size[1];
			break;
		case CenterRight:
			destinationPos[0] = fWidth - size[0];
			destinationPos[1] = center[1];// - size[1];
			break;
		case TopLeft:
			destinationPos[0] = size[0];
			destinationPos[1] = size[1];
			break;
		case TopMiddle:
			destinationPos[0] = center[0];// - size[0];
			destinationPos[1] = size[1];
			break;
		case TopRight:
			destinationPos[0] = fWidth - size[0];
			destinationPos[1] = size[1];
			break;
		default:
			break;
		
		}
		
		GetTransform().SetPosition(destinationPos[0], destinationPos[1]);
	}
	

	public void MakeTextureSize()
	{
		_Transform.SetSize(GetTextureSize());
	}
	public void MakeTextureSizeLocal()
	{
		_Transform.SetLocalSize(GetTextureSize());
	}
	
	public void BuildTouchBox()
	{
		if(m_BoundingRect == null)
		{
			m_BoundingRect = new RectF();
		}
		// build the touch rect
		float[] pos = _Transform.getPosition();
		float[] size = _Transform.GetSize();
		m_BoundingRect.top = pos[1] - (size[1]);
		m_BoundingRect.bottom = m_BoundingRect.top + (size[1] * 2);
		m_BoundingRect.left = pos[0] - (size[0]);
		m_BoundingRect.right = m_BoundingRect.left + (size[0] * 2);
	}
	
	public void UpdateUV(_AnimationTextureLink animLink, boolean bMirror)
	{
		// get the UV rect
		m_Texture =  animLink.GetTexture();
		RectF frameRect = animLink.GetAnimation().GetFrameUV(m_Texture.GetWidth(), m_Texture.GetHeight(), bMirror);
		
		// put the rect where it belongs in the vertices
		// top right
		m_TriangleVerticesData[3] = frameRect.right;
		m_TriangleVerticesData[4] = frameRect.top;
 
		// bottom right
		m_TriangleVerticesData[8] = frameRect.right;
		m_TriangleVerticesData[9] = frameRect.bottom;

		// top left
		m_TriangleVerticesData[13] = frameRect.left;
		m_TriangleVerticesData[14] = frameRect.top;

		// bottom left
		m_TriangleVerticesData[18] = frameRect.left;
		m_TriangleVerticesData[19] = frameRect.bottom;

		// save
		m_TriangleVertices.position(0);
		m_TriangleVertices.put(m_TriangleVerticesData).position(0);
	}
	

	// return RectF of the collision.  else return null
	@Override
	public RectF CheckCollision(CNyanObject other)
	{
		RectF rect = new RectF();
		CRectangle nyanRect = (CRectangle) other;
		
		boolean bIntersects = rect.setIntersect(GetBoundingRect(), nyanRect.GetBoundingRect());
		if(bIntersects == false)
		{
			return null;
		}		
		
		return rect;
	}

	@Override
	public void HandleCollision(CNyanObject other, RectF overlap)
	{
		// do nothing
	}


	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) {
		
		return false;
	}


	@Override
	public Type GetType() 
	{		
		return Type.RECT;
	}

	@Override
    public void OnGraphicsInit()
    {
    }

	@Override
    public void OnEnable()
    {
    }

	@Override
    public void OnDisable()
    {
    }
}












