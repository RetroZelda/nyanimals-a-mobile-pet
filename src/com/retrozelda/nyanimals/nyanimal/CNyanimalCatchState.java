package com.retrozelda.nyanimals.nyanimal;

import android.graphics.Color;
import android.graphics.RectF;

import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.NyanimalPages;
import com.retrozelda.nyanimals.CNyanimal.NyanimalAnimations;
import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;
import com.retrozelda.utilities.Utilities.Time;

public class CNyanimalCatchState extends CNyanState
{
	private enum CatchGameAction {MoveOnScreen, MoveOffScreen, GameOn, Sit}
	
	CNyanimal _Nyanimal;
	private float _fTranslation;	
	private float _fOnScreenPos;
	private float _fOffScreenPos;
	private float _fGameMoveSpeed;
	private float _fTransitionMoveSpeed;
	private CatchGameAction _CatchGameAction;	

	private int _nStunColor;
	private float _fStunTime;
	private float _fStunColorPulse;
	private float _fStunColorPulseSpeed;

	@Override
	public void ReadPage(IPage page)
	{
		if(page instanceof NyanimalPages.SensorChangedEvent)
		{
			NyanimalPages.SensorChangedEvent e = (NyanimalPages.SensorChangedEvent)page;
	    	float fScaled = e.GetEventData().values[0];// *= Math.abs(event.values[0]);
	    	_fTranslation = _fTranslation + ((fScaled - _fTranslation) * 0.5f);  
		}
		else if(page instanceof NyanimalPages.CatchGame_Start)
		{
			_fTranslation = 0.0f;			
			_CatchGameAction = CatchGameAction.MoveOnScreen;
		}
		else if(page instanceof NyanimalPages.CatchGame_Play)
		{
			_fTranslation = 0.0f;
			_CatchGameAction = CatchGameAction.GameOn;
		}
		else if(page instanceof NyanimalPages.CatchGame_End)
		{
			_fTranslation = 0.0f;			
			_CatchGameAction = CatchGameAction.MoveOffScreen;
		}
		else if(page instanceof NyanimalPages.CatchGame_Stun)
		{
			NyanimalPages.CatchGame_Stun e = (NyanimalPages.CatchGame_Stun)page;
			_fStunTime = e.GetStunTime();
		}
		else
		{
			super.ReadPage(page);
		}
	}
	
	@Override
	public void OnCreate()
	{
		_CatchGameAction = CatchGameAction.Sit;
		_fTransitionMoveSpeed = 3.0f;
		_fGameMoveSpeed = -4.0f;
	}
	
	@Override
	public void OnEnter()
	{
		_fTranslation = 0.0f;
		_CatchGameAction = CatchGameAction.Sit;
		
		_fOnScreenPos = (Utilities.Screen.GetWidth() / 2) - (_Nyanimal.GetTransform().GetSize()[0] / 2);
		_fOffScreenPos = (_Nyanimal.GetTransform().GetSize()[0] * -2);
		

		_fStunTime = 0.0f;
		_nStunColor = (int)((float)(Color.RED) * 0.75f);
		_fStunColorPulse = 0.0f;;
		_fStunColorPulseSpeed = 5.0f;
		
		_Nyanimal.SetAnimation(NyanimalAnimations.WALKING_LEFT);
	}
	
	@Override
	public void OnExit()
	{
		_Nyanimal.SetColor(Color.WHITE);
	}

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
	}	
	
	@Override
	public void OnPause()
	{
    	StoryBook.Chapter(NyanimalPages.Chapters.Accelerometer).Unsubscribe(NyanimalPages.SensorChangedEvent.class, this);
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Unsubscribe(NyanimalPages.CatchGame_Stun.class, this);
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Unsubscribe(NyanimalPages.CatchGame_Start.class, this);
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Unsubscribe(NyanimalPages.CatchGame_Play.class, this);
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Unsubscribe(NyanimalPages.CatchGame_End.class, this);
	}
	
	@Override
	public void OnResume()
	{
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Subscribe(NyanimalPages.CatchGame_End.class, this);
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Subscribe(NyanimalPages.CatchGame_Play.class, this);
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Subscribe(NyanimalPages.CatchGame_Start.class, this);
    	StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Subscribe(NyanimalPages.CatchGame_Stun.class, this);
    	StoryBook.Chapter(NyanimalPages.Chapters.Accelerometer).Subscribe(NyanimalPages.SensorChangedEvent.class, this);
	}
	
	@Override
	public void PriorityUpdate()
	{
	}

	private void PulseColor()
	{
		_fStunTime -= Time.DeltaTime_Seconds();
		_fStunColorPulse += _fStunColorPulseSpeed * Time.DeltaTime_Seconds();
		
		if(_fStunColorPulse > 1.0f)
		{
			_fStunColorPulse = 1.0f;
			_fStunColorPulseSpeed *= -1.0f;
		}
		else if(_fStunColorPulse < 0.0f)
		{
			_fStunColorPulse = 0.0f;
			_fStunColorPulseSpeed *= -1.0f;
		}
		
		int nColorToSet = Color.WHITE;
		if(_fStunTime > 0.0f)
		{
			nColorToSet = Color.WHITE + (int)((_nStunColor - Color.WHITE) * _fStunColorPulse);
		}
		_Nyanimal.SetColor(nColorToSet);
	}
	
	@Override
	public void Update()
	{
    	float[] pos = _Nyanimal.GetTransform().getPosition();
    	float[] size = _Nyanimal.GetTransform().GetSize();
    	float startPos = pos[0];
    	
    	if(_fStunTime > 0.0f)
    	{
    		PulseColor();
    	}
    	
		switch(_CatchGameAction)
		{
		case GameOn:
			// update the catcher's position based on the accelerometer
	    	float fDelta = _fTranslation * _Nyanimal.GetTransform().GetSize()[0] * _fGameMoveSpeed * Utilities.Time.DeltaTime_Seconds();
	    	pos[0] += fDelta;
	    	
	    	// clamp the catcher to the screen
	    	if(pos[0] < size[0])
	    	{
	    		pos[0] = size[0];
	    		_fTranslation = 0.0f;
	    	}
	    	if(pos[0] + size[0] > Utilities.Screen.GetWidth())
	    	{
	    		pos[0] = Utilities.Screen.GetWidth() - size[0];
	    		_fTranslation = 0.0f;
	    	}
			break;
		case MoveOffScreen:
			pos[0] -= _fTransitionMoveSpeed * size[0] * Utilities.Time.DeltaTime_Seconds();
			if(pos[0] < _fOffScreenPos)
			{
				_CatchGameAction = CatchGameAction.Sit;
			}
			break;
		case MoveOnScreen:
			pos[0] += _fTransitionMoveSpeed * size[0] * Utilities.Time.DeltaTime_Seconds();
			if(pos[0] > _fOnScreenPos)
			{
				_CatchGameAction = CatchGameAction.Sit;
			}
			break;
		case Sit:
			// do nothing
			break;
		default:
			break;
		
		}
		
		// mirror if needed
		if(startPos > pos[0])
		{
			_Nyanimal.SetMirrored(false);
		}
		else if(startPos < pos[0])
		{
			_Nyanimal.SetMirrored(true);
		}
		
    	_Nyanimal.GetTransform().SetPosition(pos[0], pos[1]);
	}

	@Override
	public void LateUpdate()
	{
	}

	@Override
	public void OnDestroy()
	{
	}

	@Override
	public RectF CheckCollision(CNyanObject inner)
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
	{
		return false;
	}

}
