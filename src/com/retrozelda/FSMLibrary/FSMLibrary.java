package com.retrozelda.FSMLibrary;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import android.annotation.SuppressLint;

import com.retrozelda.FSMLibrary.FSMChapter.ChapterAlreadyRunningException;
import com.retrozelda.FSMLibrary.FSMChapter.ChapterNotRunningException;
import com.retrozelda.FSMLibrary.FSMChapter.StartingChapterNotSetException;

public class FSMLibrary 
{	
	// exceptions
	public class StateExistsException extends Exception
	{
		private static final long serialVersionUID = -8655779944067433495L;
		public StateExistsException() { super(); }
		public StateExistsException(String message) { super(message); }
		public StateExistsException(String message, Throwable cause) { super(message, cause); }
		public StateExistsException(Throwable cause) { super(cause); }
	}
	public class FSMLibraryAlreadyRunningException extends Exception
	{
        private static final long serialVersionUID = 4947003576142110164L;
		public FSMLibraryAlreadyRunningException() { super(); }
		public FSMLibraryAlreadyRunningException(String message) { super(message); }
		public FSMLibraryAlreadyRunningException(String message, Throwable cause) { super(message, cause); }
		public FSMLibraryAlreadyRunningException(Throwable cause) { super(cause); }
	}
	public class FSMLibraryNotRunningException extends Exception
	{
        private static final long serialVersionUID = -7213719635730224069L;
		public FSMLibraryNotRunningException() { super(); }
		public FSMLibraryNotRunningException(String message) { super(message); }
		public FSMLibraryNotRunningException(String message, Throwable cause) { super(message, cause); }
		public FSMLibraryNotRunningException(Throwable cause) { super(cause); }
	}
	
	// data types
	protected static enum RunningState {Suspended, Paused, Running}

	// data
	private ArrayList<IFSMConstructor> _Constructors;
	private HashMap<Integer, FSMChapter> _FSMChapters;	
	private RunningState _RunningState;

	// singleton
	private static FSMLibrary Instance = new FSMLibrary();	
	
	@SuppressLint("UseSparseArrays") // because I cant use SparseArrays in this instance
    private FSMLibrary()
	{
		_Constructors = new ArrayList<IFSMConstructor>();
		_FSMChapters = new HashMap<Integer, FSMChapter>();
		_RunningState = RunningState.Suspended;
	}
	/////////////////////////////////////////////////////////
	
	// private functions
	private IFSMConstructor CreateConstructor(Class<? extends IFSMConstructor> constructorType)
	{
		IFSMConstructor ret = null;
		try
		{
			Class<?> newConstructor = Class.forName(constructorType.getName());
			Constructor<?> ctor = newConstructor.getConstructor();
			ret = (IFSMConstructor)ctor.newInstance(new Object[] { });
		}
		catch(Exception e)
		{
			e.printStackTrace();
			ret = null;
		}
		return ret;
	}
	
	private void StartNewChapter_Internal(Class<? extends IFSMConstructor> constructorType, Object... passThrough)
	{
		IFSMConstructor constructor = CreateConstructor(constructorType);
		constructor.Build();
        try
        {
        	Chapter(constructor.ChapterHash()).SetPassthrough(passThrough);
	        Chapter(constructor.ChapterHash()).StartChapter();
        } catch (StartingChapterNotSetException e)
        {
	        e.printStackTrace();
        } catch (ChapterAlreadyRunningException e)
        {
	        e.printStackTrace();
        }
	}
	
	private void StopChapter_Internal(Class<? extends IFSMConstructor> constructorType)
	{
		IFSMConstructor constructor = CreateConstructor(constructorType);
		try
        {
	        Chapter(constructor.ChapterHash()).StopChapter();
        } catch (ChapterNotRunningException e)
        {
	        e.printStackTrace();
        }
		constructor.Destroy();
	}

	private void AddConstructor_Internal(Class<? extends IFSMConstructor> constructorType) throws StateExistsException, FSMLibraryAlreadyRunningException, FSMLibraryNotRunningException
	{
		if(_RunningState == RunningState.Running)
		{
			throw new FSMLibraryAlreadyRunningException("FSM Library is already running!");
		}
		if(_RunningState == RunningState.Paused)
		{
			throw new FSMLibraryNotRunningException("FSM Library is paused!");
		}
		IFSMConstructor constructor = CreateConstructor(constructorType);  
		if(_FSMChapters.containsKey(constructor.ChapterHash()))
		{
			throw new StateExistsException("Constructor \"" + constructor.Chapter() + "\" already exists!");
		}
		
		if(!_FSMChapters.containsKey(constructor.ChapterHash()))
		{
			AddChapter(constructor.ChapterHash());
		}
		_Constructors.add(constructor);
	}
	
	private void AddChapter(int nChapterHash)
	{
		_FSMChapters.put(nChapterHash, new FSMChapter(nChapterHash));
	}

	private void Build_Internal()
	{
		for(int nConstructorIndex = 0; nConstructorIndex < _Constructors.size(); ++nConstructorIndex)
		{
			_Constructors.get(nConstructorIndex).Build();
		}
		_Constructors.clear();
	}
	
	private FSMChapter GetChapter(int nID)
	{
		if(!_FSMChapters.containsKey(nID))
		{
			AddChapter(nID);
		}
		return _FSMChapters.get(nID);
	}
	
	// private processing functions
	private void StartLibrary_Internal() throws FSMLibraryAlreadyRunningException, StartingChapterNotSetException, FSMLibraryNotRunningException
	{
		if(_RunningState == RunningState.Running)
		{
			throw new FSMLibraryAlreadyRunningException("FSM Library is already running!");
		}
		if(_RunningState == RunningState.Paused)
		{ 
			throw new FSMLibraryNotRunningException("FSM Library is paused!");
		}
		
		for(FSMChapter chapter : _FSMChapters.values())
		{
			try
            {
	            chapter.StartChapter();
            } catch (ChapterAlreadyRunningException e)
            {
	            e.printStackTrace();
            }
		}

		_RunningState = RunningState.Running;
	}
	
	private void PriorityUpdate_Internal() throws FSMLibraryNotRunningException
	{
		if(_RunningState == RunningState.Suspended)
		{
			throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		}
		if(_RunningState == RunningState.Paused)
		{
			throw new FSMLibraryNotRunningException("FSM Library is paused!");
		}
		
		for(FSMChapter chapter : _FSMChapters.values())
		{
			chapter.PriorityUpdate();
		}
	}
	
	private void Update_Internal() throws FSMLibraryNotRunningException
	{
		if(_RunningState == RunningState.Suspended)
		{
			throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		}
		if(_RunningState == RunningState.Paused)
		{
			throw new FSMLibraryNotRunningException("FSM Library is paused!");
		}
		
		for(FSMChapter chapter : _FSMChapters.values())
		{
			chapter.Update();
		}
	}
	
	private void LateUpdate_Internal() throws FSMLibraryNotRunningException
	{
		if(_RunningState == RunningState.Suspended)
		{
			throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		}
		if(_RunningState == RunningState.Paused)
		{
			throw new FSMLibraryNotRunningException("FSM Library is paused!");
		}
		
		for(FSMChapter chapter : _FSMChapters.values())
		{
			chapter.LateUpdate();
		}
	}
	
	private void StopLibrary_Internal() throws FSMLibraryNotRunningException
	{
		if(_RunningState == RunningState.Suspended)
		{
			throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		}
		
		for(FSMChapter chapter : _FSMChapters.values())
		{
			try
            {
	            chapter.StopChapter();
            } catch (ChapterNotRunningException e)
            {
	            e.printStackTrace();
            }
		}
		
		_RunningState = RunningState.Suspended;
	}

	private void ResumeLibrary_Internal() throws FSMLibraryNotRunningException
    {
		if(_RunningState == RunningState.Suspended)
		{
			throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		}
		if(_RunningState != RunningState.Running)
		{
			for(FSMChapter chapter : _FSMChapters.values())
			{
				try
                {
	                chapter.ResumeChapter();
                } catch (ChapterNotRunningException e)
                {
	                e.printStackTrace();
                }
			}
			_RunningState = RunningState.Running;
		}
    }
	
	private void PauseLibrary_Internal() throws FSMLibraryNotRunningException
    {
		if(_RunningState == RunningState.Suspended)
		{
			throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		}
		if(_RunningState != RunningState.Paused)
		{
			for(FSMChapter chapter : _FSMChapters.values())
			{
				try
                {
	                chapter.PauseChapter();
                } catch (ChapterNotRunningException e)
                {
	                e.printStackTrace();
                }
			}
			_RunningState = RunningState.Paused;
		}
    }
	
	private Object Call_Internal(int nChapterHash, String szMethod, Object ... parameters) throws FSMLibraryNotRunningException
	{
		if(_RunningState == RunningState.Suspended)
		{
			throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		}
		if(_RunningState == RunningState.Paused)
		{
			throw new FSMLibraryNotRunningException("FSM Library is paused!");
		}
		return Chapter(nChapterHash).Call(szMethod, parameters);
	}
	
	private Object[] CallQueue_Internal(int nChapterHash, String szMethod, Object ... parameters) throws FSMLibraryNotRunningException
	{
		if(_RunningState == RunningState.Suspended)
		{
			throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		}
		if(_RunningState == RunningState.Paused)
		{
			throw new FSMLibraryNotRunningException("FSM Library is paused!");
		}
		return Chapter(nChapterHash).CallQueue(szMethod, parameters);
	}

	// public static bridge functions
	public static FSMChapter Chapter(int nID)
	{
		return Instance.GetChapter(nID);
	}
	
	public static FSMChapter Chapter(String szID)
	{
		return Instance.GetChapter(szID.hashCode());
	}
	
	// public static functions
	public static void StartNewChapter(Class<? extends IFSMConstructor> constructorType, Object... passThrough)
	{
		Instance.StartNewChapter_Internal(constructorType, passThrough);
	}
	
	public static void StopChapter(Class<? extends IFSMConstructor> constructorType)
	{
		Instance.StopChapter_Internal(constructorType);
	}
	
	public static void AddConstructor(Class<? extends IFSMConstructor> constructorType) throws StateExistsException, FSMLibraryAlreadyRunningException, FSMLibraryNotRunningException
	{
		Instance.AddConstructor_Internal(constructorType);
	}

	public static void Build()
	{
		Instance.Build_Internal();
	}
	
	// public static bridge processing functions
	public static void StartLibrary() throws FSMLibraryAlreadyRunningException, StartingChapterNotSetException, FSMLibraryNotRunningException
	{
		Instance.StartLibrary_Internal();
	}
	
	public static void PriorityUpdate() throws FSMLibraryNotRunningException
	{
		Instance.PriorityUpdate_Internal();
	}
	
	public static void Update() throws FSMLibraryNotRunningException
	{
		Instance.Update_Internal();
	}
	
	public static void LateUpdate() throws FSMLibraryNotRunningException
	{
		Instance.LateUpdate_Internal();
	}
	
	public static void StopLibrary() throws FSMLibraryNotRunningException
	{
		Instance.StopLibrary_Internal();
	}

	public static Object Call(int nChapterHash, String szMethod, Object ... parameters) throws FSMLibraryNotRunningException
	{
		return Instance.Call_Internal(nChapterHash, szMethod, parameters);
	}
	
	public static Object[] CallQueue(int nChapterHash, String szMethod, Object ... parameters) throws FSMLibraryNotRunningException
	{
		return Instance.CallQueue_Internal(nChapterHash, szMethod, parameters);
	}

	public static void PauseLibrary() throws FSMLibraryNotRunningException
    {
		Instance.PauseLibrary_Internal();
    }

	public static void ResumeLibrary() throws FSMLibraryNotRunningException
    {
		Instance.ResumeLibrary_Internal();
    }

}
