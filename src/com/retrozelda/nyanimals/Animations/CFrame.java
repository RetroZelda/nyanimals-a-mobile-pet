package com.retrozelda.nyanimals.Animations;

import android.graphics.Rect;
import android.graphics.RectF;

public class CFrame
{
	int		m_posX;
	int		m_posY;
	int		m_width;
	int		m_height;
	float	m_time;

	// CFrame Constructor
	// int:posX -> left coordinate of frame
	// int:posY -> bottom coordinate of frame
	// int:width -> frame width
	// int:height -> frame height
	// float:time -> time to be on frame
	public CFrame(int posX, int posY, int width, int height, float time)
	{
		m_posX = posX;
		m_posY = posY;
		m_width = width;
		m_height = height;
		m_time = time;
	}

	// return the time this frame lasts
	public float GetTime()
	{
		return m_time;
	}

	// return the rectangle of the frame as pixel coordinates
	public Rect GetFrame(boolean mirror)
	{
		Rect frameRect = new Rect();
		frameRect.top = m_posY;
		frameRect.bottom = frameRect.top + m_height;

		// mirror if requested
		if(mirror)
		{
			frameRect.left = m_posX;
			frameRect.right = frameRect.left + m_width;
		}
		else
		{
			frameRect.right = m_posX;
			frameRect.left = frameRect.right + m_width;
		}

		return frameRect;
	}

	// return the rectangle of the frame as texture coordinates
	public RectF GetFrameUV(float sheetWidth, float sheetHeight, boolean mirror)
	{
		Rect pixelRect = GetFrame(mirror);
		RectF frameRect = new RectF();
		frameRect.left = pixelRect.left / sheetWidth;
		frameRect.bottom = pixelRect.bottom / sheetHeight;
		frameRect.right = pixelRect.right / sheetWidth;
		frameRect.top = pixelRect.top / sheetHeight;

		return frameRect;
	}

	public int[] GetFrameSize()
	{
		int[] size = new int[2];
		size[0] = m_width;
		size[1] = m_height;

		return size;
	}
}
