package com.retrozelda.nyanimals.nyanimal;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.FSMLibrary.IFSMConstructor;
import com.retrozelda.FSMLibrary.FSMChapter.ChapterAlreadyRunningException;
import com.retrozelda.storybook.StoryBook;

public class CNyanimalStateConstructor implements IFSMConstructor
{
	public static String CHAPTER = "NyanimalFSM";
	public static int CHAPTERHASH = CHAPTER.hashCode();
	
	@Override
    public String Chapter()
    {
	    return CHAPTER;
    }

	@Override
    public int ChapterHash()
    {
	    return CHAPTERHASH;
    }

	@Override
    public void Build()
    {
		StoryBook.Print("Building: " + Chapter() + " start");
		try
        {
			// NOTE: Add States Here:
	        FSMLibrary.Chapter(ChapterHash()).AddState(CNyanimalEmptyState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CNyanimalIdleState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CNyanimalEatState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CNyanimalWalkingState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CNyanimalSleepState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CNyanimalCatchState.class);
	        /////////////////////////////////////////////////////////////////////////
	        
	        FSMLibrary.Chapter(ChapterHash()).SetDefaultState(CNyanimalEmptyState.class);
        } 
		catch (ChapterAlreadyRunningException e)
        {
	        e.printStackTrace();
        }
		StoryBook.Print("Building: " + Chapter() + " end");
    }

	@Override
    public void Destroy()
    {
		StoryBook.Print("Destroying: " + Chapter() + " start");
        try
        {
    		// NOTE: Remove States Here:
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CNyanimalEmptyState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CNyanimalIdleState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CNyanimalEatState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CNyanimalWalkingState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CNyanimalSleepState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CNyanimalCatchState.class);
	        /////////////////////////////////////////////////////////////////////////
        } catch (ChapterAlreadyRunningException e)
        {
	        e.printStackTrace();
        }
		StoryBook.Print("Destroying: " + Chapter() + " end");
    }

}
