package com.retrozelda.nyanimals;

import com.retrozelda.FSMLibrary.*;
import com.retrozelda.FSMLibrary.FSMChapter.ChapterAlreadyRunningException;
import com.retrozelda.storybook.StoryBook;

public class CNyanimalsGameFSMConstructor implements IFSMConstructor
{
	public static String CHAPTER = "NyanimalsGameFSM";
	public static int CHAPTERHASH = CHAPTER.hashCode();
	
	@Override
    public String Chapter() { return CHAPTER; }
	@Override
    public int ChapterHash() { return CHAPTERHASH; }

	@Override
    public void Build()
    {
		StoryBook.Print("Building: " + Chapter() + " start");
		try
        {
			// NOTE: Add States Here:
	        FSMLibrary.Chapter(ChapterHash()).AddState(CBattleState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CBrushState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CGameSelectState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CItemState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CMedicalState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CRoomState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CSleepState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CStatusScreenState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CWashState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CCatchState.class);
	        FSMLibrary.Chapter(ChapterHash()).AddState(CLoadingState.class);
	        /////////////////////////////////////////////////////////////////////////
	        
	        FSMLibrary.Chapter(ChapterHash()).SetDefaultState(CLoadingState.class);
        } 
		catch (ChapterAlreadyRunningException e)
        {
	        e.printStackTrace();
        }
		StoryBook.Print("Building: " + Chapter() + " end");
	    
    }
	
	@Override
    public void Destroy()
    {
		StoryBook.Print("Destroying: " + Chapter() + " start");
		try
        {
			// NOTE: Add States Here:
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CBattleState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CBrushState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CGameSelectState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CItemState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CMedicalState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CRoomState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CSleepState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CStatusScreenState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CWashState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CCatchState.class);
	        FSMLibrary.Chapter(ChapterHash()).RemoveState(CLoadingState.class);
	        /////////////////////////////////////////////////////////////////////////
        } 
		catch (ChapterAlreadyRunningException e)
        {
	        e.printStackTrace();
        }
		StoryBook.Print("Destroying: " + Chapter() + " end");
    }
}
