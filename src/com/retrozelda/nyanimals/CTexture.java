package com.retrozelda.nyanimals;

public class CTexture implements Cloneable 
{
	private int	m_nTextureID;
	private int	m_nWidth;
	private int	m_nHeight;
	private float m_fAspect;
	private boolean m_bCreated;
	
	public int GetTextureID() { return m_nTextureID;}
	public int GetWidth() { return m_nWidth;}
	public int GetHeight() { return m_nHeight;}
	public float GetAspect() { return m_fAspect;}
	public boolean IsCreated() { return m_bCreated;}

	@Override
    public Object clone()
	{
		CTexture ret = new CTexture();
		m_nTextureID = ret.m_nTextureID;
		m_nWidth = ret.m_nWidth;
		m_nHeight = ret.m_nHeight;
		
		return ret;
	}
	public CTexture()
	{
		m_bCreated = false;
	}
	
	public void SetData(int nTexID, int nWidth, int nHeight)
	{
		if(m_bCreated == true)
		{
			return;
		}
		 m_nTextureID = nTexID;
		 m_nWidth = nWidth;
		 m_nHeight = nHeight;
		 m_fAspect = nWidth / nHeight;
		 m_bCreated = true;
		
	}
}
