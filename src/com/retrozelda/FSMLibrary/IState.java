package com.retrozelda.FSMLibrary;

// TODO: make abstract so the functions aren't required
public interface IState 
{
	public void OnCreate();
	public void OnEnter();
	public void PriorityUpdate();
	public void Update();
	public void LateUpdate();
	public void OnExit();
	public void OnDestroy();
	public void OnPause();
	public void OnResume();
	public void RetrievePassthrough(Object[] _PassthroughObjects);
}
