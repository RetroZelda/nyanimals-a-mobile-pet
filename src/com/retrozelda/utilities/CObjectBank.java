package com.retrozelda.utilities;

import java.util.HashMap;

public class CObjectBank
{
	public class ObjectAlreadyStoredException extends Exception
	{
        private static final long serialVersionUID = 7921431007119228289L;
		public ObjectAlreadyStoredException() { super(); }
		public ObjectAlreadyStoredException(String message) { super(message); }
		public ObjectAlreadyStoredException(String message, Throwable cause) { super(message, cause); }
		public ObjectAlreadyStoredException(Throwable cause) { super(cause); }
	}
	
	private static CObjectBank _Instance = new CObjectBank();
	private HashMap<String, Object> _Bank;
	
	private CObjectBank()
	{
		_Bank = new HashMap<String, Object>();
	}

	private Object Widthdraw_Internal(String objName)
	{
		return _Bank.remove(objName);
	}

	private void Deposit_Internal(String objName, Object obj) throws ObjectAlreadyStoredException
	{
		Object curObj = _Bank.get(objName);
		if(curObj != null)
		{
			// object already exists.  can't over-right!
			throw new ObjectAlreadyStoredException("Object \"" + objName + "\" already in the Bank!  Widthdraw the object to insert a new one!");
		}
		else
		{
			_Bank.put(objName, obj);
		}
	}

	private Object Retrieve_Internal(String objName)
    {
		return _Bank.get(objName);
    }

	public static Object Widthdraw(String objName)
	{	
		return _Instance.Widthdraw_Internal(objName);		
	}

	public static void Deposit(String objName, Object obj) throws ObjectAlreadyStoredException
	{	
		_Instance.Deposit_Internal(objName, obj);		
	}
	
	public static Object Retrieve(String objName)
	{	
		return _Instance.Retrieve_Internal(objName);		
	}
}
