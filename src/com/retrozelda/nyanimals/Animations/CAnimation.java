package com.retrozelda.nyanimals.Animations;

import java.util.Vector;

import android.graphics.RectF;

public class CAnimation
{
	String			m_animationName;
	Vector<CFrame>	m_Frames;
	int				m_curFrame;
	float			m_curTime;
	boolean			m_loop;
	int				m_loopCount;
	
	public void SetCurFrame(int nFrame) { m_curFrame = nFrame;}
	public void SetLoop(boolean bLoop) { m_loop = bLoop;}

	
	
	public int GetCurFrame() { return m_curFrame;}
	public boolean IsLooping() { return m_loop;}
	public int GetLoopCount() { return m_loopCount;}

	// gets the RECT of the current frame in texture coordinates
	public RectF GetFrameUV(int sheetWidth, int sheetHeight, boolean mirror)
	{
		CFrame thisFrame = m_Frames.get(m_curFrame);
		return thisFrame.GetFrameUV(sheetWidth, sheetHeight,
				mirror);
	}

	public int[] GetCurrentFrameSize()
	{
		CFrame thisFrame = m_Frames.get(m_curFrame);
		return thisFrame.GetFrameSize();
	}

	public CAnimation(String animationName, boolean loop)
	{
		m_Frames = new Vector<CFrame>();
		m_animationName = animationName;
		m_loop = loop;
		Reset();
	}

	public void AddFrame(CFrame newFrame)
	{
		m_Frames.add(newFrame);
	}

	public void AddFrame(int posX, int posY, int width, int height, float time)
	{
		AddFrame(new CFrame(posX, posY, width, height, time));
	}

	// resets back to the first frame
	public void Reset()
	{
		m_curFrame = 0;
		m_curTime = 0.0f;
		m_loopCount = 0;
	}

	// Updates the animation
	// return:boolean -> true if on new frame, false if not
	public boolean Update(float deltaTime)
	{
		m_curTime += deltaTime;
		CFrame thisFrame = m_Frames.get(m_curFrame);

		if(m_curTime > thisFrame.GetTime())
		{
			// reset teh time
			m_curTime = 0.0f;
			m_curFrame++;

			// loop if necessary
			int frameCount = m_Frames.size();
			if(m_curFrame >= frameCount)
			{
				if(m_loop)
				{
					m_curFrame = 0;
					m_loopCount++;
				}
				else
				{
					m_curFrame = frameCount - 1;
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
