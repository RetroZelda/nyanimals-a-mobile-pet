package com.retrozelda.nyanimals;

import android.graphics.RectF;

import com.retrozelda.FSMLibrary.IState;
import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.IReader;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

public abstract class CNyanState implements IState, IReader
{
	// TODO: use storybook
	class BackTouch implements IButtonAction
	{
		@Override
		public void HandlePress() 
		{
			
		}

		@Override
		public void HandleRelease() 
		{
	        StoryBook.Chapter(NyanimalPages.Chapters.Input).Mark(new NyanimalPages.BackButtonEvent(false));
		}
	}
	
	int	m_nScreenWidth;
	int	m_nScreenHeight;
	
	public CNyanState()
	{
		m_nScreenWidth = 0;
		m_nScreenHeight = 0;
	}

	// TODO: Use Storybook
	public void UpdateScreenData(int width, int height)
	{
		Utilities.Log.Debug("CNyanState", "Updating Screen: " + width + "x" + height);
		m_nScreenWidth = width;
		m_nScreenHeight = height;
	}

	public void PageSubscribe()
	{
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Subscribe(NyanimalPages.ScreenTouchEvent.class, this);
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Subscribe(NyanimalPages.SurfaceChangeEvent.class, this);		
	}
	
	public void PageUnsubscribe()
	{
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Unsubscribe(NyanimalPages.ScreenTouchEvent.class, this);
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Unsubscribe(NyanimalPages.SurfaceChangeEvent.class, this);
	}
	
	@Override
	public void ReadPage(IPage page)
	{
		if(page instanceof NyanimalPages.SurfaceChangeEvent)
		{
			NyanimalPages.SurfaceChangeEvent e = (NyanimalPages.SurfaceChangeEvent)page;
			UpdateScreenData(e.Width(), e.Height());
		}
		else if(page instanceof NyanimalPages.ScreenTouchEvent)
		{
			NyanimalPages.ScreenTouchEvent e = (NyanimalPages.ScreenTouchEvent)page;
			HandleTouch(e.X(), e.Y(), e.Action());
		}
		else
		{
			Utilities.Log.Warning("Nyanimal State \"", this.getClass().getName() + "\" recieved an unhandled Storybook Page: " + page.getClass().getName());
		}
	}

	@Deprecated
	public void Draw()
	{
	}
	
	// TODO: Add Super calls in child
	@Override
    public void OnEnter()
    {
    }

	// TODO: Add Super calls in child
	@Override
    public void OnExit()
    {
    }
	@Override
    public void OnPause()
    {
		Utilities.Log.Warning(this.getClass().getName(), "Pausing state: " + this.getClass().getName());
		PageUnsubscribe();
    }

	// TODO: Add Super calls in child
	@Override
    public void OnResume()
    {
		Utilities.Log.Warning(this.getClass().getName(), "Resuming state: " + this.getClass().getName());
		PageSubscribe();
    }
	
	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		
	}

	public abstract RectF CheckCollision(CNyanObject inner);
	public abstract void HandleCollision(CNyanObject inner, RectF overlap);
	
	// TODO: Use Storybook
	public abstract boolean HandleTouch(int nPosX, int nPosY, int nTouchAction);
}
