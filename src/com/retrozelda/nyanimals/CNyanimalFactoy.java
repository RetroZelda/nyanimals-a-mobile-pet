package com.retrozelda.nyanimals;

import java.util.HashMap;
import java.util.LinkedList;

public class CNyanimalFactoy
{
	private HashMap<Class<? extends CNyanObject>, LinkedList<CNyanObject>> _OpenObjects;
	private HashMap<Class<? extends CNyanObject>, LinkedList<CNyanObject>> _ObjectsInUse;
	private HashMap<Class<? extends CNyanObject>, LinkedList<CNyanObject>> _DisabledObjects; // NOTE: Should i be using a hashmap?
	
	private static CNyanimalFactoy Instance = new CNyanimalFactoy();
	private CNyanimalFactoy()
	{
		_OpenObjects = new HashMap<Class<? extends CNyanObject>, LinkedList<CNyanObject>>();
		_ObjectsInUse = new HashMap<Class<? extends CNyanObject>, LinkedList<CNyanObject>>();
		_DisabledObjects = new HashMap<Class<? extends CNyanObject>, LinkedList<CNyanObject>>();
	}
	

	private void InitializationActive_Internal(CNyanObject obj)
	{
		LinkedList<CNyanObject> EnabledList = _ObjectsInUse.get(obj.getClass());
		LinkedList<CNyanObject> DisabledList = _DisabledObjects.get(obj.getClass());
		if(obj.IsActive())
		{
			// put as active object
			if(!EnabledList.contains(obj))
			{
				obj.OnEnable();
				EnabledList.add(obj);
			}
		}
		else
		{
			// we have a disabled object
			if(!DisabledList.contains(obj))
			{
				// NOTE: because this is called upon initialization, the object shouldnt be calling
				// the OnDestroy() method
				DisabledList.add(obj);
			}
		}
	}
	
	private void SetActive_Internal(CNyanObject obj, boolean bActive)
	{
		// boolean bPulled = false;
		LinkedList<CNyanObject> EnabledList = _ObjectsInUse.get(obj.getClass());
		LinkedList<CNyanObject> DisabledList = _DisabledObjects.get(obj.getClass());
		if(bActive)
		{
			// put as active object
			/*bPulled = */DisabledList.remove(obj);
			if(!EnabledList.contains(obj))
			{
				obj.OnEnable();
				EnabledList.add(obj);
			}
		}
		else
		{
			// pull from active objects
			/*bPulled = */EnabledList.remove(obj);
			if(!DisabledList.contains(obj))
			{
				obj.OnDisable();
				DisabledList.add(obj);
			}
		}
	}
	
	private CNyanObject RequestObject_Internal(Class<? extends CNyanObject> objType)
	{
		CNyanObject returnObject = null;
		if(!_OpenObjects.containsKey(objType))
		{
			_OpenObjects.put(objType, new LinkedList<CNyanObject>());
			_ObjectsInUse.put(objType, new LinkedList<CNyanObject>());
			_DisabledObjects.put(objType, new LinkedList<CNyanObject>());
		}
		else
		{
			returnObject = _OpenObjects.get(objType).poll();
		}
		if(returnObject == null)
		{
			try
            {
	            returnObject = objType.newInstance();
            } 
			catch (InstantiationException e)
            {
	            e.printStackTrace();
            } 
			catch (IllegalAccessException e)
            {
	            e.printStackTrace();
            }
		}
		returnObject.Init();
		if(returnObject instanceof IRenderObject)
		{
			returnObject.SetObjectStatus(CNyanObject.ObjStatus.PENDING_OPENGL_CREATE);
			CRenderList.AddObject((IRenderObject)returnObject);
		}
		else
		{
			returnObject.SetObjectStatus(CNyanObject.ObjStatus.ACTIVE);
		}
		//_ObjectsInUse.get(objType).add(returnObject);
		InitializationActive_Internal(returnObject);
		return returnObject;
	}
	
	private void ReturnObject_Internal(CNyanObject obj)
	{
		if(obj.IsActive())
		{
			obj.OnDisable();
		}
		obj.Destroy();
		if(obj instanceof IRenderObject)
		{
			obj.SetObjectStatus(CNyanObject.ObjStatus.PENDING_OPENGL_DESTROY);
			CRenderList.RemoveObject((IRenderObject)obj);
		}
		else
		{
			obj.SetObjectStatus(CNyanObject.ObjStatus.DESTROYED);
		}
		_ObjectsInUse.get(obj.getClass()).remove(obj);
		_OpenObjects.get(obj.getClass()).add(obj);
	}

	private void UpdateObject_Internal()
    {
		for(LinkedList<CNyanObject> objList : _ObjectsInUse.values())
		{
			for(CNyanObject obj : objList)
			{
				// TODO: Push inactive objects in a separate list to not be updated
				if(obj.GetObjectStatus() == CNyanObject.ObjStatus.ACTIVE && obj.IsActive())
				{
					obj.Update();
				}
			}
		}
    }

	private void PriorityUpdateObjects_Internal()
    {
		for(LinkedList<CNyanObject> objList : _ObjectsInUse.values())
		{
			for(CNyanObject obj : objList)
			{
				// TODO: Push inactive objects in a separate list to not be updated
				if(obj.GetObjectStatus() == CNyanObject.ObjStatus.ACTIVE && obj.IsActive())
				{
					obj.PriorityUpdate();
				}
			}
		}
    }
	
	private void LateUpdateObjects_Internal()
    {
		for(LinkedList<CNyanObject> objList : _ObjectsInUse.values())
		{
			for(CNyanObject obj : objList)
			{
				// TODO: Push inactive objects in a separate list to not be updated
				if(obj.GetObjectStatus() == CNyanObject.ObjStatus.ACTIVE && obj.IsActive())
				{
					obj.LateUpdate();
				}
			}
		}
    }
	
	public static void SetActive(CNyanObject obj, boolean bActive)
	{
		Instance.SetActive_Internal(obj, bActive);
	}
	
	@SuppressWarnings("unchecked")
    public static <T extends CNyanObject> T RequestObject(Class<? extends CNyanObject> objType)
	{
		return (T)Instance.RequestObject_Internal(objType);
	}
	
	public static void ReturnObject(CNyanObject obj)
	{
		Instance.ReturnObject_Internal(obj);
	}
	
	public static void UpdateObjects()
	{
		Instance.UpdateObject_Internal();
	}

	public static void PriorityUpdateObjects()
    {
		Instance.PriorityUpdateObjects_Internal();
    }

	public static void LateUpdateObjects()
    {
		Instance.LateUpdateObjects_Internal();
    }


}
