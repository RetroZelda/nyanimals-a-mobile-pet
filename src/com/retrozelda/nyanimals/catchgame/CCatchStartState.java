package com.retrozelda.nyanimals.catchgame;

import android.graphics.RectF;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.CButton;
import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimalFactoy;
import com.retrozelda.nyanimals.CRectangle;
import com.retrozelda.nyanimals.IButtonAction;
import com.retrozelda.nyanimals.R;
import com.retrozelda.utilities.*;

public class CCatchStartState extends CNyanState
{
	class PlayTouch implements IButtonAction
	{
		@Override
		public void HandlePress() 
		{
			
		}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CCatchFSMConstructor.CHAPTERHASH).ChangeState(CCatchGameIntroState.class, _Nyanimal, _Catcher);
		}
	}
	
	class HowToTouch implements IButtonAction
	{
		@Override
		public void HandlePress() 
		{			
		}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CCatchFSMConstructor.CHAPTERHASH).ChangeState(CCatchHowToState.class, _Nyanimal, _Catcher);
		}
	}	
	
	private CNyanimal _Nyanimal;
	private CCatcher _Catcher;
	private CButton _PlayButton;
	private CRectangle _Instructions;
	// private CButton _HowToButton;
	
	@Override
    public void OnCreate()
    {
		_PlayButton = CNyanimalFactoy.RequestObject(CButton.class);
		_Instructions = CNyanimalFactoy.RequestObject(CRectangle.class);
		// _HowToButton = CNyanimalFactoy.RequestObject(CButton.class);

		_PlayButton.SetButtonInformation(0, 0, 0, 0, R.drawable.catch_play, R.drawable.catch_play);
		_PlayButton.SetAction(new PlayTouch());
		
		_Instructions.SetResourceID(R.drawable.catch_instr_frame);
		_Instructions.SetDrawLayer(Utilities.CommonLayers.Object);
		
		
		// _HowToButton.SetButtonInformation(0, 0, 0, 0, R.drawable.brusharrow, R.drawable.brusharrow);
		// _HowToButton.SetAction(new HowToTouch());		
    }

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
		_Catcher = (CCatcher)_PassthroughObjects[1];
		
		// hide him off screen
		_Nyanimal.GetTransform().SetPosition(-_Nyanimal.GetTransform().GetSize()[0], Utilities.Screen.GetHeight() - (Utilities.Screen.GetHeight() / 8));
	}	
	

	@Override
    public void OnDestroy()
    {
		_PlayButton.SetAction(null);
		CNyanimalFactoy.ReturnObject(_PlayButton);
		CNyanimalFactoy.ReturnObject(_Instructions);
		// CNyanimalFactoy.ReturnObject(_HowToButton);
    }
	
	@Override
	public void OnEnter()
	{
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nScreenHeight = Utilities.GLSurfaceView.getHeight();

		_PlayButton.MakeScaledSize();
		_PlayButton.GetTransform().Scale(0.35f, 0.35f);
		float[] nArrowSize = _PlayButton.GetTransform().GetSize();
		_PlayButton.GetTransform().SetPosition((nScreenWidth / 2.0f), nScreenHeight - (nArrowSize[1]));
		
		_Instructions.MakeScaledSizeLocal();
		_Instructions.GetTransform().SetPosition(nScreenWidth / 2.0f, nScreenHeight / 2.5f);

		// _HowToButton.MakeScaledSize();
		// nArrowSize = _HowToButton.GetTransform().GetSize();
		// _HowToButton.GetTransform().SetPosition(nScreenWidth - (nArrowSize[0] * 2.0f), nScreenHeight - (nArrowSize[1]));
		

	}

	@Override
    public void OnPause()
    {
		_PlayButton.SetDraw(false);
		_PlayButton.SetActive(false);
		
		_Instructions.SetDraw(false);
		_Instructions.SetActive(false);
		
		// _HowToButton.SetDraw(false);
		// _HowToButton.SetActive(false);
    }

	@Override
    public void OnResume()
    {			
		_PlayButton.SetDraw(true);
		_PlayButton.SetActive(true);
		
		_Instructions.SetDraw(true);
		_Instructions.SetActive(true);

		// _HowToButton.SetDraw(true);
		// _HowToButton.SetActive(true);
    }


	@Override
    public void PriorityUpdate()
    {
    }

	@Override
    public void Update()
	{
		// float[] pos = _PlayButton.GetTransform().getPosition();
		// Utilities.GLRenderer.m_Text.DrawText("Play", pos[0], pos[1]);

		// pos = _HowToButton.GetTransform().getPosition();
		// Utilities.GLRenderer.m_Text.DrawText("How To", pos[0], pos[1]);
    }

	@Override
    public void LateUpdate()
    {
    }

	@Override
    public RectF CheckCollision(CNyanObject inner)
    {
	    return null;
    }

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

}