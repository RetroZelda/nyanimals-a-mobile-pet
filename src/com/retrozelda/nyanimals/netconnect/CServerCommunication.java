package com.retrozelda.nyanimals.netconnect;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.commons.*;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.NyanimalPages;
import com.retrozelda.storybook.IReader;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

// TODO: push this in its own thread
public class CServerCommunication
{
	private class PostRunnable implements Runnable
	{
		CSerializableObject[] _postBatch;
		IReader _RequestListener;
		
		
		public PostRunnable(CSerializableObject[] objectBatch, IReader requestListener)
		{
			_postBatch = objectBatch;
			_RequestListener = requestListener;
		}
		
		
		@Override
        public void run()
        {
			// batch the JSON
			JSONObject batched = CSerializableObject.Combine(_postBatch);

	        // set timeouts
	        HttpParams httpParams = new BasicHttpParams();
	        HttpConnectionParams.setSoTimeout(httpParams, 5000); // Socket(data receive) - 5 seconds
	        HttpConnectionParams.setConnectionTimeout(httpParams, 5000); // Connection(data send) - 5 seconds
	        
			// post
			HttpClient httpClient = new DefaultHttpClient(httpParams);
		    try {	    	

		        HttpPost request = new HttpPost(_szAddress);
		        request.addHeader(_szHeaderName, _szHeaderValue );
		        
		        // build the name value pair to post the JSON to "NyanimalPost"
		        // TODO: make the key a function argument
		        List<NameValuePair> hack = new ArrayList<NameValuePair>();
		        NameValuePair pair = new BasicNameValuePair("NyanimalPost", batched.toString());
		        hack.add(pair);
		        
		        // set the entity to be posted
		        UrlEncodedFormEntity form = new UrlEncodedFormEntity(hack, "UTF-8");
		        request.setEntity(form);
		        Utilities.Log.Verbose("CServerCommunication_Post", "Posting data: " + pair.getName() + "-" + pair.getValue());
		        		        
		        // execute and pull a response
		        HttpResponse response = httpClient.execute(request);	        
		        StatusLine sl = response.getStatusLine();
		        
		        // handle response here...
		        Utilities.Log.Verbose("CServerCommunication_Responce", "Post Responce: " + sl.getStatusCode() + " : " + sl.getReasonPhrase());
		        StoryBook.Chapter(_RequestListener.hashCode()).Mark(new NyanimalPages.WebThread_Response(0, response));
		        
		    }
		    catch (SocketTimeoutException e)
		    {
		    	
		    	// TODO: Wrap the response in my own class
		        Utilities.Log.Verbose("Exception", e.getClass().toString());
		        Utilities.Log.Verbose("CServerCommunication_Responce", "Post Responce: 000 : " + e.getMessage());
		        StoryBook.Chapter(_RequestListener.hashCode()).Mark(new NyanimalPages.WebThread_Response(0, null));
		    }
		    catch(UnknownHostException e)
		    {
		    	// TODO: Wrap the response in my own class
		        Utilities.Log.Verbose("Exception", e.getClass().toString());
		        Utilities.Log.Verbose("CServerCommunication_Responce", "Post Responce: 000 : " + e.getMessage());
		        StoryBook.Chapter(_RequestListener.hashCode()).Mark(new NyanimalPages.WebThread_Response(0, null));
		    }
		    catch (Exception e) 
		    {
		    	// TODO: Wrap the response in my own class
		        Utilities.Log.Verbose("CServerCommunication_Responce", "Post Responce: 000 : Unhandled Exception!");
		        Utilities.Log.Verbose("Exception", e.getClass().toString());
		        Utilities.Log.Verbose("Exception", e.getMessage());
		        StoryBook.Chapter(_RequestListener.hashCode()).Mark(new NyanimalPages.WebThread_Response(0, null));
		        
		    	e.printStackTrace();
		    } 
		    finally 
		    {
		        httpClient.getConnectionManager().shutdown();
		    }
        }
		
	}
	
	private String _szAddress;
	private String _szHeaderName;
	private String _szHeaderValue;

	public CServerCommunication(String szAddress)
	{
		_szAddress = szAddress;
		_szHeaderName = "content-type";
		_szHeaderValue = "application/x-www-form-urlencoded";
		//_szHeaderValue = "multipart/form-data";
		//_szHeaderValue = "application/json";
		

        Utilities.Log.Verbose("CServerCommunication", "Connected to: " + _szAddress);
	}
	
	public CServerCommunication(String szAddress, String szHeaderName, String szHeaderValue)
	{
		_szAddress = szAddress;
		_szHeaderName = szHeaderName;
		_szHeaderValue = szHeaderValue;

        Utilities.Log.Verbose("CServerCommunication", "Connected to: " + _szAddress);
	}
	
	public void Post(IReader requestListener, CSerializableObject... ObjectBatch)
	{
		Utilities.Log.Verbose("CServerCommunication", "Starting WebThread");
		PostRunnable postRunner = new PostRunnable(ObjectBatch, requestListener);		
		CWebThread thread = new CWebThread(postRunner, "WebThread");
		thread.start();		
	}

}
