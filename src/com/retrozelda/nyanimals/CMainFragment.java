package com.retrozelda.nyanimals;

import java.util.Arrays;
import java.util.Map;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.retrozelda.nyanimals.netconnect.CSerializableObject;
import com.retrozelda.utilities.Utilities;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class CMainFragment extends Fragment
{
	private static final String TAG = "CMainFragment";
	
	
	
	
	// Note: You can add the UiLifecycleHelper and set up a corresponding Session.StatusCallback listener in any activity 
	//       or fragment where you wish to track and respond to session state changes.
	// TODO: Have session statuses be broadcast through Storybooks
	private UiLifecycleHelper _uiHelper;
	
	// ui stuff
	private LoginButton _LoginButton;
	private TextView	_WelcomeText;
	private Button 		_PlayButton;
	private CheckBox 	_AutoPlayCheckBox;

	private Session.StatusCallback _Callback = new Session.StatusCallback() 
	{
	    @Override
	    public void call(Session session, SessionState state, Exception exception) 
	    {
	        onSessionStateChange(session, state, exception);
	    }
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    _uiHelper = new UiLifecycleHelper(getActivity(), _Callback);
	    _uiHelper.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
	    View view = inflater.inflate(R.layout.mainmenu, container, false);
	    
	    // grab ui components
	    _WelcomeText = (TextView)view.findViewById(R.id.WelcomText);
	    _PlayButton = (Button)view.findViewById(R.id.PlayNowButton);
	    _AutoPlayCheckBox = (CheckBox)view.findViewById(R.id.AutoPlayCheckbox);
	    
	    // hide what needs to be hidden
	    _WelcomeText.setVisibility(View.INVISIBLE);
	    _PlayButton.setVisibility(View.INVISIBLE);
	    
	    // grab the facebook button and set permissions
	    _LoginButton = (LoginButton)view.findViewById(R.id.authButton);
	    _LoginButton.setFragment(this);
	    _LoginButton.setReadPermissions(Arrays.asList("email", "user_friends", "user_birthday"));
	    
	    // read the user data
	    _LoginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() 
	    {
            @Override
            public void onUserInfoFetched(GraphUser user) 
            {
            	Utilities.FacebookUserInfo = user;
            	
            	if(Utilities.FacebookUserInfo != null)
            	{
            		Utilities.Log.Info(TAG, "Retrived user information for " + Utilities.FacebookUserInfo.getFirstName() + " " + Utilities.FacebookUserInfo.getLastName());
            		
            		// set facebook infor in JSON object
            		Utilities.FacebookData.SetFromGraphUser(Utilities.FacebookUserInfo);
            		
            		_WelcomeText.setVisibility(View.VISIBLE);
            	    _PlayButton.setVisibility(View.VISIBLE);
            	    
            	    _WelcomeText.setText("Welcome " + Utilities.FacebookUserInfo.getFirstName() + " " + Utilities.FacebookUserInfo.getLastName());
            	    
            	    if(_AutoPlayCheckBox.isChecked())
            	    {
            	    	Toast.makeText(getActivity(), "Would be going directly to game.  Removed for DEV purposes", Toast.LENGTH_SHORT).show();
            	    }
            	}
            	else
            	{
            	    _WelcomeText.setVisibility(View.INVISIBLE);
            	    
            	    // TODO: Make invisible when not logged in
            	    // NOTE: Left visible for testing and compatibility
            	    _PlayButton.setVisibility(View.VISIBLE);
            	}
            }
	    });

	    return view;
	}
	@Override
	public void onResume() 
	{
	    super.onResume();    
	    
	    // handle a session already existing
	    Session session = Session.getActiveSession();
	    if (session != null && (session.isOpened() || session.isClosed()) ) 
	    {
	        onSessionStateChange(session, session.getState(), null);
	    }

	    _uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
	    super.onActivityResult(requestCode, resultCode, data);
	    _uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() 
	{
	    super.onPause();
	    _uiHelper.onPause();
	}

	@Override
	public void onDestroy() 
	{
	    super.onDestroy();
	    _uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) 
	{
	    super.onSaveInstanceState(outState);
	    _uiHelper.onSaveInstanceState(outState);
	}
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) 
	{
	    if (state.isOpened()) 
	    {
	        Utilities.FacebookToken.AddString("token", session.getAccessToken());
	    } 
	    else if (state.isClosed()) 
	    {
	    	Utilities.Log.Info(TAG, "Logged out...");
	        Utilities.FacebookToken.AddString("token", "");
	    }
	}
}
