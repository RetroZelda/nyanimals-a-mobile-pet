package com.retrozelda.nyanimals.catchgame;

import android.graphics.RectF;

import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CRectangle;
import com.retrozelda.nyanimals.R;
import com.retrozelda.utilities.Utilities;

public class CCatchObject extends CRectangle
{
	public static class FallObjectInfo
	{
		public int ResourceID;
		public int PointValue;
		public float StunTime;
		public float FallSpeed;
		
		public FallObjectInfo(int nResourceID, int nPointValue, float fFallSpeed, float fStunTime)
		{
			ResourceID = nResourceID;
			PointValue = nPointValue;
			FallSpeed = fFallSpeed;
			StunTime = fStunTime;
		}
		
		public FallObjectInfo(FallObjectInfo other) 
		{
		    this.ResourceID = other.ResourceID;
		    this.PointValue = other.PointValue;
		    this.StunTime = other.StunTime;
		    this.FallSpeed = other.FallSpeed;
		}
	};
	
	private FallObjectInfo _FallInfo;
	
	private float _FallSpeedEqualizer;
	
	public int GetPointValue() { return _FallInfo.PointValue;}
	public float GetStunTime() { return _FallInfo.StunTime;}
	public boolean ShouldStun() { return _FallInfo.StunTime > 0.0f;}

	public void SetCatchInfo(FallObjectInfo info)
	{
		_FallInfo = info;
		SetResourceID(_FallInfo.ResourceID);
	}
	
	@Override
    public void Init()
    {
		_FallInfo = null;
    }

	@Override
    public void OnGraphicsInit()
    {
		MakeTextureSize();

		float[] nSize = GetTransform().GetSize();
		float fCanAspect = (float)nSize[0] / (float)nSize[1];
		
		int nDivisor = 12;
		int nNewWidth = Utilities.Screen.GetWidth() / nDivisor;
		GetTransform().SetSize(nNewWidth, (int) (nNewWidth * fCanAspect));
		
		// if the object is an apple or cupcake, shrink it
		if(_FallInfo.ResourceID == R.drawable.cupcake ||
				_FallInfo.ResourceID == R.drawable.candy||
				_FallInfo.ResourceID == R.drawable.apple)
		{
			GetTransform().Scale(0.7f, 0.7f);
		}
		
		_FallSpeedEqualizer = _FallInfo.FallSpeed * Utilities.Screen.GetHeight() / 1.5f ;
    }
	
	@Override
    public void Destroy()
    {
    }

	@Override
    public void OnEnable()
    {
    }

	@Override
    public void OnDisable()
    {
    }
	
	@Override
	public void Update()
    {
		GetTransform().Translate(0.0f, _FallSpeedEqualizer * Utilities.Time.DeltaTime_Seconds());
    }
	
	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

	@Override
    public Type GetType()
    {
	    return Type.CATCHOBJECT;
    }
	
	public boolean IsOffscreen()
	{
		return GetTransform().getPosition()[1] > Utilities.Screen.GetHeight();
	}

}
