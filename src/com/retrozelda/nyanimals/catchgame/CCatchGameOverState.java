package com.retrozelda.nyanimals.catchgame;

import android.graphics.RectF;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.CBitmapFont;
import com.retrozelda.nyanimals.CButton;
import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimalFactoy;
import com.retrozelda.nyanimals.CRectangle;
import com.retrozelda.nyanimals.IButtonAction;
import com.retrozelda.nyanimals.R;
import com.retrozelda.nyanimals.catchgame.CCatchStartState.PlayTouch;
import com.retrozelda.utilities.CObjectBank;
import com.retrozelda.utilities.Utilities;

public class CCatchGameOverState extends CNyanState
{

	class ContinueTouch implements IButtonAction
	{
		@Override
		public void HandlePress() 
		{			
		}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CCatchFSMConstructor.CHAPTERHASH).ChangeState(CCatchEndState.class, _Nyanimal, _Catcher);
		}
	}	

	private CNyanimal _Nyanimal;
	private CCatcher _Catcher;
	private CButton _ContinueButton;
	private int _nScore;
	private int _nCurScore;

	private CRectangle _Apple;
	private CRectangle _Grape;
	private CRectangle _Cherry;
	private CRectangle _Candy;
	private CRectangle _Cake;
	private CRectangle _Stun;
	private CRectangle _Core;
	
	private CBitmapFont _NumberDisplay;
	private float _fFontSize = 16.0f;

	private int _AppleCountTotal;
	private int _GrapeCountTotal;
	private int _CherryCountTotal;
	private int _CandyCountTotal;
	private int _CakeCountTotal;
	private int _StunCountTotal;
	private int _CoreCountTotal;

	private int _CurAppleCount;
	private int _CurGrapeCount;
	private int _CurCherryCount;
	private int _CurCandyCount;
	private int _CurCakeCount;
	private int _CurStunCount;
	private int _CurCoreCount;
	
	private int _CountCounter;
	private boolean _bWait;
	private float _fWaitTime;
	private float _fCurWaitTime;

	@Override
    public void OnCreate()
    {
		_ContinueButton = CNyanimalFactoy.RequestObject(CButton.class);
		
		_ContinueButton.SetButtonInformation(0, 0, 0, 0, R.drawable.brusharrow, R.drawable.brusharrow);
		_ContinueButton.SetAction(new ContinueTouch());

		_Apple = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Grape = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Cherry = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Candy = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Cake = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Stun = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Core = CNyanimalFactoy.RequestObject(CRectangle.class);
				
		_Apple.SetResourceID(R.drawable.apple);
		_Grape.SetResourceID(R.drawable.grapes);
		_Cherry.SetResourceID(R.drawable.cherry);
		_Candy.SetResourceID(R.drawable.candy);
		_Cake.SetResourceID(R.drawable.cake);
		_Stun.SetResourceID(R.drawable.meatrotten);
		_Core.SetResourceID(R.drawable.applecore);
				
		_nScore = 0;
		_AppleCountTotal = 0;
		_GrapeCountTotal = 0;
		_CherryCountTotal = 0;
		_CandyCountTotal = 0;
		_CakeCountTotal = 0;
		_StunCountTotal = 0;
		_CoreCountTotal = 0;
		
    }

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
		_Catcher = (CCatcher)_PassthroughObjects[1];
		_nScore = (Integer)_PassthroughObjects[2];
		_AppleCountTotal = (Integer)_PassthroughObjects[3];
		_GrapeCountTotal = (Integer)_PassthroughObjects[4];
		_CherryCountTotal = (Integer)_PassthroughObjects[5];
		_CandyCountTotal = (Integer)_PassthroughObjects[6];
		_CakeCountTotal = (Integer)_PassthroughObjects[7];
		_StunCountTotal = (Integer)_PassthroughObjects[8];
		_CoreCountTotal = (Integer)_PassthroughObjects[9];
	}
	
	@Override
	public void OnEnter()
	{
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nScreenHeight = Utilities.GLSurfaceView.getHeight();
		_ContinueButton.MakeScaledSize();
		//_ContinueButton.GetTransform().Scale(0.25f, 0.25f);
		float[] nArrowSize = _ContinueButton.GetTransform().GetSize();
		_ContinueButton.GetTransform().SetPosition((nScreenWidth / 2) - (nArrowSize[0] / 2), nScreenHeight - (nArrowSize[1]));
	
		float fOffsetX = nScreenWidth / 4.0f;
		float fOffsetY = nScreenHeight / 9.0f;
		float fScale0 = 0.20f;
		float[] pos = new float[2];
		pos[0] = fOffsetX;
		pos[1] = nScreenHeight / 6.0f;
		
		_NumberDisplay = (CBitmapFont)CObjectBank.Retrieve("catch_numberfont");
		_NumberDisplay.SetFontSize(_fFontSize, _fFontSize);
				
		_Apple.MakeScaledSize();
		_Apple.GetTransform().Scale(fScale0 - 0.03f, fScale0 - 0.03f);
		_Apple.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += fOffsetY;
		
		_Grape.MakeScaledSize();
		_Grape.GetTransform().Scale(fScale0, fScale0);
		_Grape.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += fOffsetY;
		
		_Cherry.MakeScaledSize();
		_Cherry.GetTransform().Scale(fScale0, fScale0);
		_Cherry.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += fOffsetY;
		
		_Candy.MakeScaledSize();
		_Candy.GetTransform().Scale(fScale0, fScale0);
		_Candy.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += fOffsetY;

		_Cake.MakeScaledSize();
		_Cake.GetTransform().Scale(fScale0, fScale0);
		_Cake.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += fOffsetY;

		_Stun.MakeScaledSize();
		_Stun.GetTransform().Scale(1.25f, 1.25f);
		_Stun.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += fOffsetY;

		_Core.MakeScaledSize();
		_Core.GetTransform().Scale(fScale0, fScale0);
		_Core.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += fOffsetY;

		_CountCounter = 0;
		_bWait = true;
		_fWaitTime = 0.5f;
		_fCurWaitTime = _fWaitTime;
		
		_CurAppleCount = 0;
		_CurGrapeCount = 0;
		_CurCherryCount = 0;
		_CurCandyCount = 0;
		_CurCakeCount = 0;
		_CurStunCount = 0;
		_CurCoreCount = 0;
		_nCurScore = 0;
	}

	@Override
    public void OnPause()
    {
		_ContinueButton.SetDraw(false);
		_ContinueButton.SetActive(false);
		
		_Apple.SetActive(false);
		_Apple.SetDraw(false);
		
		_Grape.SetActive(false);
		_Grape.SetDraw(false);
		
		_Cherry.SetActive(false);
		_Cherry.SetDraw(false);
		
		_Candy.SetActive(false);
		_Candy.SetDraw(false);
		
		_Cake.SetActive(false);
		_Cake.SetDraw(false);
		
		_Stun.SetActive(false);
		_Stun.SetDraw(false);
		
		_Core.SetActive(false);
		_Core.SetDraw(false);
    }

	@Override
    public void OnResume()
    {			
		_ContinueButton.SetDraw(_CountCounter >= 8);
		_ContinueButton.SetActive(_CountCounter >= 8);
		
		_Apple.SetActive(true);
		_Apple.SetDraw(true);
		
		_Grape.SetActive(true);
		_Grape.SetDraw(true);
		
		_Cherry.SetActive(true);
		_Cherry.SetDraw(true);
		
		_Candy.SetActive(true);
		_Candy.SetDraw(true);
		
		_Cake.SetActive(true);
		_Cake.SetDraw(true);
		
		_Stun.SetActive(true);
		_Stun.SetDraw(true);
		
		_Core.SetActive(true);
		_Core.SetDraw(true);
    }

	@Override
    public void PriorityUpdate()
    {
    }

	@Override
    public void Update()
    {
		if(_bWait)
		{
			_fCurWaitTime -= Utilities.Time.DeltaTime_Seconds();
			if(_fCurWaitTime < 0.0f)
			{
				_bWait = false;
			}
		}
		else if(_CountCounter < 8)
		{
			switch(_CountCounter)
			{
			case 0: // apple
				_CurAppleCount++;
				if(_CurAppleCount >= _AppleCountTotal)
				{
					_bWait = true;
					_CountCounter++;
					_fCurWaitTime = _fWaitTime;
					_CurAppleCount = _AppleCountTotal;
				}
				break;
			case 1: // grape
				_CurGrapeCount++;
				if(_CurGrapeCount >= _GrapeCountTotal)
				{
					_bWait = true;
					_CountCounter++;
					_fCurWaitTime = _fWaitTime;
					_CurGrapeCount = _GrapeCountTotal;
				}
				break;
			case 2: // cherry
				_CurCherryCount++;
				if(_CurCherryCount >= _CherryCountTotal)
				{
					_bWait = true;
					_CountCounter++;
					_fCurWaitTime = _fWaitTime;
					_CurCherryCount = _CherryCountTotal;
				}
				break;
			case 3: // cupcake
				_CurCandyCount++;
				if(_CurCandyCount >= _CandyCountTotal)
				{
					_bWait = true;
					_CountCounter++;
					_fCurWaitTime = _fWaitTime;
					_CurCandyCount = _CandyCountTotal;
				}
				break;
			case 4: // cake
				_CurCakeCount++;
				if(_CurCakeCount >= _CakeCountTotal)
				{
					_bWait = true;
					_CountCounter++;
					_fCurWaitTime = _fWaitTime;
					_CurCakeCount = _CakeCountTotal;
				}
				break;
			case 5: // stun
				_CurStunCount++;
				if(_CurStunCount >= _StunCountTotal)
				{
					_bWait = true;
					_CountCounter++;
					_fCurWaitTime = _fWaitTime;
					_CurStunCount = _StunCountTotal;
				}
			break;
			case 6: // core
				_CurCoreCount++;
				if(_CurCoreCount >= _CoreCountTotal)
				{
					_bWait = true;
					_CountCounter++;
					_fCurWaitTime = _fWaitTime;
					_CurCoreCount = _CoreCountTotal;
				}
			break;
			case 7: // total
				_nCurScore++;
				if(_nCurScore >= _nScore)
				{
					_bWait = true;
					_CountCounter++;
					_fCurWaitTime = _fWaitTime;
					_nCurScore = _nScore;
				}
			break;
			}
		}
		else if(_CountCounter == 8)
		{
			_CountCounter++;
			_bWait = true;
			_ContinueButton.SetDraw(true);
			_ContinueButton.SetActive(true);
		}

		float[] pos = _ContinueButton.GetTransform().getPosition();
		if(_CountCounter >= 8)
		{
			Utilities.GLRenderer.m_Text.DrawText("Continue", pos[0], pos[1]);
		}
		_NumberDisplay.DrawText(": " + _nCurScore, Utilities.Screen.GetWidth() / 4, Utilities.Screen.GetHeight() - (Utilities.Screen.GetHeight() / 10));

		pos = _Apple.GetTransform().getPosition();
		pos[0] += _Apple.GetTransform().GetSize()[0];
		_NumberDisplay.DrawText(": " + _CurAppleCount, pos[0], pos[1]);

		pos = _Grape.GetTransform().getPosition();
		pos[0] += _Grape.GetTransform().GetSize()[0];
		_NumberDisplay.DrawText(": " + _CurGrapeCount, pos[0], pos[1]);

		pos = _Cherry.GetTransform().getPosition();
		pos[0] += _Cherry.GetTransform().GetSize()[0];
		_NumberDisplay.DrawText(": " + _CurCherryCount, pos[0], pos[1]);

		pos = _Candy.GetTransform().getPosition();
		pos[0] += _Candy.GetTransform().GetSize()[0];
		_NumberDisplay.DrawText(": " + _CurCandyCount, pos[0], pos[1]);

		pos = _Cake.GetTransform().getPosition();
		pos[0] += _Cake.GetTransform().GetSize()[0];
		_NumberDisplay.DrawText(": " + _CurCakeCount, pos[0], pos[1]);

		pos = _Stun.GetTransform().getPosition();
		pos[0] += _Stun.GetTransform().GetSize()[0];
		_NumberDisplay.DrawText(": " + _CurStunCount, pos[0], pos[1]);

		pos = _Core.GetTransform().getPosition();
		pos[0] += _Core.GetTransform().GetSize()[0];
		_NumberDisplay.DrawText(": " + _CurCoreCount, pos[0], pos[1]);
    }

	@Override
    public void LateUpdate()
    {
    }

	@Override
    public void OnDestroy()
    {
		_ContinueButton.SetAction(null);
		CNyanimalFactoy.ReturnObject(_ContinueButton);
		CNyanimalFactoy.ReturnObject(_Apple);
		CNyanimalFactoy.ReturnObject(_Grape);
		CNyanimalFactoy.ReturnObject(_Cherry);
		CNyanimalFactoy.ReturnObject(_Candy);
		CNyanimalFactoy.ReturnObject(_Cake);
		CNyanimalFactoy.ReturnObject(_Stun);
		CNyanimalFactoy.ReturnObject(_Core);
    }

	@Override
    public RectF CheckCollision(CNyanObject inner)
    {
	    return null;
    }

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

}