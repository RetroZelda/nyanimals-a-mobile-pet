package com.retrozelda.nyanimals;

import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class NyanimalGLSurfaceView extends GLSurfaceView
{
	public NyanimalGLSurfaceView(Context context)
	{
		super(context);
		setEGLContextClientVersion(2); // enable OpenGL ES 2.0
		setRenderer(Utilities.GLRenderer);
	}
	
	@Override
	@SuppressLint("NewApi")
	public boolean onTouchEvent(MotionEvent event)
	{
		// Utilities.Log.Verbose("TouchEvent", "onTouchEvent: " + MotionEvent.actionToString(event.getAction()));
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Mark(new NyanimalPages.ScreenTouchEvent((int)event.getX(), (int)event.getY(), event.getAction()));
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Mark(new NyanimalPages.AndroidMotionEvent(event));
		
		return true;
	}
	
	@Override
    public boolean performClick()
	{
		return super.performClick();
	}

}
