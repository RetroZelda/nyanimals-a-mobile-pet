package com.retrozelda.utilities;

import java.util.ArrayDeque;
import java.util.Date;
import java.util.Deque;
import java.util.Map;
import java.util.Random;

import org.json.JSONObject;

import android.content.res.Resources;

import com.facebook.model.GraphUser;
import com.retrozelda.nyanimals.CBitmapFont;
import com.retrozelda.nyanimals.CGameActivity;
import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimalFactoy;
import com.retrozelda.nyanimals.CNyanimalGLRender;
import com.retrozelda.nyanimals.CNyanimalGame;
import com.retrozelda.nyanimals.NyanimalPages;
import com.retrozelda.nyanimals.R;
import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.IReader;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.nyanimals.NyanimalGLSurfaceView;
import com.retrozelda.nyanimals.netconnect.CSerializableObject;
import com.retrozelda.nyanimals.netconnect.CServerCommunication;

public class Utilities
{
	public static CServerCommunication ServerLink = new CServerCommunication(Settings.ServerURL);
	public static Random Random = new Random(System.currentTimeMillis());
	
	public static NyanimalGLSurfaceView GLSurfaceView;
	public static CNyanimalGLRender GLRenderer;
	public static CGameActivity GameActivity;
	public static GraphUser FacebookUserInfo;
	public static CNyanimalGame GameClass;
	public static FaceBookDataSerializer FacebookData = new FaceBookDataSerializer();	
	public static FaceBookTokenSerializer FacebookToken = new FaceBookTokenSerializer();	

	public static class Settings
	{
		public static boolean DrawDebug = false;
		public static boolean ShowConsole = false;
		public static boolean LogToScreen = false;
		public static int PreloadTexturesPerFrame = 5;
		public static String ServerURL = "http://zooez.com/nyanimals/data.php";
	}	

	public static class FaceBookDataSerializer extends CSerializableObject
	{
		public FaceBookDataSerializer()
        {
	        super("FacebookData");
        }		
		
		public void SetFromGraphUser(GraphUser graph)
		{
			Map<String, Object> userMap = graph.asMap();
			for(String hashKey : userMap.keySet())
			{
				Object hashObject = userMap.get(hashKey);
				AddObject(hashKey, hashObject);
			}
		}
	} 	

	public static class FaceBookTokenSerializer extends CSerializableObject
	{
		public FaceBookTokenSerializer()
        {
	        super("FacebookToken");
        }		
	}

	public static class HighScoreSerializer extends CSerializableObject
	{
		public HighScoreSerializer()
        {
	        super("HighScore");
        }		
	}

	public static class NyanimalSerializer extends CSerializableObject
	{
		private CNyanimal _Nyanimal;
		public CNyanimal GetNyanimal() { return _Nyanimal;}
		
		public NyanimalSerializer(CNyanimal nyanimal)
        {
	        super("Nyanimal");
	        _Nyanimal = nyanimal;
        }		

		@Override
		public void ReadJSON(JSONObject JSON)
		{
			// fill the JSON
			super.ReadJSON(JSON);
			
			// set the nyanimal's data
		}

		@Override
		public JSONObject BuildJSON()
		{
			// TODO: write the nyanimal type(cattage, etc) 
			// NOTE: may need to write the different parts(arms, ears, etc)
			// TODO: write all the accessories(for each part?)
			
	    	// write the states
			AddInt("status", _Nyanimal.GetCurStatus().ordinal());			
			AddInt("stage", _Nyanimal.GetCurStage().ordinal());

			// write the meters
			AddFloat("happy", _Nyanimal.GetHappyMeter());
			AddFloat("health", _Nyanimal.GetHealthMeter());
			AddFloat("hunger", _Nyanimal.GetHungerMeter());
			AddFloat("tired", _Nyanimal.GetTiredMeter());
			AddFloat("bored", _Nyanimal.GetBoredMeter());
			
			// write the birthdate
			long nBirthdayMS = _Nyanimal.GetBirthday().getTime();
			AddLong("birthday", nBirthdayMS);
						
			// write the current time
			long nNowMS = new Date().getTime();
			AddLong("savedtime", nNowMS);
			
			// return a built JSON
			return super.BuildJSON();
		}
	}
	
	public static class CommonLayers
	{
		public static final int Background 	= -1;
		public static final int Base	 	= 00;
		public static final int Shadow 		= 10;
		public static final int Object 		= 15;
		public static final int ObjectAcc	= 16;
		public static final int ObjectMisc	= 17;
		public static final int ObjectHold 	= 20;
		public static final int HudBase 	= 80;
		public static final int HudItem 	= 81;
		public static final int HudBarBack 	= 82;
		public static final int HudBarFront	= 83;
		public static final int HudMisc		= 84;
		public static final int TouchHold 	= 99;
		public static final int Text	 	= 100;
	}
	
	public static class Time
	{
		private static long _StartTime;
		private static long _CurTime;
		private static long _PrevTime;
		private static float _ElapsedTime;
		private static float _DeltaTime;
		private static boolean _bRunning;
		
		private static int _nFPS = 0;
		private static int _nCounter = 0;
		private static float _fTime = 0.0f;
		
		private static Deque<Long> _StopWatchStartTimes = new ArrayDeque<Long>();
		public static float UpdateTime = 0.0f;
		public static float RenderTime = 0.0f;
		public static float FrameTime = 0.0f;
		static boolean _bSecTick = false;

		public static void StopWatch_Start()
		{
			_StopWatchStartTimes.push(System.nanoTime());
		}
		
		public static float StopWatch_Stop()
		{
			long startTime = _StopWatchStartTimes.pop().longValue();
			long nowTime = 0;
			do
			{
				nowTime = System.nanoTime();
			}
			while(nowTime == startTime);
			long result = nowTime - startTime;
			return result / 1000000.0f;
		}
		
		public static void Start()
		{
			_StartTime = System.nanoTime();
			_ElapsedTime = 0;
			_CurTime = _StartTime;
			_PrevTime = _StartTime;
			_DeltaTime = 0;
			Resume();
		}
		public static void Stop()
		{
			Pause();
		}
		public static void Update()
		{
			// update delta time
			_CurTime = System.nanoTime();
			_DeltaTime = (_CurTime - _PrevTime) / 1000000000.0f;
			_PrevTime = _CurTime;
			_ElapsedTime += _DeltaTime;
			
			// unmark the tick
			if(_bSecTick)
			{
				_bSecTick = false;
			}
			
			// update FPS counting
			_fTime += DeltaTime_Seconds();
			_nCounter++;
			if(_fTime > 1.0f)
			{
				_fTime = 0.0f;
				_nFPS = _nCounter;
				_nCounter = 0;
				
				if(!_bSecTick)
				{
					_bSecTick = true;
				}
			}
			
		}
		public static void Pause()
		{
			_DeltaTime = 0;
			_bRunning = false;
		}
		public static void Resume()
		{
			_fTime = 0.0f;
			_nCounter = 0;
			_nFPS = 0;
			_bRunning = true;
		}

		public static int FPS()
		{
			return _nFPS;
		}

		public static float DeltaTime_Millis()
		{
			return _DeltaTime * 1000.0f;
		}
		public static float DeltaTime_Seconds()
		{
			return _DeltaTime;
		}
		public static float ElapsedTime()
		{
			return _ElapsedTime;
		}
		public static boolean Running()
		{
			return _bRunning;
		}
	}

	public static class RenderConsole
	{
		private ArrayDeque<String> _Messages;
		private CBitmapFont	m_Text;

		public static int MaxLines = 45;
		public static int FontSize = 8;
		
		private static RenderConsole Instance = new RenderConsole();
		private RenderConsole()
		{
			_Messages = new ArrayDeque<String>();
			m_Text = CNyanimalFactoy.RequestObject(CBitmapFont.class);
			m_Text.SetFontInfo('!', 32, 32, R.drawable.times_news_roman);
			m_Text.SetFontSize(FontSize, FontSize);
			m_Text.SetDraw(Settings.ShowConsole);
			m_Text.SetDrawLayer(CommonLayers.Text);
		}
		
		public static void WriteLine(String szText)
		{
			if(Instance.m_Text.GetObjectStatus() != CNyanObject.ObjStatus.ACTIVE)
			{
				return;
			}
			Instance._Messages.addFirst(szText);
			while(Instance._Messages.size() > MaxLines)
			{
				Instance._Messages.pollLast();
			}
		}
		
		public static void Process()
		{
			if(Instance.m_Text.GetObjectStatus() != CNyanObject.ObjStatus.ACTIVE)
			{
				return;
			}
			float[] fSize = Instance.m_Text.GetTransform().GetSize();
			if(fSize[0] == 0.0f && fSize[1] == 0.0f)
			{
				Instance.m_Text.SetFontSize(FontSize, FontSize);
			}
			else
			{
				float fSpace = fSize[1] + (fSize[1] / 2);
				int nCount = 1;
				for(String message : Instance._Messages)
				{
					Instance.m_Text.DrawText(message, fSize[0], fSpace * (float)(nCount++));
				}
			}
		}
	}
	
	public static class Log
	{
		public static boolean PrintToScreen = true;
		public static void Screen(String szTag, String szMessage)
		{
			android.util.Log.v("ScreenPrint", szMessage);
			
			if(PrintToScreen == true)
			{
				RenderConsole.WriteLine(szMessage);
			}
		}
		public static void Debug(String szTag, String szMessage)
		{
			android.util.Log.d(szTag, szMessage);
			
			if(Utilities.Settings.LogToScreen == true)
			{
				RenderConsole.WriteLine(szMessage);
			}
		}
		public static void Error(String szTag, String szMessage)
		{
			android.util.Log.e(szTag, szMessage);
			
			if(Utilities.Settings.LogToScreen == true)
			{
				RenderConsole.WriteLine(szMessage);
			}
		}
		public static void Warning(String szTag, String szMessage)
		{
			android.util.Log.w(szTag, szMessage);
			
			if(Utilities.Settings.LogToScreen == true)
			{
				RenderConsole.WriteLine(szMessage);
			}
		}
		public static void Info(String szTag, String szMessage)
		{
			android.util.Log.i(szTag, szMessage);
			
			if(Utilities.Settings.LogToScreen == true)
			{
				RenderConsole.WriteLine(szMessage);
			}
		}
		public static void Verbose(String szTag, String szMessage)
		{
			android.util.Log.v(szTag, szMessage);
			
			if(Utilities.Settings.LogToScreen == true)
			{
				RenderConsole.WriteLine(szMessage);
			}
		}
		public static void Failure(String szTag, String szMessage)
		{
			android.util.Log.wtf(szTag, szMessage);
			
			if(Utilities.Settings.LogToScreen == true)
			{
				RenderConsole.WriteLine(szMessage);
			}
		}
	}
	
	public static class Screen implements IReader
	{
		private int _Width;
		private int _Height;
		private float _Aspect;

		@Override
        public void ReadPage(IPage page)
        {
			if(page instanceof NyanimalPages.SurfaceChangeEvent)
			{
				NyanimalPages.SurfaceChangeEvent e = (NyanimalPages.SurfaceChangeEvent)page;
				_Width = e.Width();
				_Height = e.Height();
				_Aspect = (float)_Width / (float)_Height;
			}
        }
		
		private static Screen Instance = new Screen();
		private Screen()
		{
			_Width = 0;
			_Height = 0;

			StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Subscribe(NyanimalPages.SurfaceChangeEvent.class, this);
		}
		
		public static int GetWidth() { return Instance._Width;}
		public static int GetHeight() { return Instance._Height;}
		public static float GetAspectRatio() { return Instance._Aspect;}
		public static void SetInitialResolution(int nWidth, int nHeight)
		{
			Instance._Width = nWidth;
			Instance._Height = nHeight;
			Instance._Aspect = (float)nWidth / (float)nHeight;
		}

	}
}
