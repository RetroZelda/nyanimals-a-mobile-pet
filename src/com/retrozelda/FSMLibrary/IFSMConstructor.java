package com.retrozelda.FSMLibrary;

public interface IFSMConstructor 
{
	public String Chapter();
	public int ChapterHash();
	
	public void Build();	
	public void Destroy();
}
