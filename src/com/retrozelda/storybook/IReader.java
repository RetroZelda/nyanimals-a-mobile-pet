package com.retrozelda.storybook;

// Interface class for an object to read a page
public interface IReader 
{
	public void ReadPage(IPage page);
}
