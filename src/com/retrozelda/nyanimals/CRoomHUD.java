package com.retrozelda.nyanimals;

import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;

public class CRoomHUD extends CNyanObject
{
	CNyanimal					m_NyanimalToWatch;
	boolean						m_bVisible;
	
	CRectangle m_TopBar;
	CRectangle m_BottomBar;
	CRectangle m_HeartShine;

	CButton	m_BrushButton;
	CButton	m_FeedButton;
	CButton	m_GamesButton;
	CButton	m_StatusButton;
	CButton m_WashButton;
	CButton m_SleepButton;
	CButton	m_ExitButton;
	CButton	m_MedicineButton;
	CButton	m_BattleButton;
	
	CUIBar m_HungerMeter;
	CUIBar m_EnergyMeter;
	
	public CRoomHUD()
	{		
		m_NyanimalToWatch = null;
		m_bVisible = false;
	}
	
	@Override
	public void SetActive(boolean bActive)
	{
		super.SetActive(bActive);
		
		// NOTE: Dont think these need to be handled in active.  
		m_TopBar.SetActive(bActive);
		m_BottomBar.SetActive(bActive);
		m_HeartShine.SetActive(bActive);

		m_BrushButton.SetActive(bActive);
		m_FeedButton.SetActive(bActive);
		m_GamesButton.SetActive(bActive);
		m_StatusButton.SetActive(bActive);
		m_WashButton.SetActive(bActive);
		m_SleepButton.SetActive(bActive);
		m_ExitButton.SetActive(bActive);
		m_MedicineButton.SetActive(bActive);
		m_BattleButton.SetActive(bActive);
		
		m_HungerMeter.SetActive(bActive);
		m_EnergyMeter.SetActive(bActive);
	}
	
	public void SetDraw(boolean b)
    {
		m_TopBar.SetDraw(b);
		m_BottomBar.SetDraw(b);
		m_HeartShine.SetDraw(b);

		m_BrushButton.SetDraw(b);
		m_FeedButton.SetDraw(b);
		m_GamesButton.SetDraw(b);
		m_StatusButton.SetDraw(b);
		m_WashButton.SetDraw(b);
		m_SleepButton.SetDraw(b);
		m_ExitButton.SetDraw(b);
		m_MedicineButton.SetDraw(b);
		m_BattleButton.SetDraw(b);
		
		m_HungerMeter.SetDraw(b);
		m_EnergyMeter.SetDraw(b);
    }

	@Override
	public void Init()
	{
		m_TopBar = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_BottomBar = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_HeartShine = CNyanimalFactoy.RequestObject(CRectangle.class);

		m_BrushButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_FeedButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_GamesButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_StatusButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_WashButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_SleepButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_ExitButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_MedicineButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_BattleButton = CNyanimalFactoy.RequestObject(CButton.class);
		
		m_HungerMeter = CNyanimalFactoy.RequestObject(CUIBar.class);
		m_EnergyMeter = CNyanimalFactoy.RequestObject(CUIBar.class);
		m_HungerMeter.SetDrawLayer(Utilities.CommonLayers.HudBarFront);
		m_EnergyMeter.SetDrawLayer(Utilities.CommonLayers.HudBarFront);
		
		m_TopBar.SetResourceID(R.drawable.uibar_upper);
		m_BottomBar.SetResourceID(R.drawable.uibar_lower);
		m_HeartShine.SetResourceID(R.drawable.uibar_upper_highlight);
		m_HungerMeter.SetResourceID(R.drawable.hunger_meter);
		m_EnergyMeter.SetResourceID(R.drawable.energy_meter);
		m_HungerMeter.SetGradientID(R.drawable.hunger_meter_gradient);		
		m_EnergyMeter.SetGradientID(R.drawable.energy_meter_gradient);

		int nButtonScaledSize = Utilities.Screen.GetWidth() / 15;	
		m_FeedButton.SetButtonInformation(0, 0, nButtonScaledSize, nButtonScaledSize, R.drawable.item_button, R.drawable.item_button_pressed );			
		m_StatusButton.SetButtonInformation(0,0, nButtonScaledSize, nButtonScaledSize, R.drawable.info_button, R.drawable.info_button_pressed);		
		m_WashButton.SetButtonInformation(0, 0, nButtonScaledSize, nButtonScaledSize, R.drawable.wash_button, R.drawable.wash_button_pressed );			
		m_ExitButton.SetButtonInformation(0, 0, nButtonScaledSize, nButtonScaledSize, R.drawable.exit_button, R.drawable.exit_button_pressed );		
		m_GamesButton.SetButtonInformation(0, 0, nButtonScaledSize, nButtonScaledSize, R.drawable.games_button, R.drawable.games_button_pressed);	
		m_SleepButton.SetButtonInformation(0, 0, nButtonScaledSize, nButtonScaledSize, R.drawable.sleep_button, R.drawable.sleep_button_pressed );	
		m_BrushButton.SetButtonInformation(0, 0, nButtonScaledSize, nButtonScaledSize, R.drawable.brush_button, R.drawable.brush_button_pressed );	
		m_BattleButton.SetButtonInformation(0, 0, nButtonScaledSize, nButtonScaledSize, R.drawable.battle_button, R.drawable.battle_button_pressed );
		m_MedicineButton.SetButtonInformation(0, 0, nButtonScaledSize, nButtonScaledSize, R.drawable.medicine_button, R.drawable.medicine_button_pressed );
		

		StoryBook.Chapter(NyanimalPages.Chapters.RenderHash).Subscribe(NyanimalPages.RenderListCreateRunEvent.class, this);
	}
	
	private void CalculateButtonSize(CButton button )
	{		
		// get all ratios
		button.GetUpRect().MakeTextureSizeLocal(); // so we have soemthing to go off of. will reset later
		float[] nButtonSize = button.GetUpRect().GetTransform().GetLocalSize();
		float fScreenScale = nButtonSize[0] / 1080.0f; // based on Nexus 5 screen width of 1080 and frame size
		float fButtonScale = fScreenScale * 0.35f;
		float fButtonRatio = (float)nButtonSize[0] / (float)nButtonSize[1];
				
		// get the final size
		float fFinalWidth = (Utilities.Screen.GetWidth() * fButtonScale);
		float fFinalHeight = (fFinalWidth * fButtonRatio * 1.0f);
		
		if(nButtonSize[0] > nButtonSize[1])
		{
			fFinalHeight = (fFinalWidth / fButtonRatio);
		}

		// set the rect transforms to default size
		button.GetUpRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		button.GetPressedRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		
		// set the final size
		button.GetTransform().SetSize((int)fFinalWidth, (int)fFinalHeight);
	}
	
	private void CalculateMeterSize(CUIBar meter )
	{
		// force buttons to defauls
		meter.MakeTextureSize();
		
		// get all ratios
		float[] nButtonSize = meter.GetTransform().GetSize();
		float fScreenScale = nButtonSize[0] / 1080.0f; // based on Nexus 5 screen width of 1080 and frame size
		float fButtonScale = fScreenScale * 0.35f;
		float fButtonRatio = (float)nButtonSize[1] / (float)nButtonSize[0];
		
		
		// get the final size
		float fFinalWidth = (Utilities.Screen.GetWidth() * fButtonScale);
		float fFinalHeight = (fFinalWidth * fButtonRatio * 1.1f);
		/*
		if(nButtonSize[0] < nButtonSize[1])
		{
			nFinalHeight = (int)((float)nFinalWidth / fButtonRatio);
		}
		*/
		// set the final size
		meter.GetTransform().SetSize((int)fFinalWidth, (int)fFinalHeight);
	}

	public void SetWatchObject(CRoomState Room, CNyanimal nyanimal)
	{
		m_NyanimalToWatch = nyanimal;
		
		// set all the button actions
		m_FeedButton.SetAction(Room.new FeedTouch());
		m_WashButton.SetAction(Room.new WashTouch());
		m_ExitButton.SetAction(Room.new ExitTouch());
		m_BrushButton.SetAction(Room.new BrushTouch());
		m_GamesButton.SetAction(Room.new GamesTouch());
		m_StatusButton.SetAction(Room.new StatTouch());
		m_SleepButton.SetAction(Room.new SleepTouch());
		m_BattleButton.SetAction(Room.new BattleTouch());
		m_MedicineButton.SetAction(Room.new MedicineTouch());
		
	}

	@Override
	public void Update() 
	{	
		// set all the bars
		if(m_NyanimalToWatch != null)
		{
			m_HungerMeter.SetCurrentValue((int)m_NyanimalToWatch.GetHungerMeter());
			m_EnergyMeter.SetCurrentValue((int)m_NyanimalToWatch.GetTiredMeter());
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////
	}

	@Override
	public RectF CheckCollision(CNyanObject inner)
	{
		
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap) 
	{
		
		
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{
		m_WashButton.HandleTouch(nPosX, nPosY, nTouchAction);
		m_FeedButton.HandleTouch(nPosX, nPosY, nTouchAction);
		m_ExitButton.HandleTouch(nPosX, nPosY, nTouchAction); 
		m_GamesButton.HandleTouch(nPosX, nPosY, nTouchAction);
		m_BrushButton.HandleTouch(nPosX, nPosY, nTouchAction);
		m_SleepButton.HandleTouch(nPosX, nPosY, nTouchAction);
		m_StatusButton.HandleTouch(nPosX, nPosY, nTouchAction);
		m_BattleButton.HandleTouch(nPosX, nPosY, nTouchAction); 
		m_MedicineButton.HandleTouch(nPosX, nPosY, nTouchAction); 
	
		return true;
	}

	@Override
	public Type GetType()
	{
		
		return null;
	}

	@Override
	public void Destroy()
	{
		CNyanimalFactoy.ReturnObject(m_TopBar);
		CNyanimalFactoy.ReturnObject(m_BottomBar);
		CNyanimalFactoy.ReturnObject(m_HeartShine);
		CNyanimalFactoy.ReturnObject(m_FeedButton);
		CNyanimalFactoy.ReturnObject(m_WashButton);
		CNyanimalFactoy.ReturnObject(m_ExitButton);
		CNyanimalFactoy.ReturnObject(m_BrushButton);
		CNyanimalFactoy.ReturnObject(m_GamesButton);
		CNyanimalFactoy.ReturnObject(m_SleepButton);
		CNyanimalFactoy.ReturnObject(m_HungerMeter);
		CNyanimalFactoy.ReturnObject(m_EnergyMeter);
		CNyanimalFactoy.ReturnObject(m_StatusButton);
		CNyanimalFactoy.ReturnObject(m_BattleButton);
		CNyanimalFactoy.ReturnObject(m_MedicineButton);
	}
	
	@Override
	public void ReadPage(IPage page)
	{
		if(page instanceof NyanimalPages.RenderListCreateRunEvent)
		{
			CreateHUD();
		}
		else
		{
			super.ReadPage(page);
		}
	}

    public void CreateHUD()
    {
		Utilities.Log.Debug(CRoomHUD.class.getName(), "Creating HUD");
		int nCenter =  Utilities.Screen.GetWidth() / 2;
		int nBarWidth =	nCenter;	
		int nUpperBarHeight = nBarWidth / (m_TopBar.GetTexture().GetWidth() / m_TopBar.GetTexture().GetHeight());
		int nLowerBarHeight = nBarWidth / (m_BottomBar.GetTexture().GetWidth() / m_BottomBar.GetTexture().GetHeight());
		
		m_TopBar.GetTransform().SetPosition(nBarWidth, (nUpperBarHeight));
		m_TopBar.GetTransform().SetSize(nBarWidth, nUpperBarHeight); 
		m_TopBar.SetDrawLayer(Utilities.CommonLayers.HudBase);
		
		m_BottomBar.GetTransform().SetPosition(nBarWidth, Utilities.Screen.GetHeight() - (nLowerBarHeight));
		m_BottomBar.GetTransform().SetSize(nBarWidth, nLowerBarHeight); 
		m_BottomBar.SetDrawLayer(Utilities.CommonLayers.HudBase);
		
		m_HeartShine.GetTransform().SetPosition(nBarWidth, (nUpperBarHeight));
		m_HeartShine.GetTransform().SetSize(nBarWidth, nUpperBarHeight); 
		m_HeartShine.SetDrawLayer(Utilities.CommonLayers.HudMisc);
		
		int nOffset = (int)(Utilities.Screen.GetWidth() * (18.0f / 1080.0f));
		int nHeightOffset = Utilities.Screen.GetHeight() / 100;
		int nHeightPos = Utilities.Screen.GetHeight() - nHeightOffset;
		float[] nButtonSize = new float[2];

		CalculateButtonSize(m_BrushButton);
		nButtonSize = m_BrushButton.GetTransform().GetSize();
		m_BrushButton.GetTransform().SetPosition((nCenter) - ((nButtonSize[0] + nOffset) * 6), nHeightPos - (int)(nButtonSize[1] * 3.0f));
		
		CalculateButtonSize(m_FeedButton);	
		nButtonSize = m_FeedButton.GetTransform().GetSize();
		m_FeedButton.GetTransform().SetPosition((nCenter) - ((nButtonSize[0] + nOffset) * 4), nHeightPos - (int)(nButtonSize[1] * 1.5f));

		CalculateButtonSize(m_GamesButton);	
		nButtonSize = m_GamesButton.GetTransform().GetSize();
		m_GamesButton.GetTransform().SetPosition((nCenter) - ((nButtonSize[0] + nOffset) * 2), nHeightPos - (nButtonSize[1]));

		CalculateButtonSize(m_StatusButton);
		nButtonSize = m_StatusButton.GetTransform().GetSize();
		m_StatusButton.GetTransform().SetPosition((nCenter) + ((nButtonSize[0] + nOffset) * 0), nHeightPos - (nButtonSize[1]));

		CalculateButtonSize(m_WashButton);
		nButtonSize = m_WashButton.GetTransform().GetSize();
		m_WashButton.GetTransform().SetPosition((nCenter) + ((nButtonSize[0] + nOffset) * 2), nHeightPos - (nButtonSize[1]));	

		CalculateButtonSize(m_SleepButton);
		nButtonSize = m_SleepButton.GetTransform().GetSize();
		m_SleepButton.GetTransform().SetPosition((nCenter) + ((nButtonSize[0] + nOffset) * 4), nHeightPos - (int)(nButtonSize[1] * 1.5f));

		CalculateButtonSize(m_ExitButton);
		nButtonSize = m_ExitButton.GetTransform().GetSize();	
		m_ExitButton.GetTransform().SetPosition((nCenter) + ((nButtonSize[0] + nOffset) * 6), nHeightPos - (int)(nButtonSize[1] * 3.0f));

		// do the top buttons
		nHeightPos = nUpperBarHeight * 2;

		CalculateButtonSize(m_MedicineButton);
		nButtonSize = m_MedicineButton.GetTransform().GetSize();	
		m_MedicineButton.GetTransform().SetPosition(nButtonSize[1] + nOffset, nHeightPos - (int)(nButtonSize[1] * 1.7f));

		CalculateButtonSize(m_BattleButton);
		nButtonSize = m_BattleButton.GetTransform().GetSize();	
		m_BattleButton.GetTransform().SetPosition(Utilities.Screen.GetWidth() - nButtonSize[1] - nOffset, nHeightPos - (int)(nButtonSize[1] * 1.7f));
		
		CalculateMeterSize(m_HungerMeter);
		nButtonSize = m_HungerMeter.GetTransform().GetSize();	
		int nMeterX = nCenter - ((int)(nButtonSize[0] * 1.18f));
		int nMeterY = (int)(nButtonSize[1] * 1.15f);		
		m_HungerMeter.GetTransform().SetPosition(nMeterX, nMeterY);
		
		CalculateMeterSize(m_EnergyMeter);
		nButtonSize = m_EnergyMeter.GetTransform().GetSize();	
		nMeterX = nCenter + ((int)(nButtonSize[0] * 1.06f));
		nMeterY = (int)(nButtonSize[1] * 1.175f);		
		m_EnergyMeter.GetTransform().SetPosition(nMeterX, nMeterY);

		StoryBook.Chapter(NyanimalPages.Chapters.RenderHash).Unsubscribe(NyanimalPages.RenderListCreateRunEvent.class, this);
    }

	@Override
    public void OnGraphicsInit()
    {
    }

	@Override
    public void OnEnable()
    {
    }

	@Override
    public void OnDisable()
    {
    }
}
