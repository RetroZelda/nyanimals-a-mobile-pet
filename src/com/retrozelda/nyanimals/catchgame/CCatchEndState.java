package com.retrozelda.nyanimals.catchgame;

import android.graphics.RectF;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.CButton;
import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimalFactoy;
import com.retrozelda.nyanimals.CNyanimalsGameFSMConstructor;
import com.retrozelda.nyanimals.CRoomState;
import com.retrozelda.nyanimals.IButtonAction;
import com.retrozelda.nyanimals.R;
import com.retrozelda.nyanimals.catchgame.CCatchGameOverState.ContinueTouch;
import com.retrozelda.utilities.Utilities;

public class CCatchEndState extends CNyanState
{
	class PlayAgainTouch implements IButtonAction
	{
		@Override
		public void HandlePress() 
		{			
		}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CCatchFSMConstructor.CHAPTERHASH).ChangeState(CCatchGameIntroState.class, _Nyanimal, _Catcher);
		}
	}	
	
	class ExitTouch implements IButtonAction
	{
		@Override
		public void HandlePress() 
		{			
		}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).ChangeState(CRoomState.class);
		}
	}	
	
	private CNyanimal _Nyanimal;
	private CCatcher _Catcher;
	
	CButton _PlayAgainButton;
	CButton _ExitButton;
	
	@Override
    public void OnCreate()
    {
		_PlayAgainButton = CNyanimalFactoy.RequestObject(CButton.class);
		_ExitButton = CNyanimalFactoy.RequestObject(CButton.class);

		_PlayAgainButton.SetButtonInformation(0, 0, 0, 0, R.drawable.catch_again, R.drawable.catch_again);
		_ExitButton.SetButtonInformation(0, 0, 0, 0, R.drawable.catch_back, R.drawable.catch_back);
		
		_PlayAgainButton.SetAction(new PlayAgainTouch());
		_ExitButton.SetAction(new ExitTouch());

		_PlayAgainButton.SetDraw(false);
		_ExitButton.SetDraw(false);
		_PlayAgainButton.SetActive(false);
		_ExitButton.SetActive(false);
    }
	
	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
		_Catcher = (CCatcher)_PassthroughObjects[1];
	}
	
	@Override
	public void OnEnter()
	{
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nScreenHeight = Utilities.GLSurfaceView.getHeight();

		_PlayAgainButton.MakeScaledSize();
		_ExitButton.MakeScaledSize();

		float fScale = 0.25f;
		_PlayAgainButton.GetTransform().Scale(fScale, fScale);
		_ExitButton.GetTransform().Scale(fScale, fScale);
		
		float[] nArrowSize = _PlayAgainButton.GetTransform().GetSize();
		_PlayAgainButton.GetTransform().SetPosition((nScreenWidth / 2) + (nArrowSize[0] * 2), nScreenHeight - (nArrowSize[1]));
		
		nArrowSize = _ExitButton.GetTransform().GetSize();
		_ExitButton.GetTransform().SetPosition((nScreenWidth / 2) - (nArrowSize[0] * 2), nScreenHeight - (nArrowSize[1]));
	}

	@Override
    public void OnPause()
    {
		_PlayAgainButton.SetDraw(false);
		_PlayAgainButton.SetActive(false);

		_ExitButton.SetDraw(false);
		_ExitButton.SetActive(false);
    }

	@Override
    public void OnResume()
    {
		_PlayAgainButton.SetDraw(true);
		_PlayAgainButton.SetActive(true);

		_ExitButton.SetDraw(true);
		_ExitButton.SetActive(true);
    }


	@Override
    public void PriorityUpdate()
    {
    }

	@Override
    public void Update()
    {
    }

	@Override
    public void LateUpdate()
    {
    }

	@Override
    public void OnDestroy()
    {
		_PlayAgainButton.SetAction(null);
		_ExitButton.SetAction(null);

		CNyanimalFactoy.ReturnObject(_PlayAgainButton);
		CNyanimalFactoy.ReturnObject(_ExitButton);
    }

	@Override
    public RectF CheckCollision(CNyanObject inner)
    {
	    return null;
    }

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

}