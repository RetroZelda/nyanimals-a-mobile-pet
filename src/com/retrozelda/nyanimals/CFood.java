/**
 * 
 */
package com.retrozelda.nyanimals;

import java.util.Hashtable;

import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;


/**
 * @author Erick
 *
 */
public class CFood extends CRectangle 
{
	public enum FoodStates { NORMAL, SPOILED };
	public enum FoodStatus 
	{ 
		FULL, EATEN_0, EATEN_1, EATEN_2, EMPTY;

		public FoodStatus Next() 
		{
			int myVal = this.ordinal();
			int nNextVal = myVal + 1;
			int nLastVal = FoodStatus.values().length - 1;
			FoodStatus[] all = FoodStatus.values();
			
			if(nNextVal > nLastVal)
			{
				return EMPTY;
			}
			
			return all[myVal + 1];
		}
	};

	Hashtable<FoodStates, Hashtable<FoodStatus, _AnimationTextureLink>> m_FoodAnimations;
	FoodStates m_CurState;
	FoodStatus m_CurStatus;

	CRectangle m_Shadow;
	
	// stats
	static float METER_MAX = 100.0f;
	float m_fSpoilMeter;
	float m_fBiteMeter;
	
	// rates
	float m_fSpoilRate;
	float m_fBiteAmmount;

	// others
	boolean m_bDragging;
	boolean m_bWasDragging;
	boolean m_bHeld;
	CNyanimal m_HeldBy;
	
	float GetSpoilMeter() { return m_fSpoilMeter;}
	float GetBiteMeter() { return m_fBiteMeter;}
	float GetSpoilRate() { return m_fSpoilRate;}
	float GetBiteAmmount() { return m_fBiteAmmount;}
	
	public FoodStates GetFoodState() { return m_CurState;}

	public void SetSpoilMeter(float fAmt) { m_fSpoilMeter = fAmt;}
	public void SetBiteMeter(float fAmt) { m_fBiteMeter = fAmt;}
	public void SetSpoilRate(float fAmt) { m_fSpoilRate = fAmt;}
	public void SetBiteAmmount(float fAmt) { m_fBiteAmmount = fAmt;}

	public boolean StoppedDragging() { return m_bDragging == false && m_bWasDragging == true;}
	public boolean IsDragging() { return m_bDragging;}
	public boolean IsHeld() { return m_bHeld;}
	
	public void PickedUp(CNyanimal other)
	{
		m_HeldBy = other;
		// what happens when an object picks us up
		if(m_HeldBy != null)
		{
			m_bDragging = false;
			m_bWasDragging = false;
			m_bHeld = true;
			SetDrawLayer(Utilities.CommonLayers.ObjectHold);
		}
		// what happens when an object puts us down
		else
		{
			m_bHeld = false;
			SetDrawLayer(Utilities.CommonLayers.Object);
		}
	}
	
	public float TakeBite(float fBite)
	{
		m_fBiteMeter -= fBite;
		if(m_fBiteMeter <= 0.0f)
		{
			m_CurStatus = m_CurStatus.Next();
			m_fBiteMeter = METER_MAX;
			
			Utilities.Log.Debug("CFood", "Food is now at bite: " + m_CurStatus.toString());
			
			// check if empty
			if(m_CurStatus == FoodStatus.EMPTY)
			{
				Utilities.Log.Debug("CFood", "Food Empty!");
				SetObjectStatus(ObjStatus.TO_DELETE);
				return -1.0f;
			}
		}
		
		return GetBiteAmmount();
	}
	
	public CFood()
	{
		m_FoodAnimations = new Hashtable<FoodStates, Hashtable<FoodStatus, _AnimationTextureLink>>();
		m_FoodAnimations.put(FoodStates.NORMAL, new Hashtable<FoodStatus, _AnimationTextureLink>());
		m_FoodAnimations.put(FoodStates.SPOILED, new Hashtable<FoodStatus, _AnimationTextureLink>());

		m_bDragging = false;
		m_bWasDragging = false;
		m_bHeld = false;
		m_HeldBy = null;
		
		m_fSpoilMeter = METER_MAX;
		m_fBiteMeter = METER_MAX;
		m_fBiteAmmount = 25.0f;
		m_fSpoilRate = 1.0f;
	}
	
	@Override
	public void SetDraw(boolean bDraw) 
	{
		super.SetDraw(bDraw);
		m_Shadow.SetDraw(bDraw);
	}

	@Override
	public void SetActive(boolean bActive)
	{
		super.SetActive(bActive);
		m_Shadow.SetActive(bActive);
	}

	protected void InitAnimations()
	{
		int nFrameSize = 128;		
		
		_AnimationTextureLink normalLink = new _AnimationTextureLink();		
		_AnimationTextureLink normalLink_bite_1 = new _AnimationTextureLink();	
		_AnimationTextureLink normalLink_bite_2 = new _AnimationTextureLink();	
		_AnimationTextureLink normalLink_bite_3 = new _AnimationTextureLink();	
		normalLink.Create("food_normal", R.drawable.meat, nFrameSize, nFrameSize, 1, 1, 0, true);
		normalLink_bite_1.Create("food_normal_bite_1", R.drawable.meatbitten1x, nFrameSize, nFrameSize, 1, 1, 0, true);	
		normalLink_bite_2.Create("food_normal_bite_2", R.drawable.meatbitten2x, nFrameSize, nFrameSize, 1, 1, 0, true);	
		normalLink_bite_3.Create("food_normal_bite_3", R.drawable.meatbitten3x, nFrameSize, nFrameSize, 1, 1, 0, true);	
		m_FoodAnimations.get(FoodStates.NORMAL).put(FoodStatus.FULL, normalLink);		
		m_FoodAnimations.get(FoodStates.NORMAL).put(FoodStatus.EATEN_0, normalLink_bite_1);		
		m_FoodAnimations.get(FoodStates.NORMAL).put(FoodStatus.EATEN_1, normalLink_bite_2);		
		m_FoodAnimations.get(FoodStates.NORMAL).put(FoodStatus.EATEN_2, normalLink_bite_3);
		
		
		_AnimationTextureLink spoiledLink = new _AnimationTextureLink();	
		_AnimationTextureLink spoiledLink_bite_1 = new _AnimationTextureLink();	
		_AnimationTextureLink spoiledLink_bite_2 = new _AnimationTextureLink();	
		_AnimationTextureLink spoiledLink_bite_3 = new _AnimationTextureLink();		
		spoiledLink.Create("food_spoiled", R.drawable.meatrotten, nFrameSize, nFrameSize, 1, 1, 0, true);
		spoiledLink_bite_1.Create("food_spoiled_bite_1", R.drawable.meatbitten1xrot, nFrameSize, nFrameSize, 1, 1, 0, true);	
		spoiledLink_bite_2.Create("food_spoiled_bite_2", R.drawable.meatbitten2xrot, nFrameSize, nFrameSize, 1, 1, 0, true);	
		spoiledLink_bite_3.Create("food_spoiled_bite_3", R.drawable.meatbitten3xrot, nFrameSize, nFrameSize, 1, 1, 0, true);
		m_FoodAnimations.get(FoodStates.SPOILED).put(FoodStatus.FULL, spoiledLink);		
		m_FoodAnimations.get(FoodStates.SPOILED).put(FoodStatus.EATEN_0, spoiledLink_bite_1);		
		m_FoodAnimations.get(FoodStates.SPOILED).put(FoodStatus.EATEN_1, spoiledLink_bite_2);		
		m_FoodAnimations.get(FoodStates.SPOILED).put(FoodStatus.EATEN_2, spoiledLink_bite_3);
		
		
		m_CurState = FoodStates.NORMAL;
		m_CurStatus = FoodStatus.FULL;
		UpdateUV();
	}
	

	@Override
	public void Init()
	{
		super.Init();

		float fScreenScale = 0.118f; // based on Nexus 5 screen width of 1080 and frame size of 128
		float fFoodScale = fScreenScale * 0.5f; // food was half the frame
		float fFoodRatio = 1.0f;
		int nFoodWidth = (int)(Utilities.Screen.GetWidth() * fFoodScale);
		int nFoodHeight = (int)(nFoodWidth * fFoodRatio);
				
		// set the shadow
		m_Shadow = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_Shadow.SetResourceID(R.drawable.shadow);
		m_Shadow.SetDrawLayer(Utilities.CommonLayers.Shadow);
		
		GetTransform().SetSize(nFoodWidth, nFoodHeight);
		m_Shadow.GetTransform().SetParent(GetTransform());
	}

	@Override
	public void Destroy()
	{
		CNyanimalFactoy.ReturnObject(m_Shadow);
	}

	@Override
    public void OnEnable()
    {
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Subscribe(NyanimalPages.ScreenTouchEvent.class, this);
    }

	@Override
    public void OnDisable()
    {
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Unsubscribe(NyanimalPages.ScreenTouchEvent.class, this);
    }
	
	private float _MaxY;

	@Override
	public void Update()
	{
		super.Update();
		float deltaTime = Utilities.Time.DeltaTime_Seconds();
		
		// only fall on top 4/5th of hte room
		float minX = 0;
		float maxX = Utilities.Screen.GetWidth();
		float minY = 0;
		_MaxY = Utilities.Screen.GetHeight() - (Utilities.Screen.GetHeight() / 5);
		
		if(m_bHeld == true)
		{
			if(m_HeldBy != null)
			{
				// set shadow pos
				_MaxY = m_HeldBy.GetTransform().getPosition()[1];
			}
			else
			{
				Utilities.Log.Debug("CFood", "Held but not by anything!");
			}
		}
		else
		{
				
			// falling stuff
			if(m_bDragging == false)
			{					
				
				float fFallRate = 40.0f;
	
				// check if the new position is valid	
				float[] projPos = GetTransform().getPosition();
				if((projPos[0] >= minX) && (projPos[1] >= minY) && (projPos[0] <= maxX) && (projPos[1] <= _MaxY))
				{
					// falling	
					//Log.d("CFood", "Falling!");
					float[] nPos = GetTransform().getPosition();
					float nNewY = nPos[1] + fFallRate;
					GetTransform().SetPosition(nPos[0], nNewY);
				}
				else
				{
					// not falling
					//Log.d("CFood", "Not Falling!");
				}
			}
			
			// update out spoiled amount
			if(m_CurState != FoodStates.SPOILED)
			{
				m_fSpoilMeter -= m_fSpoilRate * deltaTime;
				if(m_fSpoilMeter <= 0.0f)
				{
					Utilities.Log.Debug("CFood", "Food is now Spoiled!");
					m_CurState = FoodStates.SPOILED;
				}
			}
		}
		
		// update our animations
		UpdateAnimationFrame(deltaTime);
	}

	@Override
	public void LateUpdate()
	{
		// update the shadow
		float fShadowOffset = GetTransform().GetSize()[1] * 0.9f;
		float[] scl = GetTransform().GetSize();
		float[] pos = GetTransform().getPosition();
		if(m_Shadow != null)
		{
			float fDropOffset = _MaxY - pos[1];
			if(fDropOffset > 0.0f)
			{
				fShadowOffset += (fDropOffset);
			}
			fShadowOffset /= scl[1];
			m_Shadow.GetTransform().SetPosition(0.0f, fShadowOffset);
		}
	}

	// in: touch position, touch state
	// touch state: 0 = button is pressed.  1 = pressed.  2 = held.  3 = released
	@Override
    public boolean HandleTouch(int touchX, int touchY, int nTouchState)
	{
		m_bWasDragging = m_bDragging;
		
		// dont do anything if we are held
		if(m_HeldBy != null)
		{
			return false;
		}
		
		RectF boundingRect = GetBoundingRect();
		if(boundingRect != null)
		{
			boolean bTouchState = boundingRect.contains(touchX, touchY);
			switch(nTouchState)
			{
				case MotionEvent.ACTION_MOVE:
					if(m_bDragging == true)
					{
						GetTransform().SetPosition(touchX, touchY);
					}
					break;
				case MotionEvent.ACTION_UP:
					
					// stop dragging
					if(m_bDragging == true)
					{
						m_bDragging = false;
						SetDrawLayer(Utilities.CommonLayers.Object);
					}
					break;
				case MotionEvent.ACTION_DOWN:
					
					// start dragging if we touched the food
					if(bTouchState == true)
					{
						m_bDragging = true;
						SetDrawLayer(Utilities.CommonLayers.TouchHold);
					}
					else
					{
						// Log.d("CFood", "Drag Miss!");
					}
					break;
			}
			
		}
		else	
		{
			Log.d(CButton.class.getSimpleName(), "Invalid Food!");
		}
		return m_bDragging;
	}
	

	// TODO: put UpdateAnimationFrame and UpdateUV in an interface
	private void UpdateAnimationFrame(float deltaTime)
	{
		try
		{
			boolean newFrame = m_FoodAnimations.get(m_CurState).get(m_CurStatus).GetAnimation().Update(deltaTime);
			
			// check if we have new UVs
			if(newFrame == true)
			{
				UpdateUV();
			}
		}
		catch (Exception e)
		{
			
		}
	}

	protected void UpdateUV()
	{
		// get the UV rect
		m_Texture =  m_FoodAnimations.get(m_CurState).get(m_CurStatus).GetTexture();
		RectF frameRect = m_FoodAnimations.get(m_CurState).get(m_CurStatus).GetAnimation().GetFrameUV(m_Texture.GetWidth(), m_Texture.GetHeight(), m_bMirror);

		// put the rect where it belongs in the vertices
		// top right
		m_TriangleVerticesData[3] = frameRect.right;
		m_TriangleVerticesData[4] = frameRect.top;
 
		// bottom right
		m_TriangleVerticesData[8] = frameRect.right;
		m_TriangleVerticesData[9] = frameRect.bottom;

		// top left
		m_TriangleVerticesData[13] = frameRect.left;
		m_TriangleVerticesData[14] = frameRect.top;

		// bottom left
		m_TriangleVerticesData[18] = frameRect.left;
		m_TriangleVerticesData[19] = frameRect.bottom;

		// save
		m_TriangleVertices.position(0);
		m_TriangleVertices.put(m_TriangleVerticesData).position(0);
	}

	@Override
	public void HandleCollision(CNyanObject other, RectF overlap)
	{
		super.HandleCollision(other, overlap);
		
		if(other.GetType() == Type.FOOD || other.GetType() == Type.MEDICINE)
		{
			CFood otherFood = (CFood)other;
			if(otherFood.IsDragging() == false && IsDragging() == false)
			{
				float[] pos = GetTransform().getPosition();
				float[] newPos = GetTransform().getPosition();
				
				// adjust for the X
				if(pos[0] >= overlap.centerX())
				{
					newPos[0] += overlap.width();
				}
				else
				{
					newPos[0] -= overlap.width();
				}
				
				// adjust for the Y
				if(pos[1] >= overlap.centerY())
				{
					newPos[1] += overlap.height();
				}
				else
				{
					newPos[1] -= overlap.height();
				}
				
				// smooth to the new pos
				float[] smoothPos = new float[2];
				float nDamping = 20.0f;
				smoothPos[0] = pos[0] + ((newPos[0] - pos[0]) / nDamping);
				smoothPos[1] = pos[1] + ((newPos[1] - pos[1]) / nDamping);
				GetTransform().SetPosition(smoothPos[0], smoothPos[1]);
				//SetPosition(newPos[0], newPos[1]);
			}
		}
		
	}

	@Override
	public Type GetType() 
	{
		return Type.FOOD;
	}
	
	@Override
    public void OnGraphicsInit()
    {
		InitAnimations();
		m_Shadow.MakeTextureSize();
		float fScaleAmt = 0.5f;
		m_Shadow.GetTransform().Scale(fScaleAmt, fScaleAmt);
		/*
		float[] nSize = m_Shadow.GetTransform().GetLocalSizeF();
		float[] mySize = GetTransform().GetSizeF();
		m_Shadow.GetTransform().SetSize(nSize[0] / mySize[0], nSize[1] / mySize[1]);
		*/
    }
    
}
