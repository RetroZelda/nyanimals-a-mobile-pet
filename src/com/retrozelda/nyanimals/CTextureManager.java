package com.retrozelda.nyanimals;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;

import com.retrozelda.utilities.Utilities;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

public class CTextureManager 
{
	private static CTextureManager m_Instance = new CTextureManager();
	public static CTextureManager GetInstance()
	{
		return m_Instance;
	}	
	
	private Hashtable<Integer, CTexture> m_Textures;
	private Resources _Resources;
	Iterator<Hashtable.Entry<Integer, CTexture>> _TextureIterator;
	
	private CTextureManager()
	{
		m_Textures = new Hashtable<Integer, CTexture>();
		_Resources = Utilities.GameActivity.getResources();
	}

	public void PreloadTextures(int... ids)
	{
		for(int id : ids)
		{
			LoadTexture(id);
		}
	}

	public CTexture GetTexture(int resource)
	{
		CTexture texture = m_Textures.get(resource);
		return texture;
	}
	
	public CTexture LoadTexture(int resource)
	{
		CTexture texture = m_Textures.get(resource);
		if(texture == null)
		{
			texture = LoadTexture_Internal(resource);
		}
		
		return texture;
	}

	private CTexture LoadTexture_Internal(int resource)
	{
		// texture data
		CTexture newTex = new CTexture();
		LoadTexture_Internal(resource, newTex);
		m_Textures.put(resource, newTex);
		return newTex;
	}

	private CTexture LoadTexture_Internal(int resource, CTexture newTex)
	{
		// texture data
		int nTextureID = -1;
		int nWidth = -1;
		int nHeight = -1;
		
		// create the texture
		// NOTE: textures need to be done each time the main surface is created
		int[] textures = new int[1];
		GLES20.glGenTextures(1, textures, 0);

		// bind the texture
		nTextureID = textures[0];
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, nTextureID);

		// set the mip and mag
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

		// set the wrap mode
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);

		// load the texture image
		InputStream is = _Resources.openRawResource(resource);
		Bitmap bitmap = null;
		try
		{
			bitmap = BitmapFactory.decodeStream(is);
			nWidth = bitmap.getWidth();
			nHeight = bitmap.getHeight();
		}
		catch(OutOfMemoryError e)
		{
			e.printStackTrace();
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				is.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}


		// free the texture
		if(bitmap != null)
		{
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
			bitmap.recycle();
			
			// set the data
			newTex.SetData(nTextureID, nWidth, nHeight);
		}
		return newTex;
	}


	public void ReloadTextures() 
	{
		_TextureIterator = m_Textures.entrySet().iterator();

		while (_TextureIterator.hasNext()) 
		{
			Entry<Integer, CTexture> entry = _TextureIterator.next();			
			LoadTexture_Internal(entry.getKey(), entry.getValue());
		} 	
		_TextureIterator = null;
	}
	
	// ret: true if all textures reloaded, false if still loading
	public boolean ReloadTexturesAsync()
	{
		if(_TextureIterator == null)
		{
			_TextureIterator = m_Textures.entrySet().iterator();
		}
		
		if(_TextureIterator.hasNext()) 
		{
			Entry<Integer, CTexture> entry = _TextureIterator.next();			
			LoadTexture_Internal(entry.getKey(), entry.getValue());
			return false;
		} 	
		else
		{
			_TextureIterator = null;
			return true;
		}
	}
}
