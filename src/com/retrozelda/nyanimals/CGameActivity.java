package com.retrozelda.nyanimals;

import com.facebook.AppEventsLogger;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

/// main game class
public class CGameActivity extends Activity
{
	// tag
	private static final String		TAG	= CGameActivity.class.getSimpleName();
	
	private boolean _bGameRunning = false;
	private void StartGame()
	{
		// init openGL
		Utilities.GameActivity = this;
		Utilities.GLRenderer = new CNyanimalGLRender();
		Utilities.GLSurfaceView = new NyanimalGLSurfaceView(Utilities.GameActivity);// , size.x, size.y);

		// set the game panel
		setContentView(Utilities.GLSurfaceView);
		
		// flag
		_bGameRunning = true;
	}

	public CGameActivity()
	{
	}

	/*
	 * public CNyanimalGame GetGame() { return m_updateThread.GetGame(); }
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		Utilities.Log.Debug(TAG, "Create Start");
		super.onCreate(savedInstanceState);

		// turn off stuff
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
	    		WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	    // Start the Game
	    StartGame();
		
		Utilities.Log.Debug(TAG, "Create End");
	}
	
	@Override
	public void onBackPressed() 
	{
		StoryBook.Chapter(NyanimalPages.Chapters.Input).Mark(new NyanimalPages.BackButtonEvent(true));		
	}
	
	@Override
	protected void onStart()
	{
		Utilities.Log.Debug(TAG, "Start Start");
		//Utilities.GameClass.onStart(); 
		super.onStart();
		Utilities.Log.Debug(TAG, "Start End");
	}

	@Override
	protected void onStop()
	{
		Utilities.Log.Debug(TAG, "Stop Start");
		//Utilities.GameClass.onStop();
		super.onStop();
		Utilities.Log.Debug(TAG, "Stop End");
	}
	
	@Override
	protected void onDestroy()
	{
		Utilities.Log.Debug(TAG, "Destroy Start");
		//Utilities.GameClass.onDestroy();
		super.onDestroy();
		// System.runFinalizersOnExit(true);
		Utilities.Log.Debug(TAG, "Destroy End");
	}

	@Override
	protected void onPause()
	{
		Utilities.Log.Debug(TAG, "Pause Start");
	    //Utilities.GLSurfaceView.setVisibility(View.GONE);
		//Utilities.GameClass.onPause();
		//Utilities.GLSurfaceView.onPause();
		super.onPause();
				
		Utilities.Log.Debug(TAG, "Pause End");
	}

	@Override
	protected void onResume()
	{
		Utilities.Log.Debug(TAG, "Resume Start");
		//Utilities.GameClass.onResume();
		//Utilities.GLSurfaceView.onResume();
		super.onResume(); 
		
		Utilities.Log.Debug(TAG, "Resume End");
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
	    super.onWindowFocusChanged(hasFocus);
	    /*
	    if (hasFocus && Utilities.GLSurfaceView.getVisibility() == View.GONE) 
	    {
	    	Utilities.GLSurfaceView.setVisibility(View.VISIBLE);
	    }
	    */
	}
}
