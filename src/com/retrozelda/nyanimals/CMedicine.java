/**
 * 
 */
package com.retrozelda.nyanimals;

import android.graphics.RectF;


/**
 * @author Erick
 *
 */
public class CMedicine extends CFood 
{
	@Override
	public float TakeBite(float fBite)
	{
		SetObjectStatus(ObjStatus.TO_DELETE);
		return -1.0f;
	}
	
	public CMedicine()
	{
		super();
	}

	@Override
	protected void InitAnimations()
	{
		int nFrameSize = 128;		
		
		_AnimationTextureLink normalLink = new _AnimationTextureLink();		
		normalLink.Create("food_normal", R.drawable.medicinalherbs, nFrameSize, nFrameSize, 1, 1, 0, true);
		m_FoodAnimations.get(FoodStates.NORMAL).put(FoodStatus.FULL, normalLink);		
		
		
		m_CurState = FoodStates.NORMAL;
		m_CurStatus = FoodStatus.FULL;
		UpdateUV();
	}
	

	@Override
	public void Init()
	{
		super.Init();
		InitAnimations();
		m_fSpoilRate = 0.0f;
	}
	


	@Override
	public void Update()
	{
		super.Update();
		
		if(m_CurState == FoodStates.SPOILED)
		{
			m_CurState = FoodStates.NORMAL;
		}
	}

	@Override
	public void Draw()
	{
		super.Draw();
	}

	@Override
	public void HandleCollision(CNyanObject other, RectF overlap)
	{
		super.HandleCollision(other, overlap);
	}

	@Override
	public Type GetType() 
	{
		return Type.MEDICINE;
	}
    
}
