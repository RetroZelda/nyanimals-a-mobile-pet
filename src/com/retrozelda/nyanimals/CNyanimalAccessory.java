package com.retrozelda.nyanimals;

import java.util.Hashtable;

import android.graphics.RectF;

public class CNyanimalAccessory extends CRectangle
{
	Hashtable<CNyanimal.NyanimalAnimations, _AnimationTextureLink> m_Animations;
	
	CNyanimal m_Nyanimal;
	CNyanimal.NyanimalAnimations m_curAnimation;
	
	public CNyanimalAccessory()
	{
		super();
		
		m_Animations = new Hashtable<CNyanimal.NyanimalAnimations, _AnimationTextureLink>();
		m_Nyanimal = null;
		m_curAnimation = CNyanimal.NyanimalAnimations.IDLE;
	}
	
	public void AddAnimation(_AnimationTextureLink animation, CNyanimal.NyanimalAnimations animType)
	{
		m_Animations.put(animType, animation);
	}
	
	
	@Override
	public void Init()
	{
		super.Init();
	}

	@Override
	public void Update()
	{
		super.Update();
		
		if(m_Nyanimal != null)
		{
			m_bMirror = m_Nyanimal.m_bMirror;
			UpdateAnimationFrame();
		}
	}
	
	// TODO: put UpdateAnimationFrame and UpdateUV in an interface
	private void UpdateAnimationFrame()
	{
		_AnimationTextureLink curAnim = m_Animations.get(m_curAnimation);
		if(curAnim != null)
		{
			int nNewFrame = m_Nyanimal.m_Animations.get(m_Nyanimal.m_curAnimation).m_Animation.GetCurFrame();
			int nCurFrame = curAnim.GetAnimation().GetCurFrame();
			
			if(nNewFrame != nCurFrame)
			{
				curAnim.GetAnimation().SetCurFrame(nNewFrame);
				UpdateUV(curAnim, m_bMirror);
			}
		}
	}
	
	@Override
	public void HandleCollision(CNyanObject other, RectF overlap)
	{
		super.HandleCollision(other, overlap);
	}

	@Override
	public Type GetType() 
	{
		
		return Type.ACCESSORY;
	}
    
};