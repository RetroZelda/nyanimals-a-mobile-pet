package com.retrozelda.nyanimals;

import android.graphics.RectF;

public class CWindow extends CNyanObject
{
	CRectangle	m_frame;
	CRectangle	m_sky;
	
	CTransform _Transform;

	public CWindow()
	{
		_Transform  = new CTransform();
	}

	@Override
	public void Init()
	{
		m_frame = CNyanimalFactoy.RequestObject(CRectangle.class);
		m_sky = CNyanimalFactoy.RequestObject(CRectangle.class);
		
		m_frame.SetDraw(true);
		m_sky.SetDraw(true);

		m_frame.SetResourceID(R.drawable.frame);
		m_sky.SetResourceID(R.drawable.sky);

		m_frame.GetTransform().SetParent(_Transform);
		m_sky.GetTransform().SetParent(_Transform);

		/*
		// make the sky smaller
		int[] texSize = m_sky.GetTextureSize();
		m_sky.GetTransform().SetSize((int)(0.85f * texSize[0]), (int)(0.85f * texSize[1]));
		m_frame.MakeTextureSize();
		*/
	}

	@Override
	public void Destroy()
	{
		m_frame.GetTransform().SetParent(null);
		m_sky.GetTransform().SetParent(null);
		
		CNyanimalFactoy.ReturnObject(m_frame);
		CNyanimalFactoy.ReturnObject(m_sky);
	}

	@Override
	public void Update()
	{
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{		
		return false;
	}

	@Override
	public Type GetType() 
	{		
		return Type.WINDOW;
	}

	@Override
	public RectF CheckCollision(CNyanObject inner) 
	{		
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{		
		
	}

	@Override
    public void OnGraphicsInit()
    {
    }

	@Override
    public void OnEnable()
    {
    }

	@Override
    public void OnDisable()
    {
    }
}
