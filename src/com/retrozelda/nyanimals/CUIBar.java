package com.retrozelda.nyanimals;

import android.graphics.RectF;
import android.opengl.GLES20;

public class CUIBar extends CRectangle
{

	protected static final String m_FragmentShaderGradient_Tex =					
			  "precision mediump float;\n"
			+ "varying vec2 vTextureCoord;\n"
			+ "uniform vec4 vColor;\n"
			+ "uniform float fLamda;\n"
			+ "uniform sampler2D sTexture;\n"
			+ "uniform sampler2D sGradient;\n"
			+ "void main() {\n"
			+ "  vec4 vGradient = texture2D(sGradient, vTextureCoord);"
			+ "  if(vGradient.x > fLamda)"
			+ "    { discard; }" 
			+ "  gl_FragColor = texture2D(sTexture, vTextureCoord) * vColor;\n"
			+ "}\n";
	

	protected int m_LamdaHandle;
	protected int m_nGradientHandle;
	protected int _GradientID;
	protected CTexture m_Gradient;

	public int GetGradientID() { return _GradientID;}
	public void SetGradientID(int nID) { _GradientID = nID;}

	public CTexture GetGradient() { return m_Gradient;}
	public void SetGradient(CTexture tex) 
	{ 
		m_Gradient = tex;
	}
	
	int m_maxValue;
	int m_curValue;
	float m_percent;
	
	
	public CUIBar()
	{
		m_Gradient = null;
		m_LamdaHandle = -1;
		_GradientID = -1;
	}
	
	
	public void SetCurrentValue(int val)
	{
		m_curValue = val;
	}

	public void SetMaxValue(int val)
	{
		m_maxValue = val;
	}

	public int GetPercent()
	{
		return (int)(m_percent * 100.0f);
	}
	

	@Override
	protected void LoadTexture()
	{
		super.LoadTexture();
		if(_GradientID > -1)
		{
			SetGradient(CTextureManager.GetInstance().LoadTexture(_GradientID));
		}
	}

	@Override
	protected void LoadTextureShader()
	{
		// create shader program
		m_ShaderProgram = CNyanimalGLRender.createProgram(m_VertexShader, m_FragmentShaderGradient_Tex);
		if (m_ShaderProgram == 0)
		{
			return;
		}
	}

	@Override
	public void Init()
	{
		m_maxValue = 100;
	}

	@Override
    public void OpenGL_Create()
	{
		super.OpenGL_Create();

		// get lamda handle
		m_LamdaHandle = GLES20.glGetUniformLocation(m_ShaderProgram, "fLamda");
		if (m_LamdaHandle == -1)
		{
			throw new RuntimeException("Could not get attrib location for fLamda");
		}
		
		// get gradient handle
		m_nGradientHandle = GLES20.glGetUniformLocation(m_ShaderProgram, "sGradient");
		if (m_nGradientHandle == -1)
		{
			throw new RuntimeException("Could not get attrib location for sGradient");
		}
	}

	@Override
	public void Update()
	{		
		if(m_curValue > m_maxValue)
		{
			m_curValue = m_maxValue;
			m_percent = 1.0f;
		}
		else if(m_curValue < 0)
		{
			m_curValue = 0;
			m_percent = 0;
		}
		else
		{
			m_percent = (float)m_curValue / (float)m_maxValue;
		}
		super.Update();
	}

	@Override
	public void Draw()
	{				
		float[] mvp = GetTransform().GetMVPMatrix();
		
		// activate the stuff
		GLES20.glUseProgram(m_ShaderProgram);
		GLES20.glUniform1i(m_TextureHandle, 0);
		GLES20.glUniform1i(m_nGradientHandle, 1);
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);		
		if(m_Texture != null)
		{
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, m_Texture.GetTextureID());
		}
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);		
		if(m_Gradient != null)
		{
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, m_Gradient.GetTextureID());
		}

		// set the verts
		m_TriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
		GLES20.glVertexAttribPointer(m_PositionHandle, 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, m_TriangleVertices);

		// set the UVs
		m_TriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
		GLES20.glEnableVertexAttribArray(m_PositionHandle);

		// set the texture
		GLES20.glVertexAttribPointer(m_UVHandle, 2, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, m_TriangleVertices);
		GLES20.glEnableVertexAttribArray(m_UVHandle);
		
		// set the MVP
		GLES20.glUniformMatrix4fv(m_MVPMatrixHandle, 1, false, mvp, 0);

		// set the color
		GLES20.glUniform4f(m_ColorHandle, m_Red, m_Green, m_Blue, m_Alpha );
		
		// set the lamda
		GLES20.glUniform1f(m_LamdaHandle, m_percent);
		
		// Draw
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{
		
		return false;
	}

	@Override
	public Type GetType() 
	{
		
		return Type.UIBAR;
	}

	@Override
	public RectF CheckCollision(CNyanObject inner) {
		
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap) {
		
		
	}

	@Override
	public void Destroy() {
		
		
	}

	// NOTE: posX and posY are 0 -> 1 to be on screen
	public void SetBarInformation(int Texture, int Gradient, int posX, int posY, int width, int height, int maxValue, int curValue)
	{
    }
}
