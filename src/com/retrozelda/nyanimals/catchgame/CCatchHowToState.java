package com.retrozelda.nyanimals.catchgame;

import android.graphics.RectF;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.nyanimals.CButton;
import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimalFactoy;
import com.retrozelda.nyanimals.CRectangle;
import com.retrozelda.nyanimals.IButtonAction;
import com.retrozelda.nyanimals.R;
import com.retrozelda.nyanimals.catchgame.CCatchObject.FallObjectInfo;
import com.retrozelda.nyanimals.catchgame.CCatchStartState.HowToTouch;
import com.retrozelda.utilities.Utilities;

public class CCatchHowToState extends CNyanState
{
	class BackTouch implements IButtonAction
	{
		@Override
		public void HandlePress() 
		{
			
		}

		@Override
		public void HandleRelease() 
		{
			FSMLibrary.Chapter(CCatchFSMConstructor.CHAPTERHASH).ChangeState(CCatchStartState.class, _Nyanimal, _Catcher);
		}
	}
	
	private CButton _BackButton;
	private CNyanimal _Nyanimal;
	private CCatcher _Catcher;

	private CRectangle _Apple;
	private CRectangle _Grape;
	private CRectangle _Cherry;
	private CRectangle _Cupcake;
	private CRectangle _Cake;

	@Override
    public void OnCreate()
    {
		_BackButton = CNyanimalFactoy.RequestObject(CButton.class);
		_Apple = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Grape = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Cherry = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Cupcake = CNyanimalFactoy.RequestObject(CRectangle.class);
		_Cake = CNyanimalFactoy.RequestObject(CRectangle.class);
				
		_Apple.SetResourceID(R.drawable.apple);
		_Grape.SetResourceID(R.drawable.grapes);
		_Cherry.SetResourceID(R.drawable.cherry);
		_Cupcake.SetResourceID(R.drawable.cupcake);
		_Cake.SetResourceID(R.drawable.cake);

		_BackButton.SetButtonInformation(0, 0, 0, 0, R.drawable.brusharrow, R.drawable.brusharrow);
		_BackButton.SetAction(new BackTouch());
    }
	
	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
		_Catcher = (CCatcher)_PassthroughObjects[1];	
	}

	@Override
    public void OnDestroy()
    {
		_BackButton.SetAction(null);
		CNyanimalFactoy.ReturnObject(_BackButton);
		CNyanimalFactoy.ReturnObject(_Apple);
		CNyanimalFactoy.ReturnObject(_Grape);
		CNyanimalFactoy.ReturnObject(_Cherry);
		CNyanimalFactoy.ReturnObject(_Cupcake);
		CNyanimalFactoy.ReturnObject(_Cake);
    }
	
	@Override
	public void OnEnter()
	{
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nScreenHeight = Utilities.GLSurfaceView.getHeight();
		float fOffsetX = nScreenWidth / 16.0f;
		float fOffsetY = nScreenHeight / 16.0f;
		float fScale0 = 0.15f;
		float fScale1 = 0.20f;
		float fScale2 = 0.30f;
		float[] pos = new float[2];
		pos[0] = (float)nScreenWidth / 8.0f;
		pos[1] = nScreenHeight / 4.0f;
		
		_Apple.MakeTextureSize();
		_Apple.GetTransform().Scale(fScale0, fScale0);
		pos[0] = (nScreenWidth / 2.0f) - (_Apple.GetTransform().GetSize()[0] * 2);
		_Apple.MakeTextureSize();
		_Apple.GetTransform().Scale(fScale0 - 0.03f, fScale0 - 0.03f);
		_Apple.GetTransform().SetPosition(pos[0], pos[1]);
		
		_Grape.MakeTextureSize();
		_Grape.GetTransform().Scale(fScale0, fScale0);
		pos[0] = (nScreenWidth / 2.0f);
		_Grape.GetTransform().SetPosition(pos[0], pos[1]);
		
		_Cherry.MakeTextureSize();
		_Cherry.GetTransform().Scale(fScale0, fScale0);
		pos[0] = (nScreenWidth / 2.0f) + (_Cherry.GetTransform().GetSize()[0] * 2);
		_Cherry.GetTransform().SetPosition(pos[0], pos[1]);
		
		_Cupcake.MakeTextureSize();
		_Cupcake.GetTransform().Scale(fScale1, fScale1);
		pos[0] = nScreenWidth / 2.0f;
		pos[1] += _Cherry.GetTransform().GetSize()[1] + fOffsetX;
		_Cupcake.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += _Cupcake.GetTransform().GetSize()[1] + fOffsetX;
		
		_Cake.MakeTextureSize();
		_Cake.GetTransform().Scale(fScale2, fScale2);
		pos[0] = nScreenWidth / 2.0f;
		_Cake.GetTransform().SetPosition(pos[0], pos[1]);
		pos[1] += _Cake.GetTransform().GetSize()[1] + fOffsetX;

		
		_BackButton.MakeScaledSize();
		float[] nArrowSize = _BackButton.GetTransform().GetSize();
		_BackButton.GetTransform().SetPosition(nScreenWidth - (nArrowSize[0] * 2.0f), nScreenHeight - (nArrowSize[1]));
	}

	@Override
    public void OnPause()
    {
		_BackButton.SetDraw(false);
		_BackButton.SetActive(false);
		
		_Apple.SetActive(false);
		_Apple.SetDraw(false);
		
		_Grape.SetActive(false);
		_Grape.SetDraw(false);
		
		_Cherry.SetActive(false);
		_Cherry.SetDraw(false);
		
		_Cupcake.SetActive(false);
		_Cupcake.SetDraw(false);
		
		_Cake.SetActive(false);
		_Cake.SetDraw(false);
    }

	@Override
    public void OnResume()
    {			
		_BackButton.SetDraw(true);
		_BackButton.SetActive(true);
		
		_Apple.SetActive(true);
		_Apple.SetDraw(true);
		
		_Grape.SetActive(true);
		_Grape.SetDraw(true);
		
		_Cherry.SetActive(true);
		_Cherry.SetDraw(true);
		
		_Cupcake.SetActive(true);
		_Cupcake.SetDraw(true);
		
		_Cake.SetActive(true);
		_Cake.SetDraw(true);
    }


	@Override
    public void PriorityUpdate()
    {
    }

	@Override
    public void Update()
    {
		float[] pos = _BackButton.GetTransform().getPosition();
		Utilities.GLRenderer.m_Text.DrawText("Go Back", pos[0], pos[1]);
    }

	@Override
    public void LateUpdate()
    {
    }

	@Override
    public RectF CheckCollision(CNyanObject inner)
    {
	    return null;
    }

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

}