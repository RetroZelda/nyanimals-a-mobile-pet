package com.retrozelda.nyanimals.nyanimal;

import android.graphics.RectF;

import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CNyanState;
import com.retrozelda.nyanimals.CNyanimal;
import com.retrozelda.nyanimals.CNyanimal.Direction;
import com.retrozelda.nyanimals.CNyanimal.NyanimalAnimations;
import com.retrozelda.nyanimals.catchgame.CCatcher;
import com.retrozelda.utilities.Utilities;

public class CNyanimalWalkingState extends CNyanState
{
	CNyanimal _Nyanimal;
	float _walkSpeed;
	float[] _destinationPoint;
	
	@Override
	public void OnCreate()
	{
		_destinationPoint = new float[2];
		_walkSpeed = 1.0f;	
	}

	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_Nyanimal = (CNyanimal)_PassthroughObjects[0];
	}
	
	@Override
	public void OnEnter()
	{
		// keep object creating outside of loops	
		float[] nSize = _Nyanimal.GetTransform().GetSize();
		float minX = nSize[0];
		float maxX = Utilities.Screen.GetWidth() - minX;
		float minY = (int)(Utilities.Screen.GetHeight() * 0.70f) + nSize[1];
		float maxY = (int)(Utilities.Screen.GetHeight() * 0.85f) - nSize[1];
		float[] projPos = _Nyanimal.GetTransform().getPosition();

		// get a new destination
		_destinationPoint[0] = Utilities.Random.nextInt((int) (maxX - minX)) + minX;
		_destinationPoint[1] = Utilities.Random.nextInt((int) (maxY - minY)) + minY;
		
		// Log.d(TAG, "New Destination: (" + m_destinationPoint[0] + ", " + m_destinationPoint[1] + ") | From: (" + projPos[0] + ", " + projPos[1] + ")");
		
		// determine a direction
		float nDeltaX = _destinationPoint[0] - projPos[0];
		float nDeltaY = _destinationPoint[1] - projPos[1];
		
		// determine if we need to be mirrored
		if(nDeltaX > 0.0f)
		{
			_Nyanimal.SetMirrored(true);
		}
		else if(nDeltaX < 0.0f)
		{
			_Nyanimal.SetMirrored(false);
		}
		
		Direction newDirection = _Nyanimal.GetCurDirection();
		if(((int)nDeltaX & 1 << 31) > ((int)nDeltaY & 1 << 31))
		{
			// left or right
			if(_Nyanimal.IsMirrored() == true)
			{
				newDirection = Direction.RIGHT;
			}
			else
			{
				newDirection = Direction.LEFT;
			}
		}
		else
		{
			// up or down
			if(nDeltaY > 0)
			{
				newDirection = Direction.DOWN;
			}
			else
			{
				newDirection = Direction.UP;
			}
		}
		
		_Nyanimal.SetDirection(newDirection);
		
		// determine the animation
		switch(newDirection)
		{
		case UP:
			_Nyanimal.SetAnimation(NyanimalAnimations.WALKING_UP);
			break;
		case DOWN:
			_Nyanimal.SetAnimation(NyanimalAnimations.WALKING_DOWN);
			break;
		case LEFT:
			_Nyanimal.SetAnimation(NyanimalAnimations.WALKING_LEFT);
			break;
		case RIGHT:
			_Nyanimal.SetAnimation(NyanimalAnimations.WALKING_RIGHT);
			break;
		};
	}

	@Override
	public void PriorityUpdate()
	{
	}

	@Override
	public void Update()
	{
		float[] curPos = _Nyanimal.GetTransform().getPosition();
		float[] fDirection = new float[2];
		
		// get the direction
		float fDist = (float)Math.sqrt((float)((_destinationPoint[0] - curPos[0]) * (_destinationPoint[0] - curPos[0])) + (float)((_destinationPoint[1] - curPos[1]) * (_destinationPoint[1] - curPos[1])));
		fDirection[0] = (_destinationPoint[0] - (float)curPos[0]) / fDist;
		fDirection[1] = (_destinationPoint[1] - (float)curPos[1]) / fDist;
		//Log.d(TAG, "Destination: (" + m_destinationPoint[0] + ", " + m_destinationPoint[1] + ") | From: (" + curPos[0] + ", " + curPos[1] + ")");
		//Log.d(TAG, "Direction: (" + fDirection[0] + ", " + fDirection[1] + ")");
		//Log.d(TAG, "Dist: (" + fDist + ")"); 

		// move
		float[] nSize = _Nyanimal.GetTransform().GetSize();
		curPos[0] += (int)(fDirection[0] * (nSize[0] / _walkSpeed) * Utilities.Time.DeltaTime_Seconds());
		curPos[1] += (int)(fDirection[1] * (nSize[1] / _walkSpeed) * Utilities.Time.DeltaTime_Seconds());
		_Nyanimal.GetTransform().SetPosition(curPos[0], curPos[1]);
		
		// determine if we need to stop
		if(fDist <= 10.0f)
		{
			_Nyanimal.DeterminNextAction();
		}
	}

	@Override
	public void LateUpdate()
	{
	}

	@Override
	public void OnDestroy()
	{
	}

	@Override
	public RectF CheckCollision(CNyanObject inner)
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
	{
		return false;
	}

}
