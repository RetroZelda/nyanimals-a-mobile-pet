package com.retrozelda.nyanimals.catchgame;

import android.graphics.RectF;

import com.retrozelda.nyanimals.CNyanObject;
import com.retrozelda.nyanimals.CRectangle;
import com.retrozelda.nyanimals.NyanimalPages;
import com.retrozelda.nyanimals.R;
import com.retrozelda.nyanimals.CNyanObject.Type;
import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

public class CCatcher extends CRectangle
{

	@Override
    public void Init()
    {
		SetResourceID(R.drawable.basket);
    }

	@Override
    public void OnGraphicsInit()
    {
		MakeTextureSize();

		float[] nSize = GetTransform().GetSize();
		float fCanAspect = nSize[0] / nSize[1];
		
		int nDivisor = 12;
		int nNewWidth = Utilities.Screen.GetWidth() / nDivisor;
		GetTransform().SetSize(nNewWidth, (int) (nNewWidth * fCanAspect));
    }
	
	@Override
    public void Destroy()
    {
    }

	@Override
    public void OnEnable()
    {
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Subscribe(NyanimalPages.AndroidMotionEvent.class, this);
    }

	@Override
    public void OnDisable()
    {
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Unsubscribe(NyanimalPages.AndroidMotionEvent.class, this);
    }
	
	
	@Override
	public void Update()
	{
	}

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
		if(inner.GetType() == Type.CATCHOBJECT)
		{
			StoryBook.Chapter(NyanimalPages.Chapters.CatchGame).Mark(new NyanimalPages.CatchGame_ObjectCaught(inner));
		}
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

	@Override
    public Type GetType()
    {
	    return Type.CATCHER;
    }
}
