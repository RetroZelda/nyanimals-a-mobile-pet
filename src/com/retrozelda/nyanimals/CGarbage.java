package com.retrozelda.nyanimals;

import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;


public class CGarbage extends CRectangle
{
	
	@Override
	public void Init()
	{
		super.Init();
		
		SetResourceID(R.drawable.trashcan);
	}

	@Override
    public void OnGraphicsInit()
    {
		MakeTextureSize();

		float[] nSize = GetTransform().GetSize();
		float fCanAspect = (float)nSize[0] / (float)nSize[1];
		
		int nDivisor = 12;
		int nNewWidth = Utilities.Screen.GetWidth() / nDivisor;
		GetTransform().SetSize(nNewWidth, (int) (nNewWidth * fCanAspect));
		
		nSize = GetTransform().GetSize();
		GetTransform().SetPosition(Utilities.Screen.GetWidth() - (nSize[0]), Utilities.Screen.GetHeight() - (nSize[1] * 4));
    }
	
	@Override
	public void Update()
	{
	}
	
	@Override
	public void Draw()
	{
		super.Draw();
	}
	
	@Override
	public void HandleCollision(CNyanObject other, RectF overlap)
	{
		super.HandleCollision(other, overlap);
		
		switch(other.GetType())
		{
		case MEDICINE:
		case FOOD:
			CFood food = (CFood)other;
			if(food.IsDragging() == false)
			{
				// delete food
				food.SetObjectStatus(ObjStatus.TO_DELETE);
			}
			break;
		default:
			break;
		};
		
	}

	@Override
	public Type GetType() 
	{
		
		return Type.NYANIMAL;
	}
}
