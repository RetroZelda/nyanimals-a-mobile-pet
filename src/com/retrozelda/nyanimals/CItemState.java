package com.retrozelda.nyanimals;

import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;

public class CItemState extends CNyanState
{
	CBackdrop m_Backdrop;
	CButton m_BackButton;
	
	
	@Override
	public RectF CheckCollision(CNyanObject inner) 
	{
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap)
	{

	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{
		m_BackButton.HandleTouch(nPosX, nPosY, nTouchAction);
		return true;
	}

	@Override
    public void OnCreate()
    {
		m_Backdrop = CNyanimalFactoy.RequestObject(CBackdrop.class);
		m_Backdrop.SetBackdropInformation(R.drawable.itemsbg, R.drawable.item_titlepiece);
		m_Backdrop.SetUVScale(2.0f, 2.0f / Utilities.Screen.GetAspectRatio());
		m_Backdrop.SetScroll(-0.05f, -0.05f);

		m_BackButton = CNyanimalFactoy.RequestObject(CButton.class);
		m_BackButton.SetButtonInformation(0, 0, 0, 0, R.drawable.itemsarrow, R.drawable.itemsarrow);
		m_BackButton.SetAction(new BackTouch());
		
		m_Backdrop.SetDraw(false);
		m_BackButton.SetDraw(false);

		m_BackButton.SetActive(false);
    }
	
	@Override
	public void OnEnter()
	{
		// screen info
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nScreenHeight = Utilities.GLSurfaceView.getHeight();
		
		// calculate size and position
		m_BackButton.GetUpRect().MakeTextureSizeLocal();
		float[] nArrowSize = m_BackButton.GetUpRect().GetTransform().GetLocalSize();
		float fArrowAspect = (float)nArrowSize[0] / (float)nArrowSize[1];
		nArrowSize[0] = (int)(nScreenWidth * (nArrowSize[0] / 1080.0f)) / 2;
		nArrowSize[1] = (int) (nArrowSize[0] / fArrowAspect);
		
		// set the rect transforms to default size
		m_BackButton.GetUpRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		m_BackButton.GetPressedRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		
		// set button size and position
		m_BackButton.GetTransform().SetSize(nArrowSize[0], nArrowSize[1]);
		m_BackButton.GetTransform().SetPosition(nScreenWidth - (nArrowSize[0]), nScreenHeight - (nArrowSize[1]));
	}

	@Override
    public void OnPause()
    {
		m_Backdrop.SetDraw(false);
		m_BackButton.SetDraw(false);

		m_Backdrop.SetActive(false);
		m_BackButton.SetActive(false);
    }

	@Override
    public void OnResume()
    {		
		m_Backdrop.SetDraw(true);
		m_BackButton.SetDraw(true);

		m_Backdrop.SetActive(true);
		m_BackButton.SetActive(true);		
    }
	
	@Override
    public void PriorityUpdate()
    {
	    
    }

	@Override
    public void Update()
    {
    }


	@Override
    public void LateUpdate()
    {
	    
    }

	@Override
    public void OnDestroy()
    {
		m_BackButton.SetAction(null);
		CNyanimalFactoy.ReturnObject(m_Backdrop);
		CNyanimalFactoy.ReturnObject(m_BackButton);
    }
}
