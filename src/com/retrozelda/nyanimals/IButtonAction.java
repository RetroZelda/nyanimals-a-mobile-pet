package com.retrozelda.nyanimals;

public interface IButtonAction
{
	public void HandlePress();
	public void HandleRelease();
}