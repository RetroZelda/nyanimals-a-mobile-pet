package com.retrozelda.nyanimals;

import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.IReader;
import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;

public abstract class CNyanObject implements IReader
{
	public enum Type { RECT, NYANIMAL, FOOD, ROOM, PROGRESSBAR, WINDOW, BUTTON, MISC, ACCESSORY, UIBAR, MEDICINE, BACKDROP, INVENTORYSLOT,
						CATCHER, CATCHOBJECT};
	public enum ObjStatus {UNUSED, PENDING_OPENGL_CREATE, ACTIVE, PENDING_OPENGL_DESTROY, TO_DELETE, DESTROYED};
	
	ObjStatus m_ObjectStatus = ObjStatus.UNUSED;
	int m_nDrawLayer = 0;
	
	private boolean m_bActive = false;
	
	
	boolean IsActive() { return m_bActive;}	
	public void SetActive(boolean bActive)
	{
		if(m_bActive != bActive)
		{
			CNyanimalFactoy.SetActive(this, bActive);
			m_bActive = bActive;
		}
	}
	
	
	public ObjStatus GetObjectStatus() { return m_ObjectStatus;}	
	void SetObjectStatus(ObjStatus status) { m_ObjectStatus = status;}

	
	@Override
	public void ReadPage(IPage page)
	{
		// TODO: Need to add OnActive and OffActive functions to handle event registering there
		if(!IsActive())
		{
			return;
		}
		if(page instanceof NyanimalPages.ScreenTouchEvent)
		{
			NyanimalPages.ScreenTouchEvent e = (NyanimalPages.ScreenTouchEvent)page;
			HandleTouch(e.X(), e.Y(), e.Action());
		}
		else
		{
			Utilities.Log.Warning("Nyanimal Object \"", this.getClass().getName() + "\" recieved an unhandled Storybook Page: " + page.getClass().getName());
		}
	}

	public abstract void Init();
	public abstract void Destroy();
	
	// TODO: Make abstract.  maybe
	public abstract void OnEnable();
	public abstract void OnDisable();
	
	// TODO: make part of a render component
	public abstract void OnGraphicsInit();
	
	public void PriorityUpdate() {}
	public void Update() {}
	public void LateUpdate() {}
	
	// TODO: No collision if inactive
	public abstract RectF CheckCollision(CNyanObject inner);
	public abstract void HandleCollision(CNyanObject inner, RectF overlap);
	public abstract boolean HandleTouch(int nPosX, int nPosY, int nTouchAction);		
	public abstract Type GetType();
}
