package com.retrozelda.nyanimals;

import com.retrozelda.nyanimals.CProgressBar.BarDirection;
import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;

public class CStatusScreenState extends CNyanState
{

	CBackdrop m_Backdrop;
	CRectangle m_HeaderText; // "Status Screen"
	
	CProgressBar m_AttackBar;
	CProgressBar m_RegenBar;
	CProgressBar m_DefenseBar;
	CProgressBar m_AffectionBar;
	
	CButton m_BackButton;
	
	CNyanimal					m_NyanimalToWatch;
	
	public CStatusScreenState()
	{
		super();
		
		m_NyanimalToWatch = null;
	}

	@Override
	public RectF CheckCollision(CNyanObject inner)
	{
		
		return null;
	}

	@Override
	public void HandleCollision(CNyanObject inner, RectF overlap) 
	{
		
		
	}

	@Override
	public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction) 
	{
		m_BackButton.HandleTouch(nPosX, nPosY, nTouchAction);
		return true;
	}

	@Override
    public void OnCreate()
    {
		// TODO: PRIORITY: Get nyanimal through storybook
		//m_NyanimalToWatch = nyanimal;

		m_Backdrop = CNyanimalFactoy.RequestObject(CBackdrop.class);
		m_HeaderText = CNyanimalFactoy.RequestObject(CRectangle.class);	
		m_AttackBar = CNyanimalFactoy.RequestObject(CProgressBar.class);
		m_RegenBar = CNyanimalFactoy.RequestObject(CProgressBar.class);
		m_DefenseBar = CNyanimalFactoy.RequestObject(CProgressBar.class);
		m_AffectionBar = CNyanimalFactoy.RequestObject(CProgressBar.class);
		m_BackButton = CNyanimalFactoy.RequestObject(CButton.class);
		
		m_Backdrop.SetBackdropInformation(R.drawable.infobg, R.drawable.info_titlepiece);
		m_Backdrop.SetUVScale(2.0f, 2.0f / Utilities.Screen.GetAspectRatio());
		m_Backdrop.SetScroll(-0.05f, -0.05f);
		
		m_HeaderText.SetResourceID(R.drawable.status_screen_header);
		m_HeaderText.SetDrawLayer(Utilities.CommonLayers.HudMisc);
		m_BackButton.SetButtonInformation(0, 0, 0, 0, R.drawable.infoarrow, R.drawable.infoarrow);
		m_BackButton.SetAction(new BackTouch());

		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nScreenHeight = Utilities.GLSurfaceView.getHeight();
		float fWidthPercent = 0.40f;
		float fHeightPercent = 0.05f;
		int nWidthScaled = (int)(nScreenWidth * fWidthPercent);
		int nHeightScaled = (int)(nScreenHeight * fHeightPercent);
		int nScreenMiddle = Utilities.GLSurfaceView.getWidth() / 2;
		
		m_RegenBar.SetBarInformation(R.drawable.regen_frames, R.drawable.regen_gradient, nScreenMiddle , (int) (nScreenHeight * 0.4f), nWidthScaled, nHeightScaled, 100, 50);
		m_AttackBar.SetBarInformation(R.drawable.attack_frames, R.drawable.attack_gradient, nScreenMiddle , (int) (nScreenHeight * 0.3f), nWidthScaled, nHeightScaled, 100, 75);
		m_DefenseBar.SetBarInformation(R.drawable.defence_frames, R.drawable.defence_gradient, nScreenMiddle , (int) (nScreenHeight * 0.5f), nWidthScaled, nHeightScaled, 100, 50);
		m_AffectionBar.SetBarInformation(R.drawable.affection_frames, R.drawable.affection_gradient, nScreenMiddle , (int) (nScreenHeight * 0.75f), nWidthScaled, nHeightScaled, 100, 25);
		
		m_RegenBar.SetFillDirection(BarDirection.FillRight);
		m_AttackBar.SetFillDirection(BarDirection.FillRight);
		m_DefenseBar.SetFillDirection(BarDirection.FillRight);
		m_AffectionBar.SetFillDirection(BarDirection.FillRight);

		m_Backdrop.SetDraw(false);
		m_BackButton.SetDraw(false);
		m_RegenBar.SetDraw(false);
		m_AttackBar.SetDraw(false);
		m_DefenseBar.SetDraw(false);
		m_AffectionBar.SetDraw(false);
		

		m_BackButton.SetActive(false);
		m_RegenBar.SetActive(false);
		m_AttackBar.SetActive(false);
		m_DefenseBar.SetActive(false);
		m_AffectionBar.SetActive(false);
		
		Utilities.Log.Debug("CStatusScreenState", "Init Progress bars");
    }
	
	@Override
	public void OnEnter()
	{
		int nScreenWidth = Utilities.GLSurfaceView.getWidth();
		int nScreenHeight = Utilities.GLSurfaceView.getHeight();
		// float fWidthPercent = 0.40f;
		// float fHeightPercent = 0.05f;
		// int nWidthScaled = (int)(nScreenWidth * fWidthPercent);
		// int nHeightScaled = (int)(nScreenHeight * fHeightPercent);
		int nScreenMiddle = Utilities.GLSurfaceView.getWidth() / 2;
		
		m_HeaderText.MakeTextureSize();		
		float[] nHeaderSize = m_HeaderText.GetTransform().GetSize();
		float fHeaderAspect = (float)nHeaderSize[0] / (float)nHeaderSize[1];
		int nNewWidth = nScreenWidth / 4;
		m_HeaderText.GetTransform().SetSize(nNewWidth, (int) (nNewWidth / fHeaderAspect));
		m_HeaderText.GetTransform().SetPosition(nScreenMiddle, Utilities.GLSurfaceView.getHeight() / 6);
		
		m_BackButton.GetUpRect().MakeTextureSizeLocal();
		float[] nArrowSize = m_BackButton.GetUpRect().GetTransform().GetLocalSize();
		float fArrowAspect = (float)nArrowSize[0] / (float)nArrowSize[1];
		nArrowSize[0] = (int)(nScreenWidth * (nArrowSize[0] / 1080.0f)) / 2;
		nArrowSize[1] = (int) (nArrowSize[0] / fArrowAspect);
		m_BackButton.GetUpRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		m_BackButton.GetPressedRect().GetTransform().SetLocalSize(1.0f, 1.0f);
		m_BackButton.GetTransform().SetSize(nArrowSize[0], nArrowSize[1]);
		m_BackButton.GetTransform().SetPosition(nScreenWidth - (nArrowSize[0]), nScreenHeight - (nArrowSize[1]));
		
	}
	
	@Override
    public void OnPause()
    {
		m_HeaderText.SetDraw(false);
		m_Backdrop.SetDraw(false);
		m_BackButton.SetDraw(false);
		m_RegenBar.SetDraw(false);
		m_AttackBar.SetDraw(false);
		m_DefenseBar.SetDraw(false);
		m_AffectionBar.SetDraw(false);

		m_Backdrop.SetActive(false);
		m_BackButton.SetActive(false);
		m_RegenBar.SetActive(false);
		m_AttackBar.SetActive(false);
		m_DefenseBar.SetActive(false);
		m_AffectionBar.SetActive(false);
    }

	@Override
    public void OnResume()
    {
		m_HeaderText.SetDraw(true);
		m_Backdrop.SetDraw(true);
		m_BackButton.SetDraw(true);
		m_RegenBar.SetDraw(true);
		m_AttackBar.SetDraw(true);
		m_DefenseBar.SetDraw(true);
		m_AffectionBar.SetDraw(true);

		m_Backdrop.SetActive(true);
		m_BackButton.SetActive(true);
		m_RegenBar.SetActive(true);
		m_AttackBar.SetActive(true);
		m_DefenseBar.SetActive(true);
		m_AffectionBar.SetActive(true);
    }

	
	@Override
    public void PriorityUpdate()
    {
    }

	@Override
    public void Update()
    {
    }

	@Override
    public void LateUpdate()
    {
    }


	@Override
    public void OnDestroy()
    {
		m_BackButton.SetAction(null);
		CNyanimalFactoy.ReturnObject(m_Backdrop);
		CNyanimalFactoy.ReturnObject(m_HeaderText);
		CNyanimalFactoy.ReturnObject(m_AttackBar);
		CNyanimalFactoy.ReturnObject(m_RegenBar);
		CNyanimalFactoy.ReturnObject(m_DefenseBar);
		CNyanimalFactoy.ReturnObject(m_AffectionBar);
		CNyanimalFactoy.ReturnObject(m_BackButton);
    }
}
