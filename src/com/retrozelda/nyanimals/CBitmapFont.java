package com.retrozelda.nyanimals;

import java.util.LinkedList;

import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class CBitmapFont extends CRectangle
{
	private class CText
	{
		public String	text;
		public float	posX;
		public float	posY;

		public CText(String szText, float pos, float pos2)
		{
			this.text = szText;
			this.posX = pos;
			this.posY = pos2;
		}
	};

	int					m_charWidth;
	int					m_charHeight;
	int					m_startingChar;

	RectF[]				m_CharMap;
	LinkedList<CText>	m_DrawText;

	public void SetFontInfo(char startingChar, int charWidth, int charHeight, int resourceID )
	{
		m_startingChar = startingChar;
		m_charWidth = charWidth;
		m_charHeight = charHeight;
		SetResourceID(resourceID);
		SetFontSize(m_charWidth, m_charHeight);
	}
	
	public void SetFontSize(float fSizeX, float fSizeY)
	{
		// determine the screen scale
		float fScreenScale = fSizeY / 1080.0f; // based on Nexus 5 screen width of 1080 and frame size of 32
		float fCharRatio = fSizeX / fSizeY;
		float nCharWidth = Utilities.Screen.GetWidth() * fScreenScale;
		float nCharHeight = nCharWidth * fCharRatio;
		GetTransform().SetSize(nCharWidth * 2.5f, nCharHeight * 2.5f);

		//GetTransform().SetSize(fSizeX, fSizeY);
	}
	
	@Override
	public void Init()
	{
		super.Init();		

		SetDrawLayer(Utilities.CommonLayers.Text);
	}

	@Override
    public void OnGraphicsInit()
    {
		// create the rect array
		int numWidth = m_Texture.GetWidth() / m_charWidth;
		int numHeight = (m_Texture.GetHeight() / m_charHeight);
		int numBlock = numWidth * numHeight;
		m_CharMap = new RectF[numBlock];
		m_DrawText = new LinkedList<CBitmapFont.CText>();
		
		// fill it
		for(int charIndex = 0; charIndex < numBlock; ++charIndex)
		{
			int col = charIndex % numWidth;
			int row = charIndex / numWidth;

			// build the rect
			RectF charRect = new RectF();
			charRect.top = row * m_charHeight;
			charRect.bottom = charRect.top + m_charHeight;
			charRect.right = col * m_charWidth;
			charRect.left = charRect.right + m_charWidth;

			// get into texture space
			charRect.top /= m_Texture.GetHeight();
			charRect.bottom /= m_Texture.GetHeight();
			charRect.left /=  m_Texture.GetWidth();
			charRect.right /=  m_Texture.GetWidth();

			// insert it
			m_CharMap[charIndex] = charRect;
		}
    }

	@Override
	public void Update()
	{

	}

	public void DrawText(String szText, float pos, float pos2)
	{
		if(m_DrawText != null)
		{
			CText text = new CText(szText, pos, pos2);
			m_DrawText.addLast(text);
		}
	}

	@Override
	public void Draw()
	{
		// activate the stuff
		GLES20.glUseProgram(m_ShaderProgram);
		
		GLES20.glUniform1i(m_TextureHandle, 0);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, m_Texture.GetTextureID());

		// set the texture
		GLES20.glVertexAttribPointer(m_UVHandle, 2, GLES20.GL_FLOAT,
				false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, m_TriangleVertices);
		GLES20.glEnableVertexAttribArray(m_UVHandle);

		// set the verts
		m_TriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
		GLES20.glVertexAttribPointer(m_PositionHandle, 3, GLES20.GL_FLOAT,
				false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, m_TriangleVertices);
		
		// set teh color
		GLES20.glUniform4f(m_ColorHandle, m_Red, m_Green, m_Blue, m_Alpha );
		

		int textSize = m_DrawText.size();
		float[] scale = GetTransform().GetSize();
		float[] model = new float[16];
		float[] mvp = new float[16];
		for(int textIndex = 0; textIndex < textSize; ++textIndex)
		{
			CText text = m_DrawText.get(textIndex);
			float posX = text.posX + (scale[0] / 2.0f);
			float posY = text.posY + (scale[1] / 2.0f);

			int stringLength = text.text.length();
			for(int stringIndex = 0; stringIndex < stringLength; ++stringIndex)
			{
				char curChar = text.text.charAt(stringIndex);

				// ensure valid indicies
				if(curChar - m_startingChar >= 0)
				{
					FocusChar(curChar);

					// freshen the model matrix
					Matrix.setIdentityM(model, 0);

					// set the position
					Matrix.translateM(model, 0, posX, posY, 0.0f);

					// set any rotations

					// set any scales
					Matrix.scaleM(model, 0, scale[0], scale[1], 1.0f);
					// //////////////////////////////////////

					// calculate the MVP
					Matrix.multiplyMM(mvp, 0, Utilities.GameClass.GetViewProjection(), 0, model, 0);

					// set the UVs
					m_TriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
					GLES20.glEnableVertexAttribArray(m_PositionHandle);

					// set the MVP
					GLES20.glUniformMatrix4fv(m_MVPMatrixHandle, 1, false, mvp, 0);

					// Draw the char
					GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
				}
				posX += (scale[0]);
				
				// stop if offscreen
				if(posX >= Utilities.Screen.GetWidth())
				{
					break;
				}
			}
		}
		m_DrawText.clear(); 
	}

	// NOTE: this is here to keep the garbage collector at bay
	RectF m_focusCharRect;
	private void FocusChar(int charIndex)
	{
		m_focusCharRect = m_CharMap[charIndex - m_startingChar];
		SetUVs(m_focusCharRect);
	}
}
