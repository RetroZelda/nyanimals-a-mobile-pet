package com.retrozelda.nyanimals;
import com.retrozelda.nyanimals.Animations.CAnimation;

public class _AnimationTextureLink
{
	CAnimation m_Animation;
	CTexture m_Texture;
	
	public CAnimation GetAnimation() { return m_Animation;}
	public CTexture GetTexture() { return m_Texture;}
	
	public void SetAnimation(CAnimation anim) { m_Animation = anim;}
	public void SetTexture(CTexture tex) {m_Texture = tex;}

	public _AnimationTextureLink(CAnimation anim, CTexture tex)
	{
		m_Animation = anim;
		m_Texture = tex;
	}
	public _AnimationTextureLink()
	{
		m_Animation = null;
		m_Texture = null;
	}
	public _AnimationTextureLink(String szAnimName, int nTextureResource, int nSizeX, int nSizeY, int nNumCol, int nNumRow, float fAnimTime, boolean bLoop)
	{
		Create(szAnimName, nTextureResource, nSizeX, nSizeY, nNumCol, nNumRow, fAnimTime, bLoop);
	}
	
	public void Create(String szAnimName, int nTextureResource, int nSizeX, int nSizeY, int nNumCol, int nNumRow, float fAnimTime, boolean bLoop)
	{
		m_Animation = new CAnimation(szAnimName, bLoop);
		m_Texture = CTextureManager.GetInstance().LoadTexture(nTextureResource);
		for(int x = 0; x < nNumRow; ++x)
		{
			for(int y = 0; y < nNumCol; ++y)
			{
				m_Animation.AddFrame(y * nSizeX, x * nSizeY, nSizeX, nSizeY, fAnimTime);
			}
		}
	}
}