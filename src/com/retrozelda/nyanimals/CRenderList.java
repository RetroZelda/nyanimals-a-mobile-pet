package com.retrozelda.nyanimals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

public class CRenderList
{

	private static class RenderSorting implements Comparator<IRenderObject>
	{
		@Override
		public int compare(IRenderObject left, IRenderObject right)
		{
			// sort by standard layer
			if( left.GetDrawLayer() > right.GetDrawLayer())
			{
				return 1;
			}
			else if( left.GetDrawLayer() < right.GetDrawLayer())
			{
				return -1;
			}
			else
			{
				// sort by the set priority(generally its the Y position)
				if( left.DrawPriority() > right.DrawPriority())
				{
					return 1;
				}
				else if( left.DrawPriority() < right.DrawPriority())
				{
					return -1;
				}
				else
				{
					return 0;
				}
			}
			
		}
	}
	
	private ArrayList<IRenderObject> _CreationPending;
	private ArrayList<IRenderObject> _DestructionPending;
	private LinkedList<IRenderObject> _RenderList;
	private LinkedList<IRenderObject> _OfflineRenderList;
	
	private boolean _bPendingObjectLock;
	
	private static CRenderList Instance = new CRenderList();
	private CRenderList()
	{
		_CreationPending = new ArrayList<IRenderObject>();
		_DestructionPending = new ArrayList<IRenderObject>();
		_RenderList = new LinkedList<IRenderObject>();
		_OfflineRenderList = new LinkedList<IRenderObject>();
		_bPendingObjectLock = false;
	}

	private void SetToDraw_Internal(IRenderObject obj, boolean bDraw)
    {
		// if not pulled, then probably hasnt gone through the creation process.
		// the object will get placed in the proper list when it gets created
		boolean bPulled = false;
		if(bDraw)
		{
			// pull from offline list
			bPulled =_OfflineRenderList.remove(obj);
			if(!_RenderList.contains(obj) && bPulled)
			{
				_RenderList.add(obj);
			}
		}
		else
		{
			// pull from renderlist
			bPulled = _RenderList.remove(obj);
			if(!_OfflineRenderList.contains(obj) && bPulled)
			{
				_OfflineRenderList.add(obj);
			}
		}
    }

	private void AddObject_Internal(IRenderObject obj)
	{
		_CreationPending.add(obj);
	}
	
	private void RemoveObject_Internal(IRenderObject obj)
	{
		_DestructionPending.add(obj);
	}

	private void CreatePendingObjects_Internal()
	{
		// go through all objects that need created
		if(_CreationPending.size() > 0 && !_bPendingObjectLock)
		{
			for(IRenderObject toCreate : _CreationPending)
			{
				toCreate.OpenGL_Create();
				if(toCreate instanceof CNyanObject)
				{
					CNyanObject object = ((CNyanObject) toCreate);
					object.SetObjectStatus(CNyanObject.ObjStatus.ACTIVE);
					object.OnGraphicsInit();
				}
				if(toCreate.IsDrawing())
				{
					_RenderList.add(toCreate);
				}
				else
				{
					_OfflineRenderList.add(toCreate);
				}
			}
			_CreationPending.clear();
			
			// broadcast that we are done creating
			StoryBook.Chapter(NyanimalPages.Chapters.RenderHash).Set(new NyanimalPages.RenderListCreateRunEvent());
		}
	}
	
	// ret: true if done, false if more to remove
	private boolean CreatePendingObjectsAsync_Internal()
	{
		// go through all objects that need created
		if(_CreationPending.size() > 0 && _bPendingObjectLock)
		{
			IRenderObject toCreate = _CreationPending.remove(0);
			toCreate.OpenGL_Create();
			
			if(toCreate instanceof CNyanObject)
			{
				CNyanObject object = ((CNyanObject) toCreate);
				object.SetObjectStatus(CNyanObject.ObjStatus.ACTIVE);
				object.OnGraphicsInit();
			}
			if(toCreate.IsDrawing())
			{
				_RenderList.add(toCreate);
			}
			else
			{
				_OfflineRenderList.add(toCreate);
			}
			return false;
		}
		else
		{
			return true;
		}
	}

	private void DestroyPendingObjects_Internal()
	{
		// go through all objects that are queued to be destroyed
		if(_DestructionPending.size() > 0 && !_bPendingObjectLock)
		{
			for(IRenderObject ToDestroy : _DestructionPending)
			{
				ToDestroy.OpenGL_Destroy();
				if(ToDestroy instanceof CNyanObject)
				{
					((CNyanObject) ToDestroy).SetObjectStatus(CNyanObject.ObjStatus.DESTROYED);
				}
				_RenderList.remove(ToDestroy);
				_OfflineRenderList.remove(ToDestroy);
			}
			_DestructionPending.clear();
			
			// broadcast that we are done destroying
			StoryBook.Chapter(NyanimalPages.Chapters.RenderHash).Set(new NyanimalPages.RenderListDestroyRunEvent());
		}
	}

	// ret: true if done, false if more to remove
	private boolean DestroyPendingObjectsAsync_Internal()
	{
		// go through all objects that are queued to be destroyed
		if(_DestructionPending.size() > 0 && _bPendingObjectLock)
		{
			IRenderObject ToDestroy = _DestructionPending.remove(0);
			
			ToDestroy.OpenGL_Destroy();
			if(ToDestroy instanceof CNyanObject)
			{
				((CNyanObject) ToDestroy).SetObjectStatus(CNyanObject.ObjStatus.DESTROYED);
			}
			_RenderList.remove(ToDestroy);
			_OfflineRenderList.remove(ToDestroy);
			
			return false;
		}
		else
		{
			return true;
		}
	}
		
	private void Process_Internal()
	{
		if(!_bPendingObjectLock)
		{
			CreatePendingObjects_Internal();
			DestroyPendingObjects_Internal();
		}
		
		Collections.sort(_RenderList, new RenderSorting());
		for(IRenderObject ToDraw : _RenderList)
		{
			if(ToDraw.IsDrawing())
			{
				ToDraw.Draw();
			}
			else
			{
				Utilities.Log.Screen("CRenderList", "Object " + ToDraw.getClass().getSimpleName() + " isnt set to draw.");
				//SetToDraw_Internal(ToDraw, false);
			}
		}
	}
	
	public static void SetToDraw(IRenderObject obj, boolean bDraw)
	{
		Instance.SetToDraw_Internal(obj, bDraw);
	}
	
	public static void AddObject(IRenderObject obj)
	{
		Instance.AddObject_Internal(obj);
	}
	
	public static void RemoveObject(IRenderObject obj)
	{
		Instance.RemoveObject_Internal(obj);
	}

	public static void CreatePendingObjects()
	{
		Instance.CreatePendingObjects_Internal();
	}
	
	public static void DestroyPendingObjects()
	{
		Instance.DestroyPendingObjects_Internal();
	}

	public static void LockObjectHandling()
	{
		Instance._bPendingObjectLock = true;
	}
	
	public static void UnlockObjectHandling()
	{
		Instance._bPendingObjectLock = false;
	}
	
	public static boolean CreatePendingObjectsAsync()
	{
		return Instance.CreatePendingObjectsAsync_Internal();
	}
	
	public static boolean DestroyPendingObjectsAsync()
	{
		return Instance.DestroyPendingObjectsAsync_Internal();
	}

	public static void Process()
	{
		Instance.Process_Internal();
	}
	
}
