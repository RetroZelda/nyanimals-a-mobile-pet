package com.retrozelda.nyanimals;

import java.lang.reflect.Field;

import org.json.JSONObject;

import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.FSMLibrary.IState;
import com.retrozelda.nyanimals.CRectangle.ScreenPosition;
import com.retrozelda.nyanimals.nyanimal.CNyanimalStateConstructor;
import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.CObjectBank;
import com.retrozelda.utilities.CObjectBank.ObjectAlreadyStoredException;
import com.retrozelda.utilities.Utilities;

import android.graphics.RectF;

public class CLoadingState extends CNyanState
{
	private enum LoadingState {CreatePendingObjects, ReloadTexture, NewTextures, Nyanimal, 
		PostFacebookData, WaitForServerResponce, Finished};
	
	private _AnimationTextureLink _LoadingAnimation;
	private CRectangle _LoadingRect;
	private CRectangle _LoadingBG;
	
	private Class<IState> _StateToGoTo;
	
	private LoadingState _LoadingState;
	
	int[] _DrawableIDs;
	int _nDrawableCounter;
	
	@Override
    public void OnCreate()
    {
		_LoadingRect = CNyanimalFactoy.RequestObject(CRectangle.class);
		_LoadingRect.SetResourceID(R.drawable.loadinganimation);
		_LoadingRect.SetDrawLayer(Utilities.CommonLayers.HudItem);
		_LoadingRect.SetDraw(false);
		_LoadingRect.SetActive(false);
		
		_LoadingBG = CNyanimalFactoy.RequestObject(CRectangle.class);
		_LoadingBG.SetResourceID(R.drawable.loadingscreen_bgr);
		_LoadingBG.SetDrawLayer(Utilities.CommonLayers.Background);
    }
	
	@Override
	public void RetrievePassthrough(Object[] _PassthroughObjects)
	{
		_StateToGoTo = (Class<IState>)_PassthroughObjects[0];
	}
	
	@Override
	public void OnEnter()
	{
		_LoadingBG.FitScreen();
		
		_LoadingRect.MakeScaledSize();
		_LoadingRect.SetToScreenPosition(ScreenPosition.BottomRight);
				
		// loading animation
		_LoadingAnimation = new _AnimationTextureLink();
		_LoadingAnimation.Create("Loading", R.drawable.loadinganimation, 256, 256, 2, 2, 0.32f, true);
		
		// get all the drawable resource IDs into an array
		Class<?> drawableClass = R.drawable.class;

        Field[] fields = drawableClass.getFields();
        _DrawableIDs = new int[fields.length];
        _nDrawableCounter = 0;
        for(Field field : fields) 
        {
        	// ignore all facebook crap
        	if(field.getName().contains("facebook"))
        	{
        		continue;
        	}
        	
        	// add the ID
            int nID = -1;
            try
            {
	            nID = field.getInt(null);
            } 
            catch (IllegalAccessException e)
            {
	            e.printStackTrace();
            } 
            catch (IllegalArgumentException e)
            {
	            e.printStackTrace();
            }
            finally
            {
            	if(nID >= 0)
            	{
            		_DrawableIDs[_nDrawableCounter++] = nID;
            	}
            }
        }

        // lock pending objects
        CRenderList.LockObjectHandling();
        
        // start with reloading all textures
        _LoadingState = LoadingState.ReloadTexture;
        
        // the events we are listening
		StoryBook.Chapter(hashCode()).Subscribe(NyanimalPages.WebThread_Response.class, this);
	}
	
	@Override
	public void OnExit()
	{
		// stop listening
		StoryBook.Chapter(hashCode()).Unsubscribe(NyanimalPages.WebThread_Response.class, this);
		
        // unlock pending objects
        CRenderList.UnlockObjectHandling();
        
        // toss the animation away
		_LoadingAnimation = null;
	}


	@Override 
	public void OnPause()
	{
		_LoadingRect.SetActive(false);
		_LoadingRect.SetDraw(false);

		_LoadingBG.SetActive(false);
		_LoadingBG.SetDraw(false);
	}
	
	@Override
	public void OnResume()
	{
		_LoadingRect.SetActive(true);
		_LoadingRect.SetDraw(true);

		_LoadingBG.SetActive(true);
		_LoadingBG.SetDraw(true);
	}

	@Override
	public void ReadPage(IPage page)
	{
		if(page instanceof NyanimalPages.WebThread_Response)
		{
			NyanimalPages.WebThread_Response e = (NyanimalPages.WebThread_Response)page;
			_LoadingState = LoadingState.Finished;			
		}
		else
		{
			super.ReadPage(page);
		}
	}

	@Override
    public void PriorityUpdate()
    {
		
    }

	@Override
    public void Update()
    {
		boolean bDone = false;
		switch(_LoadingState)
		{
		case Finished:		
			FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH).ChangeState(_StateToGoTo, (Object[])null);
			break;
		case WaitForServerResponce:
			// sit and wait
			break;
		case PostFacebookData:
			Utilities.ServerLink.Post(this, Utilities.FacebookData, Utilities.FacebookToken);
			_LoadingState = LoadingState.WaitForServerResponce;
			Utilities.Log.Verbose("CLoadingState", "Waiting for Response from server...");
			break;
		case Nyanimal:
			CNyanimal nyanimal = (CNyanimal)CObjectBank.Retrieve("Nyanimal");
			if(nyanimal == null)
			{
				nyanimal = CNyanimalFactoy.RequestObject(CNyanimal.class);
				try
                {
	                CObjectBank.Deposit("Nyanimal", nyanimal);
                } 
				catch (ObjectAlreadyStoredException e)
                {
					// shouldnt happen in this instance
	                e.printStackTrace();
                }
			}
			else
			{

				nyanimal.LoadNyanimal(Utilities.GLSurfaceView.getContext());
				
				Utilities.Log.Debug("CLoadingState", "Starting Nyanimal FSM");
				FSMLibrary.StartNewChapter(CNyanimalStateConstructor.class, nyanimal);

				_LoadingState = LoadingState.PostFacebookData;
			}
			break;
		case NewTextures:
			// preload each drawable
			try
			{
				for(int nIndex = 0; nIndex < Utilities.Settings.PreloadTexturesPerFrame; ++nIndex)
				{
					Utilities.Log.Info("CLoadingState", "Loading id #" + --_nDrawableCounter + " | #" + _DrawableIDs[_nDrawableCounter]);
					CTextureManager.GetInstance().PreloadTextures(_DrawableIDs[_nDrawableCounter]);
				}
			}
			catch(ArrayIndexOutOfBoundsException e)
			{
				e.printStackTrace();
			}
			
			// move to the next minifakestate
			if(_nDrawableCounter <= 0)
			{
				_LoadingState = LoadingState.Nyanimal;
			}
			break;
		case CreatePendingObjects:
			bDone = CRenderList.CreatePendingObjectsAsync();
			if(bDone)
			{
				_LoadingState = LoadingState.NewTextures;
			}
			break;
		case ReloadTexture:
			bDone = CTextureManager.GetInstance().ReloadTexturesAsync();
			if(bDone)
			{
				_LoadingState = LoadingState.CreatePendingObjects;
			}
			break;
		default:
			break;
		
		}	
		
		// check if we have new UVs
		boolean newFrame = _LoadingAnimation.GetAnimation().Update(Utilities.Time.DeltaTime_Seconds());
		if(newFrame == true)
		{
			// NOTE: dont mirror Zees
			_LoadingRect.UpdateUV(_LoadingAnimation, false);
		}
    }

	@Override
    public void LateUpdate()
    {
    }

	@Override
    public void OnDestroy()
    {
		CNyanimalFactoy.ReturnObject(_LoadingRect);
		_LoadingRect = null;
    }

	@Override
    public RectF CheckCollision(CNyanObject inner)
    {
	    return null;
    }

	@Override
    public void HandleCollision(CNyanObject inner, RectF overlap)
    {
    }

	@Override
    public boolean HandleTouch(int nPosX, int nPosY, int nTouchAction)
    {
	    return false;
    }

}
