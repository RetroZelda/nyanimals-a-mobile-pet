package com.retrozelda.nyanimals;

import com.retrozelda.FSMLibrary.FSMChapter;
import com.retrozelda.FSMLibrary.FSMLibrary;
import com.retrozelda.FSMLibrary.FSMLibrary.FSMLibraryAlreadyRunningException;
import com.retrozelda.FSMLibrary.FSMLibrary.FSMLibraryNotRunningException;
import com.retrozelda.FSMLibrary.FSMLibrary.StateExistsException;
import com.retrozelda.storybook.IPage;
import com.retrozelda.storybook.IReader;
import com.retrozelda.storybook.StoryBook;
import com.retrozelda.utilities.Utilities;

import android.opengl.GLES20;
import android.opengl.Matrix;

public class CNyanimalGame implements IReader
{
	public static final String TAG = CNyanimalGame.class.getSimpleName();
	public static final int TAGHASH = TAG.hashCode();
	private float[]			m_VPMatrix		= new float[16];
	private float[]			m_ProjMatrix	= new float[16];
	private float[]			m_VMatrix		= new float[16];
		
	private volatile boolean m_Initialized = false;
	// private volatile Boolean m_RenderLock = false;

	public boolean ShouldExit()
	{
		return false;
	}
		
	public float[] GetViewProjection()
	{
		return m_VPMatrix;
	}

	public float[] GetView()
	{
		return m_VMatrix;
	}

	public float[] GetProjection()
	{
		return m_ProjMatrix;
	}

	public CNyanimalGame()
	{
	}
	
	public void Init()
	{
		// set the view matrix
		Matrix.setLookAtM(m_VMatrix, 0, 0, 0.0f, -1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
	}
	
	public void onSurfaceChanged(int width, int height)
	{
		Utilities.Log.Debug(TAG, "Surface Changed!");
		
		// set the projection matrix
		GLES20.glViewport(0, 0, width, height);
		Matrix.orthoM(m_ProjMatrix, 0, 0, -width, height, 0, -1, 1);

		// calculate the view proj matrix
		Matrix.multiplyMM(m_VPMatrix, 0, m_ProjMatrix, 0, m_VMatrix, 0);
				
		// pass the width and height
		StoryBook.Chapter(NyanimalPages.Chapters.InputHash).Set(new NyanimalPages.SurfaceChangeEvent(width, height));
	}
	
	public void Update()
	{
		if(m_Initialized == false)
		{
			return;
		}

		Utilities.Time.Update();
		
	        try
            {
	        	// beginning state updates
	            FSMLibrary.PriorityUpdate();
	            
		        // beginning object update
		        CNyanimalFactoy.PriorityUpdateObjects();
	            
	            // normal update
		        FSMLibrary.Update();
		        
		        // update all active objects
		        CNyanimalFactoy.UpdateObjects();
		        
		        // TODO: Handle all collision checks here
		        
		        // process all the events
		        StoryBook.Process();
		        
		        // last minute state updates
		        FSMLibrary.LateUpdate();
	            
		        // late object update
		        CNyanimalFactoy.LateUpdateObjects();
		        
		        // console
		        Utilities.RenderConsole.Process();
            } 
	        catch (FSMLibraryNotRunningException e)
            {
	            e.printStackTrace();
            }
	        
	}

	public void Render()
	{
		if(m_Initialized == false)
		{
			return;
		}
		
		// draw our game stuff
			// Log.d(TAG + ":Render()", "Render Lock");
			
			// Ignore the passed-in GL10 interface, and use the GLES20
			// class's static methods instead.
			GLES20.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
			GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
			
			CRenderList.Process();
			
	}
		
	public void onCreate()
	{
		// set the game stuff
		m_Initialized = true;
	}

	public void HandleBackButton() 
	{
		Utilities.Log.Verbose(TAG, "Back Button!");
		
			FSMChapter chapter = FSMLibrary.Chapter(CNyanimalsGameFSMConstructor.CHAPTERHASH);
			chapter.PopState();
	}
	
	protected void onStart()
	{		
		// start the game FSM.  it starts in the loading state, so it needs to know the state to go to
        FSMLibrary.StartNewChapter(CNyanimalsGameFSMConstructor.class, CRoomState.class );
            
        // register events
        StoryBook.Chapter(NyanimalPages.Chapters.Input).Subscribe(NyanimalPages.BackButtonEvent.class, this);
		                
		// start the FSM
		try
        {
	        FSMLibrary.StartLibrary();
        } 
		catch (Exception e)
        {
	        e.printStackTrace();
        }
		
		// create all objects
		CRenderList.CreatePendingObjects();
		
		// start game-related stuff
		Utilities.Time.Start();
	}

	protected void onStop()
	{
		// stop the game fsm
		FSMLibrary.StopChapter(CNyanimalsGameFSMConstructor.class);
		
		// stop the FSM
		try
        {
	        FSMLibrary.StopLibrary();
        } 
		catch (Exception e)
        {
	        e.printStackTrace();
        }

		// destroy all objects
		CRenderList.DestroyPendingObjects();
		
		// stop game-related stuff
		Utilities.Time.Stop();
	}
	
	protected void onDestroy()
	{
		// Destroy FSM
		try
        {
	        FSMLibrary.StopLibrary();
        } 
		catch (FSMLibraryNotRunningException e)
        {
	        e.printStackTrace();
        }		

		// TODO: flush objects from the factory

		// destroy all objects
		CRenderList.DestroyPendingObjects();
		
		// TODO: throw exception if there are still objects in the RenderList or Factory

        // unregister events
        StoryBook.Chapter(NyanimalPages.Chapters.Input).Unsubscribe(NyanimalPages.BackButtonEvent.class, this);
	}

	// TODO: Add onPause() to FSM Chapters
	protected void onPause()
	{
		/*
		for(int nStateIndex = 0; nStateIndex < m_States.size(); ++nStateIndex)
		{
			CNyanState state = m_States.get(nStateIndex);
			state.onPause();
		}
		*/

		// stop game-related stuff
		Utilities.Time.Pause();
	}

	// TODO: Add onResume() to FSM Chapters
	protected void onResume()
	{
		/*
		for(int nStateIndex = 0; nStateIndex < m_States.size(); ++nStateIndex)
		{
			CNyanState state = m_States.get(nStateIndex);
			state.onResume();
		}
		*/

		CTextureManager.GetInstance().ReloadTextures();
		
		// stop game-related stuff
		Utilities.Time.Resume();
	}

	@Override
    public void ReadPage(IPage page)
    {
		if(page instanceof NyanimalPages.BackButtonEvent)
		{
			HandleBackButton();
		}
		else
		{
			Utilities.Log.Warning(TAG, "CNyanimalGame recieved an unhandled Storybook Page: " + page.getClass().getName());
		}
    }
};